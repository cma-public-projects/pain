# Processing and analysing PAIN networks
The `pain_analysis` folder contains a number of miscellaneous scripts for processing network objects. This page seeks to summarise each file and how to use them. Many of them have functional documentation within the files including a description of how to run each file from the command line. Thus, we only seek to provide a higher-level overview of each scripts usage and output here.

For information on the included processing functions (found in `pain.proclib`) and associated yaml see the [proclib explanation](pain_proclib.md).

## Generating and plotting the close-contact distributions of all individuals
Two of the files included are for the purpose of finding and analysing the close-contact distribution for individuals in the network. 
### `close_contact_distribution.py`
The first, `close_contact_distribution.py` takes in a path to a `_full.gpickle` network (with close-contact groups added) as well as other parameters such as the output directory, whether to split distributions by TA, whether to shift vaccine dates, etc., and it will output (by default) csvs for:
- the number of individuals in each demographic group (`num_individiuals.csv`)
- the number of close contacts for every individual in the network by layer and demographic attributes of the contacts (`close_contact_info_individual.csv`). E.g. school, ethnicity, age, vaccine status
- a csv per layer specified (e.g. dwelling, school) that has the number of close-contacts from each demographic group to each demographic group in that layer (`close_contacts_LAYER.csv`).

The script also has the ability to make plots of these contact matrices (and other relevant statistics). These are summarised below:
- the close contact distribution (total and across each layer) as one figure (`close_contact_distribution.png`)
- the (total) contact distribution (total and across each layer) as one figure (`contact_distribution.png`)
- pie chart of the proportions of close contacts by ethnicity (`close_contact_distribution_ethnicity.png`)
- pie chart of the proportions of close contacts by age (`close_contact_distribution_age.png`)
- pie chart of the proportions of close contacts by vaccination status (`close_contact_distribution_vax.png`) 
- pie chart of the proportions of close contacts by TA (`close_contact_distribution_TA.png`) 
- distribution of close contacts by dwelling size (`dwellings_contacts_vs_size.png`)
- time series of vaccination coverage of close contacts over time (`close_contact_distribution_vax_time_series.png`)
- age close contact matrix across each layer (`age_close_contact_matrix_layers.png`)
- age total close contact matrix (`age_close_contact_matrix.png`)
- vaccine dose close contact matrix across each layer (`vaccine_close_contact_matrix_layers.png`)
- vaccine dose total close contact matrix (`vaccine_close_contact_matrix.png`)

For more information on how to generate each of these outputs from the command line run the following command (from the main pain directory):
```sh
python pain_analysis/close_contact_distribution.py -h
```

### `plot_close_contact_distribution.py`
The `csv` outputs from `close_contact_distribution.py` are read in by this plotting script which provides almost all of the plotting capabilities of the previous script (apart from the timeseries and dwelling size plots) but also allows for more layer and demographic specific (granular) plots too.

The required argument for this script is the path to the directory where the `csvs` produced by the previous analysis file were generated. The other optional arguments specify which of the plots to generate. In addition to the plots outlined above, this script can also produce:
- the log(close contact) distribution (total and across each layer) as one figure (`close_contact_distribution_log.png`)
- a log-log plot of close contacts in the community layer with the theoretical size distribution overlayed (`powerLaw.png`)
- ethnicity total contact matrix (`ethnicity_close_contact_matrix.png`)
- ethnicity close contact matrix across each layer (`ethnicity_close_contact_matrix_layers.png`)
- a plot for each layer of the ethnicity close contact matrix split by age (`ethnicity_close_contact_matrix_LAYER_by_age.png`)
- a plot for each layer of the age band close contact matrix split by ethnicity (`ethnicity_close_contact_matrix_LAYER_by_age.png`)
- TA to TA contact matrix for each layer (`TA_close_contact_matrix_layers.png`)
- TA to TA total contact matrix (`TA_close_contact_matrix.png`)

For more information on how to generate each of these outputs from the command line run the following command (from the main pain directory):
```sh
python pain_analysis/plot_close_contact_distribution.py -h
```

## Summarising the vaccine coverage of all individuals
As the number of vaccine doses in the network changes over time (similarly to the vaccine rollout in Aotearoa), we also provide a script to summarise how vaccine coverage changes for different demographics and locations. The script takes a required argument of the path to the `.gpickle` network file to analyse, and the optional arguments control the relative dates of the vaccine data in the network to the produced time series days and also whether to split by District Health Board (DHB) of SA2.

The script produces the following outputs:
- A `csv` containing the time series of counts of vaccines by age, ethnicity, dose number and (optionally) DHB (`vaccine_coverage_by_age_eth.csv`)
- Time series graph of counts of vaccine doses over time split by age (`vaccine_coverage_by_age_prop.png`)
- Time series graph of counts of vaccine doses over time split by ethnicity (`vaccine_coverage_by_age_prop.png`)
- Time series graph of counts of vaccine doses over time (`vaccine_coverage_num.png`)
- Time series graph of proportion of individuals with different vaccine doses over time (`vaccine_coverage_prop.png`)

For more information on how to generate each of these outputs from the command line run the following command (from the main pain directory):
```sh
python pain_analysis/vaccine_coverage.py -h
```

## (Re-)Generating nodelists and edgelists
This script provides the functionality of reproducing the nodelists and edgelists for the individual, workplace, dwelling and school layers of the network, and/or for the whole network (including community layers). It takes in the path to the network and the other arguments specify which layers to create nodelists and edgelists for.

For more information on how to select which nodelists/edgelists to generate from the command line run the following command (from the main pain directory):
```sh
python pain_analysis/network_to_nodelist_edgelist.py -h
```