# Scripts and Functions
The purpose of this document is to explain each file that is included in `pains.buildlib` and provide an overview of what each `main` file for each step in the network generation process aims to achieve. See the [functional documentation](modules.rst) for more information on each individual Python function.

## Main
The main run script for the whole build library is the `make_network.py` file. It contains a main script that reads in the configuration yaml for the current network building run, which it then parses and calls the main generation script for each layer depending on the parsed configuration. Once each layer of the network has been generated, it will then output the final Networkx objects as well as adding a [close-contact distribution](pain_cc) to the generated network.

## Special Selection
The `special_main.py` file is called before network generation if the outputted network is desired to be made up of smaller components of Aotearoa New Zealand. The main purpose of it is to read in the `special_selection` part of the input yaml. It will then modify the list of excluded SA2s and TAs so that any individual, school, workplace, dwelling or community event that are located within those excluded lists are not added to the network.

## Dwellings
The `dw_main.py` file contains a `main` function which generates the dwelling layer of the network and its associated output. To do this, it calls functions found in `dw_functions` and `universal_functions`. The main function takes in the individuals nodelist, dwelling nodelists and other associated dwelling parameters from the configuration yaml and first generates three nested dictionaries of the demographics of individuals to be housed in each SA2 (one for small dwellings, one for large dwellings and one for individuals that have not been assigned a dwelling). Dictionaries for the small and large dwellings themselves are then generated which contain the dwelling locations, the step in the build process in which they are to be filled and then their demographic makeup.

The first two steps in the actual build/filling process involve removing dwellings from the small and large dwellings dictionaries until the required number of people to fill the remaining dwellings in the dictionary is as most the available people in each SA2.

In step 3, 4, 5.1, 5.2, 6.1 and 6.3, the small and large dwellings begin to be filled by assigning individuals to them (using the selection criteria specified in the code and yaml). This is done by iterating over TAs, iterating over SA2s in those TAs, iterating over dwellings associated with the current 'step' and then using the selection criteria to find people to fill dwellings for the current step, SA2, TA triple. At the step of filling an individual dwelling this function first checks to see if there are sufficient people to fill the dwelling from the pool of unhoused individuals. Following this the number of people available in a SA2 is updated to reflect the composition of the current dwelling. Finally an empty dwelling dictionary is created which will contain the dwelling attributes and the attributes if the individuals who will be put into it. Following this, for each of the age bands for a dwelling, and with age and ethnicity selections, individuals are moved from the pool of unhoused individuals to the dwelling dictionary. The difference between these steps are whether the houses are filled using census matching and ethnic assortativity. After step 4, we also allow individuals who had no assigned dwelling to be selected when filling the remaining dwellings. 

## Workplaces
The `wp_main.py` file contains a `main` function which generates the workplace layer of the network and its associated output. To do this, it calls functions found in `wp_functions` and `universal_functions`. The main function takes in the individuals nodelist, workplace nodelist, sector files and other associated dwelling parameters from the configuration yaml and first generates nested dictionaries corresponding to workers, workplaces to fill, the number of individuals in each sector/SA2 pair and a dictionary containing the proportion and count of workers who commute to/from each SA2 pair.

We then fill the workplaces in four 'steps'. In each step we shuffle the remaining workplaces to fill (so that they are filled in a random order) and then iterate over every workplace. We select the home TA/SA2 of a worker and then get their demographic characteristics based on their TA and inputted selection criteria. Finally, the exact individual that matches a set of possible demographic characteristics is selected. This process is continued until workplaces are filled or until we run out of individuals of the correct characteristics to fill the workplace with. In the first two steps, we enforce worker selection via the sector/SA2 counts of individuals from census data and then relax this requirement in the last two steps. Steps 1 and 3 fill by selecting workers based on their home SA2 and in steps 2 and 4 we relax this requirement to just select from TA

The first two steps in the actual build/filling process involve removing dwellings from the small and large dwellings dictionaries until the required number of people to fill the remaining dwellings in the dictionary is as most the available people in each SA2.

## Schools
The `sc_main.py` file contains a `main` function which generates the school layer of the network and its associated output. To do this, it calls functions found in `sc_functions` and `universal_functions`. The main function takes in the individuals nodelist, early childhood education nodelist, school nodelist and other associated school parameters from the configuration yaml and then generates a dictionary of students in each SA2, and dictionaries for early childhood education centres, single age and multi-age schools. A dictionary containing the proportion and count of students who commute to/from each SA2 pair is also created. The age, then ethnicity then actual student is chosen based on the attributes of the school and students in the chosen commute SA2. This is continued until schools are filled.

In step 1 and 3 schools with one age band are filled by using commute SA2 and then commute TA respectively. In steps 2 and 4, schools with multiple age bands are filled similarly. Finally, in steps 5 and 6 the early childhood education centres are also filled first by SA2 and then TA.

## Community (both EV and EX)
The `community_main.py` file contains a `main` function which generates the community layer of the network and its associated output. We note that this function is called twice by `make_network.py` first to create the EX layer and then the EV layer. We combine our description of these calls however, as they are functionally the same but just with different event distribution parameters. To do this, it calls functions found in `community_functions` and `universal_functions`. In `make_network.py`, the `generate_new_nodelist` function from `community_functions.py` is first called to generate an individuals nodelist from the individuals nodelist csv that contains information about the number and location of long-range community events an individual is apart of. The function also reads in the school and workplace layer to add the workplace and school (if they exist) SA2 of each individual to their possible list of short-range community event locations. The main function of `community_main.py` then takes in this individuals nodelist and parameters associated with each community layer from the configuration yaml. It first generates a dictionary of individuals containing information about the actual community events in the network an individual is part of. The number of events and location of each event that an individual is part of is created probabilistically every time the community layer is generated and so this generated nodelist dictionary will be different every time. 

This individuals nodelist dictionary is then passed to the `make_events` function. The function iterates over every SA2 in every TA that individuals in the dictionary reside in and generates events with size distribution given by the input parameters. Individuals who have a short- or long-range community event in the SA2 of this community event are then randomly chosen to fill this event. This process continues until all individuals have been assigned to their correct number of community events.

## Merge
After each layer in the network has been generated the `main` function within `merge_main.py` is called next. This can parse paths to existing layer files (if a network is desired to be built from existing layers) or will use the layers that have been generated during the previous steps in the network build process. 

The function reads in all group layers and adds the group nodes (and their attributes) and readme associated with each layer to a 'final dictionary'. It will also read in custom layers and link group nodes together if desired. Finally, it adds every individual node, their attributes and their edgelists, to the final dictionary. This final dictionary is then saved as a `.pkl` file.

## Networkx (NX)
The `main` function within `nx_main.py` takes in the path to the `.pkl` file containing the 'final dictionary' as well as the desired workplace shutdown parameters. It then adds all nodes, their attributes and their edges to a Networkx graph. Workplaces are also probabilistically assigned an OPEN/CLOSED attribute for each workplace shutdown level.

This final network is then saved as a `.gpickle` file along with the full network readme (as a `.txt` file).

## Close-contact
If desired, the `make_network.py` script will then call the `main` function from `close_contact_main.py`. This function takes in a path to an existing network and the close-contact distribution parameters and then calls functions from `close_contact_functions` and `universal_functions` to add the close-cover (and overcover) groups to the network before saving the modified object.

For more information on the implementation of the close-contact distribution please see the corresponding section of the [close-cover documentation](pain_cc.md) as well as the latter parts of the build section within the [pains tutorial](pain_tutorial.md).


# Outputs
Each `main` file within the `make_network.py` process saves output to do with the layer. In this section we provide an overview of the files generated that are similar/shared for/across each layer and then provide a layer-specific list of files at the bottom.

## Shared/Similar output
Below is the list of files that are produced by the `main` script for every layer.
- `alphaNumericCode_nodelist_layerID.csv`: Contains the nodelist and node attributes for the layer. NB: not produced by default for the community layers as they would be too large. Also not produced for the individual's layer
- `layerAlphaNumericCode_edgelist_layerID.csv`: Contains the edgelist for the layer. NB: also not produced for community layers due to size constraints
- `layerAlphaNumericCode_layerID_readme.txt`: Contains the readme for the layer (e.g. information regarding the 'steps' in the filling processes)
- `current_layerID.pkl`: When parsed into Python, will return the layer of the network as a dictionary
- `current_layerID.readme`: Contains the readme for the layer (e.g. information regarding the 'steps' in the filling processes)

## Merge-specific output
The `merge_main.py` script also outputs a `.pkl` file containing the 'final dictionary' for the merged network.

## NX-specific output
The `nx_main.py` script also produces the final `.gpickle` object containing the Networkx graph for the network and the final `_readme.txt` for the whole network. It also reproduces the layer nodelist and edgelists for the specific network created (as layers may have been generated separately to this final network):
- `networkAlphaNumericCode_network_NODELIST_layerID.csv`: Contains the nodelist and node attributes for the layer (_including_ the individual's layer). NB: not produced by default for the community layers as they would be too large. 
- `networkAlphaNumericCode_network_EDGELIST_layerID.csv`: Contains the edgelist for the layer. NB: also not produced for community layers due to size constraints
Finally, it also produces a `_INDIVIDUAL_ATTRIBUTES.pkl` file that contains a dictionary of attributes for each individual in the network.

## Close-cover-specific output
The `close_cover.py` script reproduces the nodelist and edgelist for each layer with the added close-cover and overcover groups. These are instead labelled:
- `networkAlphaNumericCode_network_full_group_nodelist_layerID.csv`
- `_full_edgelist_layerID.csv`
- `_full_indiv_nodelist.csv`
It also produces a new `networkAlphaNumericCode_network.full.gpickle` file that contains the updated Networkx graph and a `networkAlphaNumericCode_network_full_README.txt` for information about the close-cover process.