# `pain` dependencies

The `pain` package has the following dependencies (package name followed by minimum version). These will be installed/updated by default during installation of the package.

python >= 3.10  
numpy >= 1.23.1    
networkx >= 2.2  
PyYAML >= 5.1  
matplotlib >= 3.6.0    
seaborn >= 0.12.2