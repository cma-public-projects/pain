# Adding custom edgelists/nodelists

To add custom edges between group nodes, the network generation code will need to be run more than once:

## Step 1 - generate initial network

The first run used to generate a network from scratch, generating the dwelling, workplace, school, and community layers. `configs/default.yaml` provides an example of how a configuration .yaml file should be set up to achieve this. 
- use `python3 generate_pain.py input\file.yaml` in command line to generate a network

file.yaml (example file name) should have for each layer: `generate` set to `True`  and `custom layers` set to `False`

e.g. `dw_parameters:
  generate: True  ` (+ wp, sc, ex, ev )

and

`custom_layers: False` 

## Step 2 - generate new individual - group edge lists

Before running `generate_pain.py` a second time, we will run code to link output files from the first network. Code to link layers can be found in `network_group_edgelists\code`. Current custom options include the following:
a) `link_education_staff_workplaces.rmd`: takes the original network file output and links schools to workplaces (and individuals within those workplaces). This will produce a new edge list (in the format of `individual|Node_ID`) which will be saved to `input/network_custom_edges`.
b) `create_custom_community_events.Rmd`: takes the node/edge lists from the original network and then links individuals together with a set of custom group nodes. The function to create groups will take in a number of groups to make, as well as a set of input parameters on the composition of age,sex,ethnicity to match and a set of probabilities for SA2s to draw individuals from (this can just be the work/school commute data that was previously used in the original network construction, or a brand new set of probabilities). More details are discussed in the notebook.

Run required r markdown script in `network_group_edgelists\code` to generate an edge list of individuals connected to group nodes.  These scripts have the intention of adding workers to schools (e.g., teaching staff), workers to dwellings (e.g., in aged care) and so on

**Ensure** that the r scripts take the output node lists/edge lists from Step 1.

e.g., `link_education_staff_workplaces.rmd`

## Step 3 - add new custom edges to network

We will then run `generate_pain.py` a second time, but this time using an update configuration .yaml file, where the option to generate the dwelling, workplace, school and community layers is set to `False`, and the option for `custom_layers` is set to `True`. The new edge list should be referenced under `custom_parameters:`. See `default_part2.yaml` for an example.


- use `python3 generate_pain.py input\second_file.yaml` in command line to generate a network

second_file.yaml (another example file name) should have for each layer: `generate` set to `False`  and `custom layers` set to `True`

e.g. `dw_parameters:
  generate: False  ` (+ wp, sc, ex, ev )

and

`custom_layers: True`

with reference to the output file from Step 2.

If adding new node and new edges to the network, the yaml should be structured as follows:

```
custom_parameters:
  1:
  ​	 nodelist: "output/new_nodelist.csv"
 ​   edgelist: "output/new_edgelist.csv"
```


If just adding new edges to the network (e.g., linking working staff to schools), the yaml should be structured as follows: 

```
custom_parameters:
  1:
    edgelist: "output/new_edgelist.csv"
```

