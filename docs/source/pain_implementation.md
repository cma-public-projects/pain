# Structure of the directory and how to make a network (simple steps)
This page begins with a brief outline on how to run the network building code included in the `pains` sublibrary before providing a high-level overview of the files and folders found that relate to the generation and processing of `pain` networks. Further information on specific files are linked throughout and can also be found on later pages.

## Instructions
To generate a network, there are several steps that should be followed. Detailed information on this process can be found in the [pain tutorial](pain_tutorial.md). We provide a brief summary of the steps here:

1) Place all required input files (including those from `painputs`) in the ```input``` subdirectory.

2) Create a [config yaml](yaml_reference.md) based on one of the default yamls found in the ```configs``` subdirectory.

3) Run the `generate_pain.py` `Python` file with the path to your config yaml as an argument (e.g. ```python generate_pain.py "path/to/config/yaml.yml"```). 

4) The network `gpickle` and its associated layer and readme files will then be outputted in the output directory specified in the config yaml.

## Directory Structure
The following section will describe each of the folders within the `pain` library that relate to networks. 

### `pains`
This folder contains the `pains` sublibrary, with a (sub)sublibrary related to the [building](pain_buildlib) (`buildlib`) and the [processing and analysis](pain_proclib) (`proclib`) of networks.

### `configs`
The location of the configuration files that are used to generate networks. The config files are in `yaml` format and have several sections specifying the input parameters for the network. This includes the setting the file names of the individual, dwelling, workplace, and school node lists, as well as setting other specific parameters. For more information on this see the [configuration yaml description](yaml_reference.md). This folder also includes the file `labels.yaml`, which is used to map variable names within `pains` to column headings within the input files.

### `input`
This is where all input files to the network building code should be located. Files produced by scripts in the ```pain_add_custom``` folder will also get saved here within a (created) folder named ```network_custom_edges```.

### `output`
All output files from the network generation code will go here. These will include:

- A `.pkl` and `.gpickle` file, each containing a networkx object.
- A separate `.pkl` and `.txt` for each layer, as well as `.csv` files for edges and node lists.
- A `readme.txt` files with messages from each step of the building process and useful network metrics.

### `pain_add_custom`
This folder contains scripts that take a network and create new custom edges. The files that carry out this process are as follows: 

- `link_education_staff_workplaces.Rmd`: This file takes a network and probabilistically links schools to workplaces using an algorithm to find the best match (in terms of workplace and school size, and the potential ratio between them). This will produce an edge list where school workers will be linked to students at a school, as well as other employees. 
- `link_aged_care_facilities_workplaces.Rmd`: This notebook links aged-care facilities (ID present in the dwelling node list) to health care sector workplaces.
- `create_custom_community_events.Rmd`: This notebook contains a function to generate custom group events, given a set of input parameters (e.g. number of events, size, demographics). 

### `pain_analysis`
This folder contains a set of scripts for extracting useful metrics from a given network including:

- `close_contact_distribution.py`: Outputs the distribution of close-contacts for individuals of chosen demographics
- `network_to_nodelist_edgelist.py`: Outputs the nodelists and edgelists for a chosen network (note that similar functions are run by default during PAIN generation)
- `plot_close_contact_distribution.py`: Plots the output files of `close_contact_distribution.py`
- `vaccine_coverage.py`: Plots/Saves the vaccine coverage for a chosen network