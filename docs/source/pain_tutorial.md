# PAINs Tutorial
## Overview
The [first tutorial section](#basic-tutorial) of this page intends to briefly guide the user through the process of building a small PAIN network. It also demonstrates how the user can take the output from the model and produce simple analysis files using the processing library.

[Following sections](#adding-custom-edgelists) in this page extend the tutorial and show how to use other capabilities of the `pains` sublibrary (namely adding extra layers).

```{note}
This tutorial follows the process to run the `pains` sublibrary locally, with inputs appropriate to local use. 
However the building of larger or custom networks (with different nodelists and attributes) may require more memory and processing than usually available locally. For building such networks the COVID-19 Modelling Aotearoa team uses a High Performance Supercomputer.
```

## Brief overview of the required input files
The standard basic files to configure and build a `pains` network are:
- `config.yaml`
  This contains the configuration settings for the network building process. This yaml specifies the inputs to the `pains.buildlib` code. It specifies which nodelists to use; initial and so on. More detailed documentation of `config.yaml` parameters can be found in the [yaml explanation page](yaml_reference.md).
  
- `small_dwelling_nodelist.csv`, `large_dwelling_nodelist.csv`, ...
  These are the nodelist files and other related information required to build a network. For more information on these other input files see the [input file explanation](pain_inputs).

## Basic tutorial
This tutorial runs through the basic steps for building a PAIN network using `pains`.

```{caution}
Make sure that you have first completed the steps in [Prerequisites](python_prereqs.md) and [Installation](install.md) before starting this tutorial.
```

### 1. Set up
#### 1.1. Create a configuration yaml
Open the `tutorial.yaml` from `/configs`.

We now need to edit the tutorial `yaml` to ensure that our network can be built using it as an input. Open the `yaml` for editing in your preferred editor.

This file is broken into 9 sections; 
- BASE FILES, 
- DWELLING LAYER, 
- WORKPLACE LAYER, 
- SCHOOL LAYER,
- COMMUNITY LAYERS, 
- MERGING,
- NX PARAMETERS 
- SPECIAL CROPPING and 
- CLOSE-COVER PARAMETERS.

The majority of `tutorial.yaml` is already set up as required to run a simulation on a small network, but we need to make some small changes. 

In the BASE FILES section change the line

```
  output_directory: "output_PAIN/"
```
to

```
  output_directory: "output_PAIN/tutorial/"
```
This will change the location where all outputs from the `pains` build process are placed to `output/tutorial/`. NB: we recommend each network generated should use a different subdirectory within `output/` for consistency and to stop confusion occurring wherein different network files would otherwise all be located in the same folder.

Next, in the SPECIAL CROPPING section, change

```
  custom_variables:
    selected_ta:
      - 76
```

to

```
  custom_variables:
    selected_ta:
      - 50
```
This will change the outputted network from covering containing all of Auckland Region (a large city with ~1.6million people) to South Wairarapa (which is much smaller and more easily constructed locally, especially for testing out the code).

Once you have tested the code for the small Territorial Authority (TA) you can also experiment with larger TAs. Some suggestions are: TA 60 (Christchurch City) or using a list of multiple (preferably adjacent) Territorial Authorities which you would format like this in the yaml:
```
  custom_variables:
    selected_ta:
      - 27
      - 28
```

### 2. Build the network
We can now create our network. To do this, first navigate to the `pain` directory within your terminal and then run

```
python  generate_pain.py pain_configs/tutorial.yaml
```

This build process should take less than a minute depending on your computer specs.

#### 2.1 Check that the build process has produced some output
During the build process, barring any errors causing Python to quit, you should see times printed to the terminal corresponding to the amount of time taken to complete each step in the build process. For example:

```
DW 0:00:08
WP 0:00:05
SC 0:00:05
COMMUNITY SETUP 0:00:06
EX 0:00:00
EV 0:00:00
MERGE 0:00:06
NX FUNCTIONS 0:00:00
CC FUNCTIONS 0:00:05
```

Once the network has been built, check that it has produced some output. Amongst many layer-specific output files, there should be one `network.full.gpickle` and one `network.gpickle` file in your `output/tutorial` directory (or wherever you specified for the `output_directory` parameter in your config `yaml`.)

For more information on the type and structure of the output files from running the `pains.buildlib`, see [here](pain_buildlib.md).

### 3. Postprocess outputs
The `pain` package includes a number of scripts for processing the generated network files of simulations in the `postlib` submodule. These are documented [here](pain_proclib.md).

We will use the `postlib` to extract some simple analytics metrics regarding the number of contacts for individuals and the corresponding age-contact matrices.

#### 3.1 Check the desired metrics are selected

Open the `pain_configs/tutorial_processing.yml` and make sure that at least the following lines are set to True:

```
# This is a default input file for the running of the network processing code
# For information on variables refer to `docs/source/processing_yaml_reference.md`

# Path to network
...
extract_analytics: True
age_contact_matrices: True
...
```

We will also need to edit the network path to your generate `network.full.gpickle` file, for example:

```
# This is a default input file for the running of the network processing code
# For information on variables refer to `docs/source/processing_yaml_reference.md`

# Path to network
network: output_PAIN/tutorial/2023_06_15_K1AES_network.full.gpickle
...
```
But you will need to change the network name to date and unique 5 character string that was generated when you ran the code above.

An explanation of processing yamls and their contents is located [here](pain_proclib.md)

#### 3.2 Run the processing library
Once you have the metrics and analytics you want, we can execute the processing run script.
From the terminal run:

```
python  process_pain.py pain_configs/tutorial_processing.yml
```

Once this has finished running your `output_PAIN/tutorial` folder should contain a `.full._network_analytics.txt` file that contains information on the subcomponents contact matrices and age contact matrices for your generated network.

There are additional metrics that you can turn on (set to `True`) in the processing yaml, including producing SA2 to SA2 connection matrices, and writing community event node and edgelists. These are turned off by default as they are (currently) very memory intensive, especially for larger networks.


## Adding custom edgelists
!!!TBD: based on custom stuff from painputs
We begin this section by looking at how the `npz` files and network information can be combined to provide demographic-dependent statistics. The `example_networks/IH_nodelist_0_doses.csv` contains demographic information for every individual node within the simulation run. By joining this data onto the IDs of either the 
enode or the anode, we can gain information as to the demographics of the infected/infector individuals within the simulation. 

The section of `vignette_plotting.py` labelled **Plotting outputs by different demographic factors** contains code that plots the ethnicity of infected individuals over time. To run this section of the code, edit the paths of the outputted plots to your desired directory (found in lines 46 and 50). The first argument when you run the file should be your 

The resultant timeseries and bar chart produced should look like similar to (where we can see the breakdown in infections by ethnicity):