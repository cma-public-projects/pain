pains package
=============

.. automodule:: pains
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pains.buildlib
   pains.proclib
