# Instructions
The following document will outline each of the steps required to generate input files for the network building code. This includes the individuals, dwellings, workplaces, and education node lists, as well as the workplace and education commuting files. For the latest information on where to find previously generated files, see the file `docs/running_network_code.md`. 

## For the following steps:
- Ensure that the repositories (repos) are up to date on your machine. The notebooks will save all files using the short ID of the git commit that is checked out in file/folder names, if you make changes locally and then run the notebook, it will still use the latest git commit ID. To make sure that file names are consistent with content, make sure to push any changes before running any of the node list construction notebooks.
- Ensure that the git repo has an `inputs` and `outputs` folder. These should be should already exist, but may be ignored by git. input files will be copied automatically over from Dropbox into the `inputs` folder, and output files will be automatically copied over to Dropbox from the `outputs` folder. There should be no need to manually copy any files in either direction. 
- Ensure that input files that are located on dropbox are available offline. This is an easy thing to forgot and will lead to errors in the code that are not obviously tied to the file being empty. If you run into any errors, this should be the first thing that is checked. 


## 1) Download RStudio 
- See: [R Studio Download](https://posit.co/download/rstudio-desktop/)


## 2a) Generate Individual Node List
- Clone [individuals repo](https://gitlab.com/cma-public-projects/nz_individuals) 
- Open `nz_individuals.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `construct_individual_nodelist.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 65-121). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on.
- Set the week specified for the Marketview data on line 113. 
- Go to `tools` and `project options` and navigate to `Python`. Here, make sure a python interpreter is selected. 
- Whichever python interpreter you use should have the following packages installed: 
`pandas`, `numpy`, `os`, `datetime`, `collections`, `random`, `math`. (note: ideally we would use a virtual environment to ensure consistency across machines). 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 30-40 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `individual/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. 

### 2b) Generate Clean TA Sector Triples Data
- The count of individuals per workforce sector is available through the IDI and the original extracted output is located in dropbox at: `covid-19-sharedFiles/data/IDI_raw_outputs/workforce/Workforce_Structure_Resubmission/sectorTriples/df_census_and_IR2018_ta_safe.csv`.
- Manually copy over the files `df_census_and_IR2018_ta_safe.csv` and df_census_and_IR2018_nz_safe.csv` to the  within the `inputs` folder in the same individuals git repo. 
- Open the notebook `code/backfilling_workSector_triples.Rmd` [found here](https://gitlab.com/cma-public-projects/nz_individuals/-/blob/main/code/backfilling_workSector_triples.Rmd).  
- Click on Run All.
- A cleaned version of the original data, `df_census_and_IR2018_ta_safe_backfilled.csv`, will be stored within the outputs folder. This file is used within the network building. A copy should be stored within the Dropbox at: `covid-19-sharedFiles/data/IDI_processed_outputs/individual/sectorTriples`.


## 3) Generate Dwelling Node List
- Clone [dwellings repo](https://gitlab.com/cma-public-projects/nz_dwelling_structures)
- Open `nz_dwelling_structures.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `construct_complete_dwelling_nodelist_with_aged_care.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats`, `sf` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 33-75). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on. (ST: After rerunning the code on a new machine, I found that all the bugs that I ran into were to do with the input files not being available offline - even if the error message is not clear). 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 10 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `dwellings/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. 


## 4a) Generate Workplace Node List
- Clone [workplaces repo](https://gitlab.com/cma-public-projects/nz_workplace_structures)
- Open `nz_workplace_structures.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `processing/construct_workplace_nodelist.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats`, `poweRlaw` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 49-58). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on. 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 30 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `workplaces/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. 

## 4b) Generate Workplace Commute File
- From the same repo ([workplaces repo](https://gitlab.com/cma-public-projects/nz_workplace_structures), open the file `clean_commuting_work.Rmd`.
- Ensure the packages `tidyverse`,`here` are installed.
- Set the local directory for Dropbox on line 34.
- Run notebook by selecting `Run All` in the top right of RStudio.
- Output files will be stored in dropbox: `data/IDI_processed_outputs/commuting/` 


## 5a) Generate Education Node List
- Clone the [schools repo](https://gitlab.com/cma-public-projects/nz_education)
- Ensure that the repo is up to date on your machine. The notebook will save all files using the 
- Open `nz_education.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `processing/construct_education_nodelist.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats`, `poweRlaw` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 49-58). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on. 
- Set the year of the data that you want. As of 2022-12-12 the dropbox contains data from 2022 and 2019. 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 30 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `education/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. Three separate node lists will be produced: one for ECE centres, one for schools, and one where they are combined into one file. As of 2022-12-12, the network building code takes the two separate ECE and school node list files as input.

## 5b) Generate Education Commute File
- From the same repo ([schools repo](https://gitlab.com/cma-public-projects/nz_education)), open the file `clean_commuting_schools.Rmd`.
- Ensure the packages `tidyverse`,`here` are installed.
- Set the local directory for Dropbox on line 35.
- Run notebook by selecting `Run All` in the top right of RStudio.
- Output files will be stored in dropbox: `data/IDI_processed_outputs/commuting/` 

## 6) Other Files
There are a number of other files that are required for the network building that have been manually created and are now stored within dropbox. 
- `school_age_breakdown.csv`: available at Dropbox `covid-19-sharedFiles/data/education`. This file was manually made in microsoft excel and contains proportions of under 15s and over 14s for each school type (proportions derived based on the ages of students at the school). 
- `SA2_TA18_concordance.csv`: available at Dropbox `covid-19-sharedFiles/data/spatialdata/concordances`. 
- `community_age_assortativity_EX.csv`: available at Dropbox: `covid-19-sharedFiles/data/community/networkBuildingInputs/`. This is a matrix of proportions of contacts for EX (close) community events by age band (e.g. 0.48 of contacts for 0-14 year olds will be with other 0-14 year olds, 0.14 with 15-29 years etc).
- `community_age_assortativity_EV.csv`: available at Dropbox:  `covid-19-sharedFiles/data/community/networkBuildingInputs/`. The matrix of proportions of contacts for EV (casual) community events by age band 
- `WorkOnSite_CPF.csv`: available at Dropbox: `covid-19-sharedFiles/data/workforce/`. A file with the proportion of workplaces that are open at each COVID-19 Protection Framework Setting (i.e Red, Amber, Green) for each industry sector. These are estimates. 
- `census_file.yaml`: available at Dropbox: `covid-19-sharedFiles/data/individuals/`. This is a .yaml file with the counts of individuals within each dwelling size (with higher sizes categorised to size 12+) for each ethnicity and age-band. (ST: I believe the original file for this is `covid-19-sharedFiles/data/IDI_raw_outputs/triples/dwellingTriplesdwellingTriples_V2_nz_safe-Checked.csv`, and was converted to a `.yaml` with suppressed values (-999999) randomly imputed with a value 1:3). 

