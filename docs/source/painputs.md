# The Populated Aotearoa Interaction Network Inputs (PAINputs)

The PAINputs sublibrary serves as a companion sublibrary to PAIN in that it is used to take census data from the New Zealand Integrated Data Infrastructure (IDI) and Populated Aotearoa Interaction Network (PAIN) is a multi-layer, bipartite network representing the population of Aotearoa New Zealand (N ~ 4,700,000), and their interactions within dwellings (N ~ 1,760,000), workplaces (N = 490,000), schools (N = 6,800), and the general community. This sublibrary is used to generate networks from a collection of input files and other specified parameters. 

## Instructions
To generate a network, there are several steps that should be followed. Detailed information on this process can be found in the `docs` folder. The markdown files in `docs` should be read in the following order.

1) `instructions_to_get_input_files.md`: This file provides a summary of all the input files that are required to build a network. Within these instructions it will detail the process of running code to generate node lists for individuals, dwellings, workplaces, and schools, as well as other required input files. Once the input files have been generated, they can be sym-linked to the `input` folder (or manually copied over), and the process of running the network building code can begin. 

2) `yaml_reference.md`: This files provides a summary of the configuration file that is required as an argument to the main network building function. The are a large number of parameters that need to be set, including the specification of node list files (and other input files) as well as additional parameters. This file will describe each part of the configuration file, as well as the dropbox location where required files can be found.

3) `network_code_reference.md`: This file provides a summary of each function that is used in the network building code. This document does not necesarily need to be read, but provides a useful reference when seeking to understand what the code is doing. 

4) `running_network_code.md`: This file provides instructions for running the code to generate a network. 

## Folders
The following section will describe each of the folders present within the repo. 

### `analysis`
This folder contains scripts that can be used to analyse network output. The files can be summarised as follows:

- `network_layer_validation.Rmd`: This notebook validates the structure of a given network comparing against public sources of data (e.g., recorded census populations). Data used for this validation can be found in Dropbox (see notebook for more details).  

- `network_analysis.Rmd`: This notebook provides useful metrics and plots for a given network. This includes summaries of degree distributions for each layer, as well as additional sense checks (e.g. checks for missing data).

- `NumberofContactsSandbox.R`: rough approximation at taking event size distributions (power law settings) and event participation rates (poisson distribution settings) and seeing how they 'play out' for an approximation of the EX layer building process.

- `network_comparison.Rmd`: This notebook takes two different networks and calculates differences between them. For example, the number of nodes/edges in each layer. This is particularily useful for comparing one network with the same network with additional custom edges added. 

- `compare_network_commutes.Rmd`: This notebook takes a network and compares the resulting commuting structure with the original input commute data. This is a useful sense check for determining how well regional connections actually represent the source data. 

### `docs`
The main source of documentation for the repo. See `Instructions` for more details.

### `configs`
The location of the configuration files that are used to generate networks. The config files are in `yaml` format and have several sections specifying the input parameters for the network. This includes the setting the file names of the individual, dwelling, workplace, and school node lists, as well as setting other specific parameters. For more information on this, check `docs/yaml_reference.yaml`, and the example yaml file `default.yaml`. This folder also includes the file `labels.yaml`, which can be used to ensure that variable names that the network building code looks for is kept consistent with the variable names presented within the input data. 

### `input`
This is where all input files to the network building code should be located. 

(ST: It is possible to sym-link input files from Dropbox, someone should update this readme with the code to do that). 

Files produced by `network_group_edgelists` will also get saved here within a folder that will get created called `network_custom_edges`.

### `output`
All output files from the network generation code will go here. These will include:

- A `.pkl` and `.gpickle` file, each containing a networkx object.

- A separate .pkl for each layer, as well as `.csv` files for edges and node lists.

- A readme.txt files with messages from each step of the building process and useful network metrics.

### `new_buildlib`
This folder contains the main code base for building the network. More information on each of the files and functions stored here is available in `docs/network_code_reference.md`.  

### `network_group_edgelists`
This folder contains scripts that take a network and create new custom edges. The files that carry out this process are as follows: 

- `link_education_staff_workplaces.Rmd`: This file takes a network and probablistically links schools to workplaces using an algorithm to find the best match (in terms of workplace and school size, and the potential ratio between them). This will produce an edge list where school workers will be linked to students at a school, as well as other employees. 

- `link_aged_care_facilities_workplaces.Rmd`: This notebook links aged-care facilities (ID present in the dwelling node list) to health care sector workplaces.

- `create_custom_community_events.Rmd`: This notebook contains a function to generate custom group events, given a set of input parameters (e.g. number of events, size, demographics). 

### `refactor sandpit`
This sandpit contains (old) code used when code was refactored. 

### `processing scripts`
This folder contains a set of scripts for extracting useful metrics from a given network. 

### `process_pain.py`
This script runs the function `process_main.py` from `new_buildlib`. (ST: Not clear what this does).

### `generate_pain.py`
This python script is the main call to generate the network. More details/instructions are available in `docs/running_network_code.md`.

### `generate_network.sl`
This is a slurm script for running the code to generate a network on NeSI (this file may be deprecated as it refers to a `generate_network.py` which is no longer present).

### `final_toynetwork_scripts`
This folder contains various scripts for creating toy networks. 

### `example`
This folder contains an example of a configuration file (`default.yaml`). This folder is probably redundant given that the `default.yaml` is now located in `configs`.

### `dev`
This folder is for any snippets of code that are in development/testing and not yet at a stage where it's worth trying to find a place to integrate them into the main code or to justify a dev branch in the repo. It's basically a glorified scratch space.

### `copy_network_script.sh`
This script is a shell script for moving network code files over to NeSI. Very likely deprecated.

### `copy_input.sh`
This script is a shell script for moving input files over to NeSI. Very likley deprecated.

