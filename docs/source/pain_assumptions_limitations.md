# Assumptions, Limitations and Quirks
Here we present a list of modelling assumptions that flow-through into both the code implementation and output. We also summarise some limitations and quirks of the codebase itself, and what effects these have on code output. Some of the (latter) points are limitations that could be turned into substantial developments to the package if users desire (e.g. implementing tertiary education nodes).

## Representative Population
Derived from multiple sources, different time periods (Individuals census 2018. Workplaces – 2019, Schools 2020).
Network Population ~4.7 million, ERP 2021 ~5.1 million.
Network is static, the real world changes over time.

## Student worker proportions – up to 10% difference compared to NZ levels (difficult with suppression). 

## Poor data quality for ECE. 

## Commuting data from census is not perfect – SA2 to SA2 level is missing around 30% of commutes.

## Community layer is designed as a “catch-all” – difficult to accurately represent. 


## Missing links
Schools to workplaces = work in progress.
Community events to workplaces missing, meaning individuals are only connected to colleagues and not clients/customers (we almost get this for free with how community events can draw on individuals based in their workplace SA2).
Dwelling to workplace missing, for example aged-care facilities.

## School layer
Missing Tertiary Education

## Community layer 
– individuals only come from their workplace, school or home SA2s.
– Limited capacity for very large events (potentially needs a dedicated community events node list). 

## Deprivation is not included as an attribute.
Means to provide masks, ventilation etc. across all layers in the network.
