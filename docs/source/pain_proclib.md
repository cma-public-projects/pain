# Proclib
The processing library is a separate submodule within `pains` that is designed to work on the outputted `.gpickle` files from the main `pains.buildlib` network generation library. It outputs text and csv files containing information regarding analysis of the network and its occupants (such as connected components and group connections). It can also relabel group nodes in the existing network.

## Input/Processing yaml and capabilities/output
The processing library takes in a `processing.yml` that primarily contains `True/False` flags as to whether to run each available function within the processing library. These, and any parameters required, are listed below:

- network: Path to the network `.gpickle` to process
- count_groups: Boolean on whether to output a csv counting the number of contacts every individual has through each group type (e.g. home and home_close)
- relabel_nodes: Boolean on whether to relabel the layer ID for certain nodes in the network. The following two parameters (`label1`, `label2`) specify which layer IDs to relabel (e.g. `label1: PM, label2: SC` would relabel all PM nodes as SC). The resultant network after the relabelling is saved as a separate `_label1_to_label2_network.gpickle` file.
- write_sa2_to_sa2_connection_matrix: Boolean on whether to write a connection matrix for the number of connections between each SA2. The following parameter (`write_sa2_to_sa2_layer_list`) is a list of layers to produce a connection matrix for (each saved as a separate csv). If `ALL` is included in the list, a total connection matrix across all layers is also outputted.
- write_community_edgelists: Boolean as to whether to write edgelists for the EX and EV layers. These will be saved as `_EDGELIST_EX.csv` and `_EDGELIST_EV.csv` files.
- extract_analytics: Boolean as to whether to output a `_network_analytics.txt` file containing information about the components and contact matrices of the network. 
- ind_threshold: Integer minimum size of components to summarise.
- age_contact_matrices: Boolean (requires the above to be true) as to whether to add age stratified contact matrices to the network analytics file.
- age_contact_limit: Integer maximum number of each age group to count when constructing age stratified contact matrices.