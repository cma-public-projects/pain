# The config yaml
This document aims to explain what the various parameters in the configuration yamls do and how they influence the construction of the network. It is worth mentioning that how these variables are parsed by the network building code and the functions within are not explored in this document (refer to the [buildlib explanation](pain_buildlib.md) or the [functional documentation](modules.rst)).

The format of this file will include the name of the variable followed by a colon, underneath an indented line with an example and then underneath that an indented line explaining the variable. Note that the examples are all taken from ```configs/tutorial.yaml```.

We note that paths to files can be given as a relative path to where the `generate_pain.py` script will be run from or as the absolute path. It is highly recommended that this path is kept relative so that if migrating the code between environments, no change in the yaml needs to be made.

For more information on what each input file is see the [PAINs input file references](pain_inputs.md).

Information on the nodelist csvs can be found in the documentation for painputs !!!TBD: Link to painputs mds

individuals_file:
    
    individuals_file: "input/individual_nodelist_tutorial.csv"

    This line specifies the path to the file with the attributes of the individuals within the network that is in essence used to construct the individuals layer.

output_directory:

    output_directory: "output/"

    This line specifies where the `generate_pain.py` output will be written to. It will also be treated as the location of network layers for various elements of the network code unless an alternative path is specified.


label_yaml:

    label_yaml: "labels.yaml"

    This is a string specifying the path to the labels file which is used to map the column headings used in the input files to the variable names used in the `pains.buildlib` code.

## DWELLING LAYER VARIABLES

dw_parameters:

    dw_parameters:

    This specifies the parameters which are used in the construction of the dwelling layer of the network. All of the indented variables given here will be used in the construction of the dwelling layer and determine the form that it takes.

generate:

    generate: True

    If this is set to `True` the network code will produce two versions of a new dwelling layer in the specified output_directory. The first version will have the date that the layer was generated and a unique 5 character code to identify it (e.g. `your_set_output_directory/2022_02_13_04WH0_dw.pkl`). The second version will simply be titled `your_set_output_directory/current_dw.pkl`. If this variable is set to False then the dwelling layer will not be generated and any functions which require a dwelling layer will either look for a specified location or will look for `your_set_output_directory/current_dw.pkl`. Along with the dwelling layer, specifying this to True will produce accompanying readmes outlining output metrics from the running of the code and illustrates the output of each step in the dwelling layer construction.

ethnic_assortativity:

    ethnic_assortativity: True 

    If this is set to True the network code will try and use the current composition of a dwelling to inform the ethnicity of the next individual who is placed into it, assuming they match the required assigning criteria. If this is set to False then the ethnic assortativity will be purely influenced by the remaining demographic composition of the SA2 that the dwelling is in.

ethnic_assortativity_weight:

	ethnic_assortativity_weight: 0.9

	This is a floating point value that should be between 0 and 1. A value of 0 is the equivalent of setting "ethnic_assortativity: False". A value of 1 means that ethnic assortativity will always be sought for a dwelling. Values between 0 and 1 set the probability of any individual having their ethnicity chosen using the ethnic composition of the dwelling (otherwise using the remaining demographic composition of the dwelling's SA2).

small_dw_file:

	small_dw_file: "input/small_dwelling_nodelist_tutorial.csv"

	This is a string specifying the path to the small dwellings node list.

large_dw_file:

	large_dw_file: "input/large_dwelling_nodelist_tutorial.csv"

	This is a string specifying the path to the large dwellings node list.

census_matching:

	census_matching: True

	If this is set to True then the age and ethnicity of an individual put into a dwelling will be probabilistically determined from the yaml specified in census_file. This will kick in in a scenario where ethnic assortativity is switched off or the probability check fails. If this is set to False then the individual chosen will be based off of the ethnic composition of the SA2. It is worth mentioning that in spite of this variable name that there is no requirement that the values in the census_file be related to the census information. The age, ethnicity inputs based off of size can be based off of anything that you put into the yaml specified in census_file.

census_file:

	census_file: "input/census_file_tutorial.yaml"

	This variable gives the path to a yaml file specifying the age and ethnicity composition for dwellings. If switching census_matching to True then this input data will be used to probabilistically determine the age and ethnicity of an individual put into a dwelling if ethic assortativity is not used.

age_restriction:

	age_restriction: True

	If this is switched to True then the first person put into a dwelling will not be in the age bracket 0-14 if there is another option available. The underlying principle behind this is to limit the number of dwellings which exclusively house minors.

clone_threshold:

	clone_threshold: 100

	Once all dwellings are filled the code creates duplicates of filled dwellings and populates them with unhoused individuals. The variable here gives a maximum number of times the code will attempt to find a suitable dwelling within a SA2 to duplicate before giving up on that SA2. It is worth mentioning that setting this above a certain number will not increase the time it takes to run. The reason for this is that if the cloned dwelling cannot be filled it is removed from the list of available dwellings until there are no options remaining.

## WORKPLACE LAYER VARIABLES

wp_parameters:

	wp_parameters:

	This specifies the parameters which are used in the construction of the workplace layer of the network. All of the indented variables given here will be used in the construction of the workplace layer and determine the form that it takes.

generate:

	generate: True

	If this is set to True the network code will produce two versions of a new workplace layer in the specified output_directory. The first version will have the date that the layer was generated and a unique 5 character code to identify it (e.g. `your_set_output_directory/2022_02_13_04WH0_wp.pkl`). The second version will simply be titled `your_set_output_directory/current_wp.pkl`. If this variable is set to False then the workplace layer will not be generated and any functions which require a workplace layer will either look for a specified location or will look for `your_set_output_directory/current_wp.pkl`. Along with the workplace layer, specifying this to True will produce accompanying readmes outlining output metrics from the running of the code and illustrates the output of each step in the workplace layer construction.

sector_file:

	sector_file: "input/df_census_and_IR2018_ta_safe_backfilled_tutorial.csv"

	This is a string specifying the path to a file indicating the counts of age, sex, ethnicity triple for a given workplace sector in a given TA. This data is used in conjunction with the commute data to choose an individual for a workplace. 

sector_suppressed_probs:

	sector_suppressed_probs:
      1: 0.6                                                  
      2: 0.2
   	  3: 0.1
      4: 0.05
      5: 0.05

	This is a nested dictionary to provide the "new value":"relative probability of this value appearing" for the suppressed values within the sector file.

commute_file:

	commute_file: "input/commute_work_sa2_tutorial.csv"

	This file gives the counts of people commute from a SA2 they dwell in to a SA2 they work in. These counts are converted into a probability which is used in conjunction with the sector information to choose the age, sex, ethnicity and sa2 of residence for a worker in a workplace.

wp_file:

	wp_file: "input/workplace_nodelist_tutorial.csv"

	This is a string giving the path to the file containing the workplace ID, SA2, TA, workplace sector and worker count of each workplace used in the network. 

maximum_trials:

	maximum_trials:
      Step_1: 20
      Step_2: 20
      Step_3: 20
      Step_4: 20

	This gives the maximum number of attempts that each step in the workplace building will use to find a worker for a workplace before the code will give up and find another workplace. Given that the only reason that this step will fail is if a worker is already in the workplace the trial counts can be kept low.

## SCHOOL LAYER VARIABLES

sc_parameters:

	sc_parameters:

	This specifies the parameters which are used in the construction of the school layer of the network. All of the indented variables given here will be used in the construction of the School layer and determine the form that it takes.

generate:

	generate: True

	If this is set to True the network code will produce two versions of a new school layer in the specified output_directory. The first version will have the date that the layer was generated and a unique 5 character code to identify it (e.g. `your_set_output_directory/2022_02_13_04WH0_sc.pkl`). The second version will simply be titled `your_set_output_directory/current_sc.pkl`. If this variable is set to False then the school layer will not be generated and any functions which require a school layer will either look for a specified location or will look for `your_set_output_directory/current_sc.pkl`. Along with the school layer, specifying this to True will produce accompanying readmes outlining output metrics from the running of the code and illustrates the output of each step in the school layer construction.
	
ece_file:

	ece_file: "input/ECE2019_nodelist_tutorial.csv"

	This is a string giving the path to the early childhood education centres nodelist file.

school_file:

	school_file: "input/school2019_nodelist_tutorial.csv"

	This is a string giving the path to the schools nodelist file.

commute_file:

	commute_file: "input/commute_ed_prop_sa2_tutorial.csv"

	This string gives the path to the file which gives the a SA2 of a school and a SA2 of residence and the counts of people who commute between them. This is used to determine the SA2 of residence of a student in a school.

school_characteristics:

	school_characteristics: "input/school_age_breakdown_tutorial.csv"

	This string points to a csv which gives the percentage composition for of people in the 0-14 and 15-29 age brackets for the different school types.

## COMMUNITY LAYER VARIABLES
	
community_parameters:

	community_parameters:

	This specifies the parameters which are used in the construction of the EX and EV community layers of the network. The indentation here is used here is more complex than for the other network layers because whilst the variables used for the construction of the layers is different the actual functions used to generate them are the same. To rectify this this section has 2 subsections, the first being to specify the alteration of the input nodelist for use in both the EX and EV layers and the other being the EX/EV layer specific parameters. We note that the EX/EV layer specific parameters are separate within the yaml, but the parameters are all the same/have the same effects so are combined here for brevity.

commute_nodelist:

	commute_nodelist:

	This section of the yaml specifies the input for the community layers where various network layers in pickle form are read in with the individuals nodelist and are converted into a dictionary for use in the construction of the individuals node list. All variables until ex_parameters are parts of the generation of the input nodelist with short range commuting.

wp_commute:
	
	wp_commute: True

	This is a boolean specifying whether or not the pickled workplace layer should be read into the community layer building. If set to True there is a probability that an individuals short range link in both the EX and EV layer will be in their SA2 of work as opposed to their SA2 of residence.

sc_commute:

	sc_commute: True

	This is a boolean specifying whether or not the pickled school layer should be read into the community layer building. If set to True there is a probability that an individuals short range link in both the EX and EV layer will be in their SA2 of school as opposed to their SA2 of residence.

read_wp:

	read_wp: False

	This is a boolean specifying whether or not a specific work layer should be read in to the network code. If this is set to True a workplace layer specified in wp_file will be read in. Otherwise the file current_wp.pkl sitting in the directory specified in output_directory will be used.

wp_file:

	wp_file: "/path/to/wp/file/" 

	This is a string specifying the path to the pickled workplace file if read_wp is set to True. If read_wp is set to False this string is ignored.

read_sc:

	read_sc: False

	This is a boolean specifying whether or not a specific school layer should be read in to the network code. If this is set to True a school layer specified in sc_file will be read in. Otherwise the file current_sc.pkl sitting in the directory specified in output_directory will be used.

sc_file:

	sc_file: "/path/to/sc/file/"

	This is a string specifying the path to the pickled school file if read_sc is set to True. If read_sc is set to False this string is ignored.

sa2_ta_concordance_file:

	sa2_ta_concordance_file: "input/SA2_TA18_concordance_tutorial.csv"

	This is a string specifying the path to a SA2/TA concordance file. This is used for getting the TA of a commute SA2 in the event that the commute crosses a TA boundary.

ex/ev_parameters:

	ex/ev_parameters:

	This is the section used to generate the input parameters for the generation of the EX/EV layer. We note that the parameter labels for both community layers (ex and ev) are the same, and so we combine the descriptions for both of them here. We also note that the provided values are from the ex_parameters listed in the tutorial yaml.

generate:

	generate: True

	This is a boolean when if set to True will generate an EX (close)/EV (casual) community layer of the network. Two EX/EV layers will be generated if this is set to True and put in the output directory. If this is set to True the network code will produce two versions of a new ex/ev layer in the specified output_directory. The first version will have the date that the layer was generated and a unique 5 character code to identify it (e.g. `your_set_output_directory/2022_02_13_04WH0_ex.pkl`/`your_set_output_directory/2022_02_13_04WH0_ev.pkl`). The second version will simply be titled `your_set_output_directory/current_ex.pkl`/`your_set_output_directory/current_ev.pkl`. If this variable is set to False then the ex/ev layer will not be generated and any functions which require a ex/ev layer will either look for a specified location or will look for in the output directory for current_ex.pkl. Along with the ex layer, specifying this to True will produce accompanying readmes outlining output metrics from the running of the code and illustrates the output of each step in the EX/EV layer construction.

maximum_size:

	maximum_size: 100

	This is an integer specifying the maximum number of people in a EX event. 

minimum_size:

	minimum_size: 2

	This is an integer specifying the minimum number of people in a EX/EV event. It is worth mentioning that setting this to 1 will create community events of one individual (i.e. the individual would have no connections through this event context).

power_law:

	power_law: 3.2 

	This is a floating point value specifying the power law value used in the power law distribution. This will inform the distribution of values which fall between the maximum_size and the minimum_size. See the [powerlaw package](https://github.com/jeffalstott/powerlaw) for more information on the distribution.

age_assortativity:

	age_assortativity: True

	This is a boolean specifying whether or not age assortativity is switched on in the construction of the EX/EV layer If this is set to True the csv specified in age_parameters will be used to determine the ratio of contacts each age bracket has to other age brackets and this will be used in the filling of community events. If this is set to False the age composition of the SA2 in which the event is is used to determine the age composition of the community events.

age_parameters:

	age_parameters:  "input/community_age_assortativity_EX.csv" (dropbox: covid-19-sharedFiles\data\community\networkBuildingInputs)

	This is a string specifying the path to the age assortativity csv which is used to determine the ratio of contacts between the different age brackets. If age_assortativity is set to True this variable is used to specify the age breakdown of community events. If age_assortativity is set to False this parameter is ignored.

participation:

	participation:
      0-14: 2.07
      15-29: 2.88
      30-59: 1.93
      60+: 1.15

	This is a floating point value being used to determine the average number of events people in an EX event participate in broken down by age. This provides the expected value of a shifted Poisson distribution (shifted up by 1 so that individuals all have at least one community event). It is worth noting that setting this value to 1 will break the code.

long_accept:

	long_accept:
      0-14: 0.25
      15-29: 0.5
      30-59: 0.5
      60+: 0.5
	
	This is a floating point value between 0 and 1 specifying the fraction of long range links from the individuals node list which are accepted for each age bracket. Setting this to 0 will mean that individuals in that age bracket have no long range links and all inter-TA community commuting will be from the work and school layers.

ethnic_assortativity:

	ethnic_assortativity:

	This is a set of parameters specifying the enforcement of ethnic assortativity in the community layer. At a broad level ethnic assortativity probabilistically chooses the ethnicity of an individual based off the current ethnic composition of a community event. In the absence of ethnic assortativity the ethnicity of an individual placed into a community event is dependent on the ethnic composition of the SA2.

	enforce: True

	If this is set to True ethnic assortativity is enforced in the community layer of the network. A random floating point number between 0 and 1 is chosen and if it is less than the value specified in threshold then the ethnicity of the next individual to be put into an event is probabilistically determined by the composition of the event.

	threshold: 0.8

	This is a floating point value that should be between 0 and 1. A value of 0 is the equivalent of setting "enforce: False". A value of 1 means that ethnic assortativity will always be sought for a community event. Values between 0 and 1 set the probability of any individual having their ethnicity chosen using the ethnic composition of the community event (otherwise using the ethnic composition of the event's SA2).

include_long_range:

	include_long_range: True

	This is a boolean determining whether or not long range links are included in the community layer. If set to True than the long range links are included in the community layer. If set to False then the only inter TA links in the community layer will be due to the school and work commuting

offset_long_range:

	offset_long_range: True

	This is a boolean determining whether or not an individuals short range links are offset by the number of short range links. If the number of short range links an individual has is greater than their short range links then no short range links will be generated. If the average participation is low we recommend setting this to False.

trial_threshold:

	trial_threshold: 10 

	This is an integer determining the number of times the code will attempt to select an individual to put into a community event. The reason for this is that individuals participate in multiple community events. So the selection process has a risk of putting the same person into the same event. If the trial threshold number of attempts to find an individual is met the event is closed and no more individuals are put into it.

## MERGE LAYER VARIABLES

merge_parameters:

	merge_parameters:

	These are the parameters that are used for the merging the pickled dictionaries representing the network layers into one dictionary ready to be converted into a Networkx object.

generate:

	generate: True

	This is a boolean specifying whether or not the network layers should be merged.

layers_to_include:

	layers_to_include:
	  - "DW"
	  - "WP"
	  - "SC"
	  - "EX"
	  - "EV"

	This is a list specifying which 'layers' to include in the final merged network. This uses the two letter code that matches the `read_*` and `*_layer` variables that follow, and the 5 options in the above list are currently the only options without changing the `merge_main.py` code (and other places).

read_dw:

	read_dw: False

	This is a boolean specifying whether or not a specific DW layer should be read into the merging process. If this is set to True then the program will use the string specified in `dw_layer` as the path to the DW layer. If this variable is set to False then the script will look in the specified output directory for the file `your_set_output_directory/current_dw.pkl`.

dw_layer:

	dw_layer: "/path/to/dw/file/"

	This is a string pointing to the DW file to be used in the network merging process if read_dw is set to True. If read_dw is set to False then this string is ignored.

read_wp:

	read_wp: False

	This is a boolean specifying whether or not a specific WP layer should be read into the merging process. If this is set to True then the program will use the string specified in `wp_layer` as the path to the WP layer. If this variable is set to False then the script will look in the specified output directory for the file `your_set_output_directory/current_wp.pkl`.

wp_layer:

	wp_layer: "/path/to/wp/file/"

	This is a string pointing to the WP file to be used in the network merging process if read_wp is set to True. If read_wp is set to False then this string is ignored.

read_sc:

	read_sc: False

	This is a boolean specifying whether or not a specific SC layer should be read into the merging process. If this is set to True then the program will use the string specified in `sc_layer` as the path to the SC layer. If this variable is set to False then the script will look in the specified output directory for the file `your_set_output_directory/current_sc.pkl`.

sc_layer:

	sc_layer: "/path/to/sc/file/"

	This is a string pointing to the SC file to be used in the network merging process if read_sc is set to True. If read_sc is set to False then this string is ignored.

read_ex:

	read_ex: False

	This is a boolean specifying whether or not a specific EX layer should be read into the merging process. If this is set to True then the program will use the string specified in `ex_layer` as the path to the EX layer. If this variable is set to False then the script will look in the specified output directory for the file `your_set_output_directory/current_ex.pkl`.

ex_layer:

	ex_layer: "/path/to/ex/file/"

	This is a string pointing to the EX file to be used in the network merging process if read_ex is set to True. If read_ex is set to False then this string is ignored.

read_ev:

	read_ev: False

	This is a boolean specifying whether or not a specific EV layer should be read into the merging process. If this is set to True then the program will use the string specified in `ev_layer` as the path to the EV layer. If this variable is set to False then the script will look in the specified output directory for the file `your_set_output_directory/current_ev.pkl`.

ev_layer:

	ev_layer: "/path/to/ev/file/"

	This is a string pointing to the ev file to be used in the network merging process if read_ev is set to True. If read_ev is set to False then this string is ignored.

custom_layers:

	custom_layers: False

	This is a boolean specifying whether or not a custom layer is to be included in the final network (for example linking schools to dwelling nodes).

custom_parameters:

	A dictionary of parameters for each custom layer to add to the network

	1:
	  nodelist:
	  edgelist:

	For example, this dictionary would add one custom layer to the network that could either be defined by a path to a custom edgelist or a custom edgelist and a custom nodelist.

link_groups:

	link_groups: False

	This is a boolean specifying whether to link group nodes together (i.e. link individuals in one group node to individuals in another group node). If True then the code will add edges between the edgelists specified in the `link_group_edgelists` list.

link_group_edgelists:

	link_group_edgelists:
	- 'input/tmp_link.csv'

	If `link_groups` is True, then all edgelist `csvs` included in this list will be read in and edges added between the specified group nodes. If `link_groups` is False then this parameter is ignored.

individual_attributes:

	individual_attributes:
	  TA:
		column_heading: "TA2018"
		include: True
		type: "int"
	  SA2:
		column_heading: "SA22018"
		include: True
		type: "int"
	  sex:
		column_heading: "sex"
		include: True
		type: "str"
	  age:
		column_heading: "age_band"
		include: True
		type: "int"
	  ethnicity:
		column_heading: "ethnicity"
		include: True
		type: "str"
	  vaccine_status:
		column_heading: "vaccine_status"
		include: False
		type: "int"
	  vaccine_dates:
		column_heading: "vaccine_dates"
		include: True
		type: "list-int"

	The individual attributes is probably the most complex inclusion in the yaml. It determines which characteristics from the individuals nodelist should be included in the merged network. The name of the dictionary (e.g. TA, SA2, sex, age, ...) is the name of the variable that the merged network and ultimately pickled network will have. The column heading variable is a string indicating the name of the column heading in the individuals node list. The include variable is a boolean specifying whether or not the variable should be included. The type variable defines the type of the resultant variable in the attributes of the nodelist i.e. "int" for integer, "str" for string, "float" for floating point value, "list-str" for a list of strings, "list-int" for a list of integers and "list-float" for a list of floats.

	Important: The code will fail to merge if an attribute is set to `True` but is not present as a column in the individual nodelist.


## CREATE NETWORKX OBJECT VARIABLES

nx_parameters:

	nx_parameters:

	These are the parameters which are used to covert a merged dictionary into a Networkx object. Until special_selection the following variables are used to form a merged network. Unlike the other steps there will be no current_network.gpickle file because this is a recipe for disaster. Instead only one object will be created with the date and a unique five character string (e.g. `2022_05_25_NEWQ0_network.gpickle) in the specified output directory

generate:

	generate: True 

	This is a boolean specifying whether or not a Networkx object will be generated at all. If this is set to True a `.gpickle` object will be generated. 

read_network:

	read_network: False

	This is a boolean specifying whether or not a specific network should be read in. If this is set to True then the string specified in network_file: will be treated as the path to the pickled merged network to be converted into a Networkx object. If this is set to False then the script will look in the specified output directory for the file `your_set_output_directory/current_network.pkl` and use this for the generation of the Networkx object.

network_file:

    network_file: "/path/to/network/file/"

    This is a string specifying the relative or absolute path to a merged network pickle object. This string will be ignored if read_network is set to False.

workplace_shutdown:

    workplace_shutdown: True

    This is a boolean specifying whether or not different levels of workplace shutdown should be incorporated into the final network. If this is set to True the file used in "workplace_shutdown_file:" will be used to shut down workplaces until the number of open workplaces matches the specifications

workplace_shutdown_file:

    workplace_shutdown_file: "input/WorkOnSite_CPF_tutorial.csv"

    This is a string specifying the path to the workplace shutdown file. If "workplace_shutdown" is set to False then this string will be ignored in the construction of the Networkx object.

workplace_shutdown_use_workers:

    workplace_shutdown_use_workers: True

    This is a boolean specifying whether or not worker count should be used for workplace shutdown. If set to True then the number of workers working on site will be the threshold used to match to the values specified in the workplace shutdown file. If it is set to False then the number of open or closed workplaces will be used. Because the size distribution of workplaces used can have a very heavy tail it is recommended to keep this value set to True.

workplace_shutdown_headings:

    workplace_shutdown_headings:
      - "GREEN"
	  - "AMBER"
	  - "RED"

    This is a list specifying the column headings for the workplace shutdown file so that different types of workplace intervention can be used (for example traffic light system vs alert levels). It is crucial for this to work properly that the headings are given in descending order of open workplaces. If green has more workplaces open than red then green must be specified first in the list.

## SPECIAL SELECTION

The parameters here have a very specific function, which is to create a network in a certain region. The way that each of these functions work is by modifying the exclude TAs list.

special_selection:
    
	auckland: False
    wellington: False
  	christchurch: False
  	north_island: False
  	south_island: False

	Whether to just include certain cities in the network.

custom:

    custom: True

    This is a Boolean which if set to True will modify the excluded TAs and excluded SA2s list as appropriate to create a network using the specified TAs or SA2s

custom_variables:

    custom_variables:

    This is a dictionary which specifies the selected TAs and selected SA2s to be included in the network. If custom is set to False then this dictionary will be ignored

selected_ta:

    selected_ta:
      - 76

    This is a list of Tas to include in the network if custom variables is set to True. Every Ta not included in this list is added to to excluded TA list. It is important to note that if custom is set to True and this list is empty no network will be generated.

excluded_sa2:

    excluded_sa2: []

    This is a list of SA2s to exclude from the network. For a full New Zealand network it is highly recommended that this list is kept empty.

excluded_ta:

    excluded_ta:
      - 67

    This is a list of TAs which are to be excluded from the network construction. For all networks, it is highly recommended that TA 67 is included in this list as this includes the Chatham Islands.

## CLOSE-CONTACT PARAMETERS

cc_parameters:

    cc_parameters:

    This specifies the parameters which are used in the construction of the close-contact groups of the network. All of the indented variables given here will be used in the construction of close-contact groups and determine the forms that they take.

generate:

    generate: True

    If this is set to `True` the network code will produce new versions of the dwelling, workplace and school layers in the specified output_directory. These will have so called 'close-contact' groups formed from the existing groups in the existing layers. These groups can be considered as smaller 'sub-groups' that would naturally occur within a larger interaction context. For example, in a workplace might consist of 100 individuals (who are form the full group) but any certain individual is likely to only interact 'casually' with these individuals (e.g. passing them at the water cooler or in the elevator) and are far more likely to interact 'closely' with individuals within their team (e.g. ones they share an office or break-room with). For each layer label specified in the yaml, the close-cover code relabels all group nodes with that prefix that have a size smaller than the `threshold`. If a group size is larger than the `threshold`, close-contact groups are added to the network with size drawn from a lognormal distribution with parameters `mean` and `sigma`. Then additional close-cover groups with number/probability proportional to `overcover` that follow the same size distribution. New nodelists, edgelists, readmes, `.pkl`s and and a `.gpickle` with a `.full` suffix are also created including the new close-cover groups.

output_path:

    output_path: ~

    If this is set to a non-empty value then the new close-cover output files will be saved in this directory. If it is less empty than the output files will be placed in this same location as the existing `.gpickle` file.

save_full_lists:

	save_full_lists: True

	A boolean

"DW"/"WE"/"WP"/"SC"/"PM":

	These provide a dictionary of parameters to be used for each group type given (DW, WE, WP, SC, PM). The parameter names and functions are the same for each dictionary so are combined here for brevity. We note that the example values provided are for the "DW" dictionary in the tutorial yaml.

threshold: 
	
	threshold: 20 

	This number is the minimum size of a group before it will have close-contact groups added for its individuals. For example, groups of size 19 and less will not have close-contact groups added to the network for it and will instead just be relabelled. Groups of size 20, 21, ..., will lead to close-contact subgroups being added to the network that have total size equal to the original group size. Overcover groups then may also be added to the network with individuals from the original group.

mean: 
	
	mean: 15

	This provides the average of the lognormal distribution used for close-contact group sizes.

sigma: 

	sigma: 0.1

	This provides the standard deviation of the lognormal distribution used for close-contact group sizes.

overcover: 

	overcover: 0.2

	Proportional to the number/probability of additional 'over-cover' groups to be added to groups greater than the `threshold`. The exact number added is given by `floor(overcover * group_size / mean)` where `group_size` is the size of the original group.