## default input file locations

## below are the defaults in the git repo. this should probably not change unless there is an IDI data update, or a code refactor

##  for individual nodelist
input_filepath_list<- list(
  
  #dwelling composition (size and age structure) data
  dwelling_comp_nz_loc = here("source_data","IDI_outputs","DwellingStructure","dwelling_composition_V2_nz_safe - Checked.csv"),
  dwelling_comp_ta_loc = here("source_data","IDI_outputs","DwellingStructure","dwelling_composition_V2_ta_safe - Checked.csv"),
  dwelling_comp_sa2_loc = here("source_data","IDI_outputs","DwellingStructure","dwelling_composition_V2_sa2_safe - Checked.csv"),
  
  
  #dwelling size distribution for different age/sex/ethnicities
  dwelling_triples_nz_loc = here("source_data","IDI_outputs","dwellingComposition","dwellingTriples_V2_nz_safe-Checked.csv"),
  dwelling_triples_ta_loc = here("source_data","IDI_outputs","dwellingComposition","dwellingTriples_V2_ta_safe-Checked.csv"),
  dwelling_triples_sa2_loc = here("source_data","IDI_outputs","dwellingComposition","dwellingTriples_V2_sa2_safe-Checked.csv"),
  
  #aged care data
  agecare_clean_loc = here("source_data","AgedCareFacilities_clean.csv")
  
)

