
join_and_expand_individual_nodelist <- function(layers) {
  
  # create new version of triples_SA2_individuals that can have columns joined to it
  triples_SA2_withinfo <- triples_SA2_individuals
  
  if("dwelling" %in% layers_to_build){
    triples_SA2_withinfo <- triples_SA2_withinfo %>%
      left_join(triples_SA2_dwellingAssigned, by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      left_join(triples_SA2_smallDwellings, by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      replace_na(list(count_dwellingAssigned = 0, count_smallDwellings = 0))
    
  }

  if("school" %in% layers_to_build & "work" %in% layers_to_build){
    triples_SA2_withinfo <- triples_SA2_withinfo %>%
      left_join(triples_SA2_workEducation, by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      replace_na(list(count_workEducation = 0))
    
  }else if("work" %in% layers_to_build){
    triples_SA2_withinfo <- triples_SA2_withinfo %>%
      left_join(triples_SA2_workforce, by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      replace_na(list(count_workers = 0))
    
  }else if("school" %in% layers_to_build){
    triples_SA2_withinfo <- triples_SA2_withinfo %>%
      left_join(triples_SA2_education, by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      replace_na(list(count_students = 0))
    
  }
  
  
  #Now expand rows to make a single row for each individual. This will (for now) just have SA2/age/sex/ethnicity level counts for all other attributes
    individual_nodelist <- triples_SA2_withinfo %>%
      #first filter out any where count==0
      filter(count!=0) %>%
      # mutate(count=as.numeric(count))
      #first take the count and disaggregate, so each row represents an individual. (keep the count column)
      uncount(weights = count,.remove = F) %>%
      select(-count)
    
    #Add dwelling attributes to individuals
    
    if("dwelling" %in% layers_to_build){
      # Assign dwelling attributes to individuals
    
      individual_nodelist <- individual_nodelist %>%
        #Since people in small dwellings must have dwelling assigned == Y, we will create dwelling assigned first and then make sure 
        #we only allocate small dwelling == Y when dwelling assigned == Y. 
        group_by(SA22018,age_band,sex,ethnicity) %>%
        #add dwelling assigned attribute randomly (this will shuffle so that it is not in ID order)
        mutate(dwellingAssigned = sample(c(rep("Y",first(count_dwellingAssigned)),
                                           rep("N",n()-first(count_dwellingAssigned))
                                           ), n(),replace=FALSE)) %>%
        ungroup() %>%
        #add small dwelling attribute (need to regroup with dwelling assigned so we only have small_dwelling == Y when dwelling_assigned == Y)
        group_by(SA22018,age_band,sex,ethnicity,dwellingAssigned) %>%
        #add small dwelling attribute
        mutate(smallDwelling = ifelse(dwellingAssigned == "N", "N", #if dwelling assigned is N, make small dwelling N
                                      sample(c(rep("Y",first(count_smallDwellings)), #otherwise assign Y with sample
                                               rep("N",n()-first(count_smallDwellings))
                                               ), n(),replace=FALSE))
               ) %>%
        ungroup()  %>%
        select(-count_dwellingAssigned,-count_smallDwellings)
      
      
    }
    
    #Add work and/or education attributes to individuals
    
    if("work" %in% layers_to_build & "school" %in% layers_to_build){
      
      #Assign student/worker attributes to individuals
      individual_nodelist <- individual_nodelist %>%
        ## sample from the options of work/student combinations for each SA2/age/sex/ethnicity combination (slow - there is definitely a much better way of doing this)
        group_by(SA22018,age_band,sex,ethnicity) %>%
        mutate(work_ed_status = sample(c(rep("Y_Y",first(count_workY_edY)),
                                         rep("Y_N",first(count_workY_edN)),
                                         rep("N_Y",first(count_workN_edY)),
                                         rep("N_N",first(count_workN_edN))),
                                        n(),replace=FALSE)
        ) %>%
        ungroup() %>%
        separate(work_ed_status, c("worker","student")) %>%
        select(-count_workY_edY,-count_workY_edN,-count_workN_edY,-count_workN_edN)
        
      ## quick manual check that the right number of Y/N combinations was created:
      # individual_nodelist_test <- individual_nodelist %>%
      #   group_by(SA22018,age_band,sex,ethnicity,count_workY_edY,count_workY_edN,count_workN_edY,count_workN_edN,worker,student) %>%
      #   summarise(count_workEducation = n()) %>%
      #   ungroup()
        
      
        
    }else if("work" %in% layers_to_build ){
      
      #Assign worker attributes to individuals
      individual_nodelist <- individual_nodelist %>%
        ## sample count_workers number of workers for each SA2/age/sex/ethnicity combination (slow - there is definitely a much better way of doing this)
        group_by(SA22018,age_band,sex,ethnicity) %>%
        mutate(worker = sample(c(rep("Y",first(count_workers)),
                                         rep("N",n() - first(count_workers))),
                                       n(),replace=FALSE)
        ) %>%
        ungroup() %>%
        select(-count_workers)
      
    }else if("school" %in% layers_to_build){
      #Assign student attributes to individuals
      individual_nodelist <- individual_nodelist %>%
        ## sample count_students number of students for each SA2/age/sex/ethnicity combination (slow - there is definitely a much better way of doing this)
        group_by(SA22018,age_band,sex,ethnicity) %>%
        mutate(student = sample(c(rep("Y",first(count_students)),
                                 rep("N",n() - first(count_students))),
                               n(),replace=FALSE)
        ) %>%
        ungroup()%>%
        select(-count_students)
      
    }
    
  return(individual_nodelist)
}
