
## this code is designed to be run from `construct_dwelling_nodelist.Rmd`

#We have extracted data on the size of dwellings that people live in for the full combination of age (15-29,30-59,60+), sex(M,F), and ethnicity (M&P, M, P, other). 
#Some of these cells are suppressed - as they must be any time there are fewer than 6 people who match those categories.
#Furthermore, the data we have pulled out is RR3 rounded

## On further investigation we can see that the 'mean per suppressed cell' is >>5, this indicates that there has been some secondary suppression or similar, 
## and that perhaps we should be filling some of the NA cells in with values>>5 (rather than sampling between 1 and 5)
## either that, or there is an error in the NZ level counts, or the TA level counts that are not suppressed

## we have information independently about the counts of the different SA2/age/sex/ethnicity combinations that are in 'large dwellings' by taking the difference 
## between dwellingAssigned and smallDwellings but do not use this here for now. Just accept that we won't have the 'right' mix of people in dwelling sizes in some of those 100+ ones.

get_backfilled_dwellingSizes <- function(filepath_nz,
                                         filepath_ta,
                                         filepath_sa2,
                                         concordance_sa2_ta,
                                         save_intermediate_outputs=FALSE){
  require(here)
  require(tidyverse)
  # scipen=999
  
  # filepath_nz=input_filepath_list$dwelling_triples_nz_loc
  # filepath_ta=input_filepath_list$dwelling_triples_ta_loc
  # filepath_sa2=input_filepath_list$dwelling_triples_sa2_loc
  # concordance_sa2_ta=concordance
  # save_intermediate_outputs=save_intermediate_outputs
  
  #Data that we have to fill in from, is that we have the same counts at TA and NZ levels. 
  
  #Loading in the triples data, we make sure that ethnicity and sex are factors, 
  #with the order we would like (so that it is consistent in all datasets - SA2, TA, and NZ), 
  #Suppressed cells are coded as '999999', since we can't take NA out of the IDI. We find these and change them to NA.
  
  # loading triples
 
  #loading and tidying formatting - including creating factors for categorical variables in a consistent manner
  triples_NZ <- read_csv(filepath_nz,
                         col_types = cols(
                          ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
                          age_band = col_character())
                         ) %>%
    select(- `...1`) %>%
    mutate(sex = ifelse(sex == 'MF', "X", sex)) %>%
    #we also got count of NA, which we don't need here
    rename(NoDwelling = "NA")  %>% select(-NoDwelling) %>%
    #pivot long
    pivot_longer(cols = c("01":"11","c012-019","c020-049","c050-099","c100+"), values_to = "triple_count", names_to = "dwl_size") %>%
    # remove small dwellings
    filter(dwl_size %in% c("c020-049","c050-099","c100+"))
  
  # fill in missing values with NA 
  triples_NZ <- triples_NZ %>%
    #filter out 0 counts (-999), keep NAs
    filter(triple_count != "-999")   %>%
    #For NZ level, we will fill in missing randomly from 1 to 5 using log normal distribution with mean of 2
    rowwise() %>%
    mutate(triple_count = ifelse(triple_count=='-999999',min(max(round(rlnorm(n=1, 0.55, 0.55)),1),5),triple_count))  %>% ungroup()
  
  # #TA level
  triples_TA <- read_csv(filepath_ta,
                         col_types = cols(
                           ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
                           age_band = col_character())
                         ) %>%
    select(- `...1`) %>%
    mutate(sex = ifelse(sex == 'MF', "X", sex)) %>%
    #we also got count of NA, which we don't need here
    filter(!is.na(dwl_size))  %>%
    # remove small dwellings
    filter(dwl_size %in% c("c020-049","c050-099","c100+")) %>%
    #dwl_size isn't consistent with NZ, so clean this to make it so (single numbers should be padded to length 2)
    mutate(dwl_size = ifelse(nchar(dwl_size) == 1, str_pad(dwl_size,width = 2,side = "left", pad = "0"),dwl_size)) %>%
    #rename TA col
    rename(TA2018 = "ur_ta")
  
  # fill in missing values with NA 
  triples_TA <- triples_TA %>%
    #filter out 0 counts (-999), keep NAs
    filter(triple_count != "-999")   %>%
    mutate(triple_count = ifelse(triple_count=='-999999',NA,triple_count))                       
  
  
  #SA2 level
  triples_SA2 <- read_csv(filepath_sa2,
                          col_types = cols(
                            ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
                            age_band = col_character())
                          ) %>%
    select(- `...1`) %>%
    #if sex is MF (combined), change to 'X'
    mutate(sex = ifelse(sex == 'MF', "X", sex)) %>%
    #dwl_size isn't consistent with NZ, so clean this to make it so (single numbers should be padded to length 2)
    mutate(dwl_size = ifelse(nchar(dwl_size) == 1, str_pad(dwl_size,width = 2,side = "left", pad = "0"),dwl_size)) %>%
    #we also got count of NA, which we don't need here
    filter(!is.na(dwl_size))  %>%
    # remove small dwellings
    filter(dwl_size %in% c("c020-049","c050-099","c100+")) %>%
    #rename SA2 col
    rename(SA22018 = "ur_sa2")
  
  # fill in missing values with NA 
  triples_SA2 <- triples_SA2 %>%
    #filter out 0 counts (-999), keep NAs
    filter(triple_count != "-999")   %>%
    mutate(triple_count = ifelse(triple_count=='-999999',NA,triple_count)) 
  
  #One thing we noticed straight away was that there was a code TA code 888, 
  #and SA2 code 888888 which we didn't recognise. 
  #On investigation it seemed that these were people with no usual residence in NZ (tourists??) 
  #and we found that none of these individuals had a work or education listed in NZ either.
  
  
  # get TA 888 data -  hopefully 0 counts.
  if(nrow(triples_TA %>%
          filter(TA2018=='888')
          )>0){warning("row with TA888 exists in data")}
  
  # get SA2 999999 data -  hopefully 0 counts.
  if(nrow(triples_SA2 %>%
          filter(SA22018=='999999')
          )>0){warning("row with SA2 999999 exists in data")}
  
  
  
  #### Looking for missing data
  
  #Aggregating the TA to NZ level to look at total populations missing,NB when 'missing_xxlevel' is negative, 
  #there are too many people at the next level down i.e. summing up SA2s to TA level gives you more than the TA level count.
  
  #summing up to TA and NZ level to compare
  
  
  # create a column that is the number of suppressed cells 
  # create a column that tells you whether (for that triple) any of the TA level counts were suppressed (==NA)
  # then sum up SA2 counts to TA level (ignoring NAs)
  
  
  triples_NZ_fromTA <- triples_TA %>%
    group_by(age_band,ethnicity,sex,dwl_size) %>%
    mutate(suppressed_TA_number = sum(is.na(triple_count))) %>%
    mutate(suppressed_TA = if_else(is.na(sum(triple_count, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(age_band,ethnicity,sex,dwl_size,suppressed_TA,suppressed_TA_number) %>%
    summarise(triple_count_fromTA = sum(triple_count, na.rm = TRUE)) %>%
    ungroup()
  
  
  # find missing values, and reconstruct known NA from zeros
  
  comparison_NZ_level <- triples_NZ %>%
    right_join(triples_NZ_fromTA,  by = c("age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(missing_NZlevel = triple_count-triple_count_fromTA) %>%
      mutate(triple_count_fromTA = case_when(
      triple_count_fromTA==0 & suppressed_TA=='No' ~ 0,
      triple_count_fromTA==0 & suppressed_TA=='Yes' ~ 999999,
      TRUE ~    triple_count_fromTA
        )) %>%
      mutate(triple_count_fromTA = ifelse(triple_count_fromTA==999999,NA,triple_count_fromTA))
  
  #Now looking at TA level from SA2 counts and NZ level from SA2 counts:
  
  # create a column that is the number of suppressed cells
  # create a column that tells you whether (for that triple) any of the SA2 level counts were suppressed (==NA)
  # then sum up SA2 counts to TA level (ignoring NAs)
  
  triples_TA_fromSA2 <- triples_SA2 %>%
    left_join(concordance) %>%
    group_by(TA2018,age_band,ethnicity,sex,dwl_size) %>%
    mutate(suppressed_SA2_number = sum(is.na(triple_count))) %>%
    mutate(suppressed_SA2 = if_else(is.na(sum(triple_count, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(TA2018,age_band,ethnicity,sex,dwl_size,suppressed_SA2,suppressed_SA2_number) %>%
    summarise(triple_count_fromSA2 = sum(triple_count, na.rm = TRUE)) %>%
    ungroup()
  
  
  # find missing values, and reconstruct known NA from zeros
  comparison_TA_level <- triples_TA %>%
    right_join(triples_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(missing_TAlevel = triple_count-triple_count_fromSA2) %>%
      mutate(triple_count_fromSA2 = case_when(
      triple_count_fromSA2==0 & suppressed_SA2=='No' ~ 0,
      triple_count_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
      TRUE ~    triple_count_fromSA2
        )) %>%
      mutate(triple_count_fromSA2 = ifelse(triple_count_fromSA2==999999,NA,triple_count_fromSA2))
  
  comparison_TA_level %>% filter(suppressed_SA2=='No') %>% arrange(desc(missing_TAlevel))
  
  
  triples_NZ_fromSA2 <- triples_SA2 %>%
    left_join(concordance) %>%
    group_by(age_band,ethnicity,sex,dwl_size) %>%
    mutate(suppressed_SA2_number = sum(is.na(triple_count))) %>%
    mutate(suppressed_SA2 = if_else(is.na(sum(triple_count, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(age_band,ethnicity,sex,dwl_size,suppressed_SA2,suppressed_SA2_number) %>%
    summarise(triple_count_fromSA2 = sum(triple_count, na.rm = TRUE)) %>%
    ungroup()
  
  
  comparison_NZ_level_fromSA2 <- triples_NZ %>%
    right_join(triples_NZ_fromSA2,  by = c("age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(missing_NZlevel = triple_count-triple_count_fromSA2) %>%
    mutate(triple_count_fromSA2 = case_when(
      triple_count_fromSA2==0 & suppressed_SA2=='No' ~ 0,
      triple_count_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
      TRUE ~    triple_count_fromSA2
        )) %>%
    mutate(triple_count_fromSA2 = ifelse(triple_count_fromSA2==999999,NA,triple_count_fromSA2)) %>%
    mutate(mean_per_suppressed = missing_NZlevel/suppressed_SA2_number)
  
  
  
 #  #Just to check visually we look at how upsampled this is by looking at TAs which have no suppression at SA2 level. 
  
  # sampling_effect <- comparison_TA_level %>% 
  #   filter(suppressed_SA2 =='No') %>%
  #   mutate(proportion_extra = missing_TAlevel/triple_count)
  
  # print(ggplot(sampling_effect)+
  #       geom_histogram(aes(x=proportion_extra)) +
  #       ggtitle("Sampling effect", paste0("Mean: ",mean(sampling_effect$proportion_extra, na.rm = TRUE)))
  #       )
  
  #### Guessing the suppressed counts
  
  #At this point we know that at TA level there are some missing when there was a lot of suppression, 
  #but in general the counts are around 0.5% too high. We use the number missing and divide by the number of suppressed cells to
  #work out the mean number of extra people need to be added for that SA2 and triple combination. 
  #Then we use that mean and sample a number between 1 and 5, but centred on that number to guess the unsuppressed count. 
  #Because of rounding to an integer, and round above 5 or below 1, the mean won't quite work as expected, 
  #but it is a decent first guess. If there are already too many people in that triple, then they are set to the minimum possible (1).
  
  #This is what we did for population counts anyway, but now we have the extra problem that we don't have TA level totals, 
  #as some of those were suppressed, so we will do that first.
  
  #We have an extra constraint that there cannot be more inhabitants in a triple in an SA2 then there are individuals.
  #So for any cells that were suppressed at total population level, they will also be suppressed at workforce level, 
  #and we should make sure that we don't 'make' more inhabitants than there are people. 
  #Sooo... first let's reconstruct TA level counts. this shouldn't clash with total population at NZ level as all
  #of those were greater than 2*5 = max that we can reconstruct from suppressed. But since we'll need the data later I will load it up.
  
  #load population triples. get from dropbox (data/individuals). Original code to generate file is in nz_individuals git repo
  # population_triples_SA2 <- read_csv(here("inputs","tripleCounts_sa2_backfilled.csv"),
  #                                    col_types = cols(
  #                                      SA22018 = col_double(),
  #                                      ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
  #                                      age_band = col_factor(levels = c('0-14','15-29','30-59','60+')),
  #                                      count = col_double()) 
  #                                    ) %>%
  #   #if age band is 0-14 or 15-29, change sex to x
  #   mutate(sex = ifelse((age_band == "0-14"| age_band == "15-29"),"X",sex)) %>%
  #   arrange(SA22018,age_band,ethnicity,sex) %>%
  #   select(SA22018,age_band,ethnicity,sex,count)
  #   
  # population_triples_TA <- population_triples_SA2 %>%
  #   left_join(concordance,  by = "SA22018") %>%
  #   group_by(TA2018,age_band,ethnicity,sex) %>%
  #   summarise(count=sum(count))
  # 
  # 
  # population_triples_NZ <- population_triples_SA2 %>%
  #   # left_join(concordance,  by = "SA22018") %>%
  #   group_by(age_band,ethnicity,sex) %>%
  #   summarise(count=sum(count))
  # 
  # 
  # population_dwellings_NZ <- triples_NZ %>%
  #   group_by(age_band,sex,ethnicity) %>%
  #   summarise(triple_count = sum(triple_count)) %>%
  #   ungroup() %>%
  #   left_join(population_triples_NZ %>% mutate(sex = as.character(sex))) %>%
  #   mutate(proportion = triple_count/count)
  # 
  #We need to first do this at TA level for the suppressed counts there. Then we move on to SA2 level.
  
  missing_per_TA <- comparison_NZ_level %>%
    mutate(mean_per_suppressed = if_else(suppressed_TA_number>0,missing_NZlevel/suppressed_TA_number,0)) %>%
    select(age_band, ethnicity, sex, dwl_size, mean_per_suppressed)
  
 
  
  # this is very slow and could be sped up with a map call or anything really, rowwise is a terrible way to be doing this.
  triples_TA_infill <- triples_TA %>% 
    filter(is.na(triple_count)) %>%
    # head(100) %>%
    left_join(missing_per_TA, by = c("age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(sd = 1.2, 
           location =if_else(mean_per_suppressed>1, log(mean_per_suppressed^2 / sqrt(sd^2 + mean_per_suppressed^2)),-0.445999), 
           shape = ifelse(mean_per_suppressed>1,sqrt(log(1 + (sd^2 / mean_per_suppressed^2))),0.9444565)) %>%
    rowwise() %>%
    mutate(triple_count_guess = case_when(
      mean_per_suppressed<1    ~ 1,
      mean_per_suppressed>=5    ~ 5,
      TRUE                      ~ max(1,min(5,round(rlnorm(n=1, location, shape))))
    )
    ) %>%
    ungroup()
  
  triples_TA_infill <- triples_TA %>%
    left_join(triples_TA_infill %>% select(TA2018,age_band, ethnicity, sex, dwl_size, triple_count_guess),  by = c("TA2018","age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(triple_count_guess = if_else(is.na(triple_count),triple_count_guess,triple_count)) #%>%
    # select(-triple_count) %>%
    # rename(triple_count=triple_count_guess) 
  
  
  ## final step is to compare against TA triple population and make smaller if too big. 
  #   
  # population_workforce_TA <- triples_TA_infill %>%
  #   group_by(TA2018,age_band,ethnicity,sex) %>%
  #   mutate(numbersuppressed_triple = sum(is.na(triple_count))) %>%
  #   ungroup() %>%
  #   group_by(TA2018,age_band,sex,ethnicity,numbersuppressed_triple) %>%
  #   summarise(triple_count = sum(triple_count_guess)) %>%
  #   ungroup() %>%
  #   left_join(population_triples_TA, by = c("TA2018", "age_band", "ethnicity", "sex")) %>%
  #   mutate(difference = count-triple_count) %>%
  #   mutate(proportion = triple_count/count)
  
  
  #We now have a 'complete' (no NA) count at TA level. Now we do the same thing but at SA2 level.
  
  
  ## take number missing per suppressed cell in that TA and use a normal distribution around the mean of approximately
  #the required number of people per suppressed cell
  
  
  comparison_TA_level_infill <- triples_TA_infill %>%
    right_join(triples_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex", "dwl_size")) %>%
    mutate(missing_TAlevel = triple_count_guess-triple_count_fromSA2) %>%
    mutate(triple_count_fromSA2 = case_when(
    triple_count_fromSA2==0 & suppressed_SA2=='No' ~ 0,
    triple_count_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
    TRUE ~    triple_count_fromSA2
    )) %>%
    mutate(triple_count_fromSA2 = ifelse(triple_count_fromSA2==999999,NA,triple_count_fromSA2))
  
  
  missing_per_SA2 <- comparison_TA_level_infill %>%
    mutate(mean_per_suppressed = if_else(suppressed_SA2_number>0,missing_TAlevel/suppressed_SA2_number,0)) %>%
    select(TA2018, age_band, ethnicity, sex, dwl_size, mean_per_suppressed)
  
  # this is very slow and could be sped up with a map call or anything really, rowwise is a terrible way to be doing this.
  triples_SA2_infill <- triples_SA2 %>%
    left_join(concordance) %>%
    filter(is.na(triple_count)) %>%
    # head(100) %>%
    left_join(missing_per_SA2, by = c("TA2018","age_band", "ethnicity", "sex", "dwl_size")) %>%
    mutate(sd = 1.2, 
           location =if_else(mean_per_suppressed>1, log(mean_per_suppressed^2 / sqrt(sd^2 + mean_per_suppressed^2)),-0.445999), 
           shape = ifelse(mean_per_suppressed>1,sqrt(log(1 + (sd^2 / mean_per_suppressed^2))),0.9444565)) %>%
    rowwise() %>%
    mutate(triple_count_guess = case_when(
      mean_per_suppressed<1    ~ 1,
      mean_per_suppressed>=5    ~ 5,
      TRUE                      ~ max(1,min(5,round(rlnorm(n=1, location, shape))))
    )
    ) %>%
    ungroup()
  
  triples_SA2_infill <- triples_SA2 %>%
    left_join(triples_SA2_infill %>%
    select(SA22018,age_band, ethnicity, sex, dwl_size, triple_count_guess), 
    by = c("SA22018", "age_band", "ethnicity", "sex", "dwl_size")) %>%
    mutate(triple_count_guess = if_else(is.na(triple_count),triple_count_guess,triple_count))
  
  
  # 
  ## final step is to compare against SA2 triple population and make smaller if too big.
  #Finally we look at a comparison:

  triples_SA2_infill_NZ <- triples_SA2_infill %>%
    group_by(age_band,ethnicity,sex,dwl_size) %>%
    summarise(triple_count_fromSA2_infill = sum(triple_count_guess, na.rm = TRUE)) %>%
    ungroup()

  comparison_NZ_level_infill <- comparison_NZ_level_fromSA2 %>%
    right_join(triples_SA2_infill_NZ,  by = c("age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(missing_NZlevel_infill = triple_count-triple_count_fromSA2_infill)

  triples_SA2_infill_TA <- triples_SA2_infill %>%
    left_join(concordance) %>%
    group_by(TA2018,age_band,ethnicity,sex,dwl_size) %>%
    summarise(triple_count_fromSA2_infill = sum(triple_count_guess, na.rm = TRUE)) %>%
    ungroup()

  comparison_TA_level_infill <- comparison_TA_level %>%
    right_join(triples_SA2_infill_TA,  by = c("TA2018","age_band", "ethnicity", "sex","dwl_size")) %>%
    mutate(missing_TAlevel_infill = triple_count-triple_count_fromSA2_infill)

# 
#   triples_NZ <- triples_NZ %>%
#     group_by(age_band,sex,ethnicity) %>%
#     summarise(triple_count=sum(triple_count, na.rm=TRUE)) %>%
#     ungroup() 
# 
# 
#   triples_TA <- triples_TA %>%
#     group_by(TA2018,age_band,sex,ethnicity) %>%
#     summarise(triple_count=sum(triple_count, na.rm=TRUE)) %>%
#     ungroup()
# 
#   triples_SA2 <- triples_SA2 %>%
#     group_by(SA22018,age_band,sex,ethnicity) %>%
#     summarise(triple_count=sum(triple_count, na.rm=TRUE)) %>%
#     ungroup()
#   
  #And if all looks good we replace the guess with 'count' and save as csv.
  
  triples_SA2_infill_final <-triples_SA2_infill %>%
    #aggregate over dwl_size
    rename(count = triple_count_guess) %>%
    select(SA22018,age_band,sex,ethnicity,dwl_size,count)
  
  #Save backfilled data
  
  # save as csv files 
  if(save_intermediate_outputs){
    # save as csv files one with the date, and one with no date that will always be 'the most recent'
    triples_SA2_infill_final %>%
      write_csv(file.path(output_path,"intermediates",paste0("tripleCounts_largeDwellingSizes_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")))
    
    triples_SA2_infill_final %>%
      write_csv(file.path(output_path,"intermediates","tripleCounts_largeDwellingSizes_sa2_backfilled.csv"))
    
  }
  
  return(triples_SA2_infill_final)
}
