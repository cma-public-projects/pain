"""Network processing and analysis code"""
import os as _os

if any(_os.path.exists(_os.path.join(module_path, 'proclib')) for module_path in __path__):
    __all__ = ['buildlib', 'proclib']
else:
    __all__ = ['lib']

from . import *