import networkx as nx
import csv
import sys
import pickle
import yaml
import copy
import numpy as np

from random import shuffle

def read_yaml(config_yaml):
    """ Reads in the processing config yaml

    Args:
      config_yaml (str): Path to the processing yaml
      
    Returns:
      Dictionary of data stored in the yaml
    """
    with open(config_yaml) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data

def read_network(network):
    """ Reads in a network

    Args:
      network (str): Path to network

    Returns:
      The Networkx graph object
    """
    in_file = open(network,'rb')
    G = pickle.load(in_file)
    in_file.close()
    return G

def count_groups(network):
    """ Writes a csv file of the number of groups each individual is connected to

    Args:
      network (str): Path to network
    """
    # Define group types and read in network
    close_groups = {'HOME': ['DG', 'DZ'], 'WORK': ['WG', 'WZ', 'SG', 'SZ', 'PG', 'PZ'], 'COMM': ['EX']}    
    G = read_network(network)
    # Get list of individuals
    indivs = [x for x,y in G.nodes(data=True) if y['bipartite'] == 1]
    # save dictionary as .csv. Needs to be a list of row dictionaries
    column_headings = ['individual_node','individual_age','Total_num_groups','Home','Home_close','Work','Work_close','School','School_close','Community','Community_close']
    data = []
    # For each individual, count groups
    for i in indivs:
        row = {'individual_node' : '', 'individual_age' : 9, 'Total_num_groups': 0 , 'Home' : 0, 'Home_close' : 0, 'Work' : 0, 'Work_close' : 0, 'School' : 0, 'School_close' : 0, 'Community' : 0, 'Community_close' : 0}
        row['individual_node'] = i
        row['individual_age'] = G.nodes[i]['age']
        groups = list(G[i])
        row['Total_num_groups'] = len(groups)
        row['Home'] = len([g for g in groups if g[0]=='D'])
        row['Home_close'] = len([g for g in groups if g[:2] in close_groups['HOME']])
        row['Work'] = len([g for g in groups if g[0]=='W'])
        row['Work_close'] = len([g for g in groups if g[0]=='W' and g[:2] in close_groups['WORK']])
        row['School'] = len([g for g in groups if g[:2]=='P' or g[0]=='S'])
        row['School_close'] = len([g for g in groups if (g[0]=='P' or g[0]=='S') and (g[:2] in close_groups['WORK'])])
        row['Community'] = len([g for g in groups if g[0]=='E'])
        row['Community_close'] = len([g for g in groups if g[:2] in close_groups['COMM']])
        data.append(row)
    with open(network.split(".gpickle")[0]+'_num_groups_per_individual.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames = column_headings)
            writer.writeheader()
            for row in data:
                writer.writerow(row)

def convert_labels(network, label1, label2):
    """ Renames the label1 nodes as label2 nodes in a network and saves the resultant gpickle

     Args:
       network (str): Path to network
       label1 (str): Layer ID to change e.g. PM
       label2 (str): Layer ID to change to e.g. SC
    """
    mapping = {}
    G = read_network(network)
    # Get relabelling mapping
    for node in G:
        if node[:2] == label1:
            mapping[node] = node.replace(label1,label2)
    # Relabel and save network
    network = nx.relabel_nodes(G, mapping, copy=False)
    tout = network.split('_')
    tout[-1] = label1 + "_to_" + label2 + "_" + tout[-1]
    with open("_".join(tout), 'wb') as fp:
        pickle.dump(network, fp, protocol=4)

def write_sa2_to_sa2_connection_matrix(network, layer):
    """ Writes the total number of connections from SA2 to SA2 for the specified layer or all

    Args:
      network (str): Path to network
      layer (list or str): Layer(s) to write connections for
    """
    
    def writing(G, layer):
        ### Helper function to allow for input differences
        # For each node in the network (including group nodes!)
        for individual in G:
            # if the node is an individual node
            if G.nodes[individual]["bipartite"]==1:
                # get the list of all groups that individual is connected to
                attached_groups = list(G[individual])
                i_dict = {}
                # Get the number of contacts in each SA2 for each attached group node for the specified layer
                for group_node in attached_groups:
                    if group_node[:2] == layer:
                        attached_individuals = list(G[group_node])
                        for contact in attached_individuals:
                            if contact != individual:
                                if G.nodes[contact]["SA2"] not in i_dict:
                                    i_dict[G.nodes[contact]["SA2"]] = 0
                                i_dict[G.nodes[contact]["SA2"]] += 1
                # Or get the number of contacts in each SA2 for all group nodes
                    elif layer == "ALL":
                        attached_individuals = list(G[group_node])
                        for contact in attached_individuals:
                            if contact != individual:
                                if G.nodes[contact]["SA2"] not in i_dict:
                                    i_dict[G.nodes[contact]["SA2"]] = 0
                                i_dict[G.nodes[contact]["SA2"]] += 1
                # or if there are no group nodes of the type specified in `layer`do nothing
                    # else:
                        # attached_individuals = list(G[node])
                        # i_dict[G.nodes[individual]["SA2"]] = 0
                for sa2 in i_dict:
                    output_dict[G.nodes[individual]["SA2"]][sa2] += i_dict[sa2]
        # Adding top line to csv string
        out_str = ''
        sa2_list = []
        for sa2 in output_dict:
            sa2_list.append(sa2)
            out_str += ',' + str(sa2)
        # Adding rows to csv string
        within_sa2 = 0
        total_links = 0
        for sa2 in sa2_list:
            out_str += '\n' + str(sa2)
            for sa2_2 in sa2_list:
                if sa2_2 in output_dict[sa2]:
                    total_links += output_dict[sa2][sa2_2]
                    out_str += "," + str(output_dict[sa2][sa2_2])
                    if sa2_2 == sa2:
                        within_sa2 += output_dict[sa2][sa2_2]
                if sa2_2 not in output_dict[sa2]:
                    out_str += ",0"
        # Writing to csv
        wf = open(network.split(".gpickle")[0]+'_' + layer + '_sa2_sa2_matrix.csv','w')
        wf.write(out_str)
        wf.close()

    G = read_network(network)

    ## First we create a list of all the SA2s in the input network

    ## Option1: make dictionary of the SA2s of the full network (including all group nodes!!) and then filter to just look at SA2s for individuals
    # network_SA2_dict = nx.get_node_attributes(G, "SA2")
    # ind_SA2_dict = {k:v for (k,v) in network_SA2_dict.items() if "IH" in k}
    # SA2_list = list(set(list(ind_SA2_dict.values())))
    # SA2_list # is then a list of all SA2s that individuals in the network live in


    ## Option2: make a subgraph of just the individuals, then get their SA2s -- this is faster on a large (NZ) network in testing
    ind_G = G.subgraph([node for node,attr in G.nodes(data=True) if attr['bipartite']==1])
    ind_SA2_dict = nx.get_node_attributes(ind_G, "SA2")
    SA2_list = list(set(list(ind_SA2_dict.values())))
    SA2_list.sort()
    # SA2_list # is then a list of all SA2s that individuals in the network live in and sorts it

    # Handling different input from config yaml
    if (type(layer) == list):
        for l2 in layer:
        # first wipe any pre-existing output_dict from previous layer calculations
           output_dict = {} 
           # then make output_dict (nested dict of all combinations of SA2s, all set to zero initially)
           for sa2_x in SA2_list:
            output_dict[sa2_x]={}
            for sa2_y in SA2_list:
                output_dict[sa2_x][sa2_y]=0

           writing(G, l2)
    else:
        # first wipe any pre-existing output_dict from previous layer calculations
        output_dict = {} 
        # then make output_dict (nested dict of all combinations of SA2s, all set to zero initially)
        for sa2_x in SA2_list:
            output_dict[sa2_x]={}
            for sa2_y in SA2_list:
                output_dict[sa2_x][sa2_y]=0

        writing(G, l2)

def write_community_edgelists(network):
    """ Writes the edgelists for the EX and EV layers

    Args:
        network (str): Path to network

    Notes:
        NB: ONLY RUN ON SMALL NETWORKS
    """
    # Read in network and get edgelists
    G = read_network(network)
    network_df = nx.to_pandas_edgelist(G)
    #rename cols to be consistent with other network output files
    network_df.columns = ['INDIVIDUAL', 'GROUP NODE']

    #Save EX edges to edge list csv file
    EX_edges_df = network_df[network_df['GROUP NODE'].str.contains("EX")]
    EX_edges_df.reset_index(drop=True).to_csv(network.replace(".gpickle","_EDGELIST_EX.csv"),index=False)

    #Save EV edges to edge list csv file
    EV_edges_df = network_df[network_df['GROUP NODE'].str.contains("EV")]
    EV_edges_df.reset_index(drop=True).to_csv(network.replace(".gpickle","_EDGELIST_EV.csv"),index=False)

def bin_obj(in_dict, num):
    """ Adds to a dictionary of count 'bins'

    Args:
      in_dict (dict): Dictionary of bins
      num (int): Number to count and add to a bin
    
    Returns:
      The updated in_dict
    """
    if num <= 10:
        in_dict[num] += 1
    elif num <= 100:
        in_dict["<1E2"] += 1
    elif num <= 1000:
        in_dict["<1E3"] += 1
    elif num <= 10000:
        in_dict["<1E4"] += 1
    elif num <= 100000:
        in_dict["<1E5"] += 1
    elif num <= 1000000:
        in_dict["<1E6"] += 1
    elif num <= 10000000:
        in_dict["<1E7"] += 1
    else:
        in_dict[">1E7"] += 1
    return in_dict

def extract_analytics(network, acm=True, acl=50000, ind_thresh=0):
    G = network
    ind_threshold = ind_thresh
    age_contact_matrices = acm
    age_contact_limit = acl
    readme = "Network Analytics for graph: " + network

    count_default_dict = {0:0,1:0,2:0,3:0,4:0,5:0,6:0,7:0,8:0,9:0,10:0,"<1E2":0,"<1E3":0,"<1E4":0,"<1E5":0,"<1E6":0,"<1E7":0,">1E7":0}
    count_keys = [0,1,2,3,4,5,6,7,8,9,10,"<1E2","<1E3","<1E4","<1E5","<1E6","<1E7",">1E7"]

    if "full" in network:
        node_types = ["EX","EV","DW","DZ","DG","WP","WG","WZ","SC","PG","PZ","PM","IH"]

        default_age_contact_counter = {"EX":[0,0,0,0],"EV":[0,0,0,0],"DW":[0,0,0,0],"DZ":[0,0,0,0],"DG":[0,0,0,0],"WP":[0,0,0,0],"WG":[0,0,0,0],"WZ":[0,0,0,0],"SC":[0,0,0,0],"SG":[0,0,0,0],"SZ":[0,0,0,0],"PM":[0,0,0,0],"PG":[0,0,0,0],"PZ":[0,0,0,0],"IH":[0,0,0,0],"TOT":[0,0,0,0]}
        default_age_contact_matrix = {0:[[],[],[],[]],1:[[],[],[],[]],2:[[],[],[],[]],3:[[],[],[],[]]}
        age_contact_keys = ["EX","EV","DW","DZ","DG","WP","WG","WZ","SC","SG","SZ","PG","PZ","PM","IH","TOT"]

        node_size_metrics = {"EX":[],"EV":[],"DW":[],"DZ":[],"DG":[],"WP":[],"WG":[],"WZ":[],"SC":[],"SG":[],"SZ":[],"PG":[],"PZ":[],"PM":[],"IH":[]}

        node_connect_metrics_1 = {"EX":[],"EV":[],"DW":[],"DZ":[],"DG":[],"WP":[],"WG":[],"WZ":[],"SC":[],"SG":[],"SZ":[],"PG":[],"PZ":[],"PM":[],"IH":[]}
        node_connect_metrics_2 = {"EX":[],"EV":[],"DW":[],"DZ":[],"DG":[],"WP":[],"WG":[],"WZ":[],"SC":[],"SG":[],"SZ":[],"PG":[],"PZ":[],"PM":[],"IH":[]}
        node_counts = {"EX":0,"EV":0,"DW":0,"DZ":0,"DG":0,"WP":0,"WG":0,"WZ":0,"SC":0,"SG":0,"SZ":0,"PG":0,"PZ":0,"PM":0,"IH":0}
        tmp_counts = {"EX":0,"EV":0,"DW":0,"DZ":0,"DG":0,"WP":0,"WG":0,"WZ":0,"SC":0,"SG":0,"SZ":0,"PG":0,"PZ":0,"PM":0,"IH":0}

        group_age_contact_matrix ={"EX":copy.deepcopy(default_age_contact_matrix),"EV":copy.deepcopy(default_age_contact_matrix),"DW":copy.deepcopy(default_age_contact_matrix),"DZ":copy.deepcopy(default_age_contact_matrix),"DG":copy.deepcopy(default_age_contact_matrix),"WP":copy.deepcopy(default_age_contact_matrix),\
                                   "WG":copy.deepcopy(default_age_contact_matrix),"WZ":copy.deepcopy(default_age_contact_matrix),"SC":copy.deepcopy(default_age_contact_matrix),"SG":copy.deepcopy(default_age_contact_matrix),\
                                   "SZ":copy.deepcopy(default_age_contact_matrix),"PG":copy.deepcopy(default_age_contact_matrix),"PZ":copy.deepcopy(default_age_contact_matrix),"PM":copy.deepcopy(default_age_contact_matrix),"IH":copy.deepcopy(default_age_contact_matrix),"TOT":copy.deepcopy(default_age_contact_matrix)}

        size_dicts = {"EX":copy.deepcopy(count_default_dict),"EV":copy.deepcopy(count_default_dict),"DW":copy.deepcopy(count_default_dict),"DZ":copy.deepcopy(count_default_dict),"DG":copy.deepcopy(count_default_dict),"WP":copy.deepcopy(count_default_dict),\
                      "WG":copy.deepcopy(count_default_dict),"WZ":copy.deepcopy(count_default_dict),"SC":copy.deepcopy(count_default_dict),"SG":copy.deepcopy(count_default_dict),"SZ":copy.deepcopy(count_default_dict),\
                      "PG":copy.deepcopy(count_default_dict),"PZ":copy.deepcopy(count_default_dict),"PM":copy.deepcopy(count_default_dict),"IH":copy.deepcopy(count_default_dict)}
        connection_dicts = {"EX":copy.deepcopy(count_default_dict),"EV":copy.deepcopy(count_default_dict),"DW":copy.deepcopy(count_default_dict),"DZ":copy.deepcopy(count_default_dict),"DG":copy.deepcopy(count_default_dict),"WP":copy.deepcopy(count_default_dict),\
                            "WG":copy.deepcopy(count_default_dict),"WZ":copy.deepcopy(count_default_dict),"SC":copy.deepcopy(count_default_dict),"SG":copy.deepcopy(count_default_dict),"SZ":copy.deepcopy(count_default_dict),\
                            "PG":copy.deepcopy(count_default_dict),"PZ":copy.deepcopy(count_default_dict),"PM":copy.deepcopy(count_default_dict),"IH":copy.deepcopy(count_default_dict)}

    else:
        node_types = ["EX","EV","DW","WP","SC","PM","IH"]
        
        default_age_contact_counter = {"EX":[0,0,0,0],"EV":[0,0,0,0],"DW":[0,0,0,0],"WP":[0,0,0,0],"SC":[0,0,0,0],"PM":[0,0,0,0],"IH":[0,0,0,0],"TOT":[0,0,0,0]}
        default_age_contact_matrix = {0:[[],[],[],[]],1:[[],[],[],[]],2:[[],[],[],[]],3:[[],[],[],[]]}
        age_contact_keys = ["EX","EV","DW","WP","SC","PM","IH","TOT"]

        node_size_metrics = {"EX":[],"EV":[],"DW":[],"WP":[],"SC":[],"IH":[],"PM":[]}

        node_connect_metrics_1 = {"EX":[],"EV":[],"DW":[],"WP":[],"SC":[],"IH":[],"PM":[]}
        node_connect_metrics_2 = {"EX":[],"EV":[],"DW":[],"WP":[],"SC":[],"IH":[],"PM":[]}
        node_counts = {"EX":0,"EV":0,"DW":0,"WP":0,"SC":0,"PM":0,"IH":0}
        tmp_counts = {"EX":0,"EV":0,"DW":0,"WP":0,"SC":0,"IH":0,"PM":0}

        group_age_contact_matrix ={"EX":copy.deepcopy(default_age_contact_matrix),"EV":copy.deepcopy(default_age_contact_matrix),"DW":copy.deepcopy(default_age_contact_matrix),"WP":copy.deepcopy(default_age_contact_matrix),\
                                   "SC":copy.deepcopy(default_age_contact_matrix),"IH":copy.deepcopy(default_age_contact_matrix),"TOT":copy.deepcopy(default_age_contact_matrix),"PM":copy.deepcopy(default_age_contact_matrix)}

        size_dicts = {"EX":copy.deepcopy(count_default_dict),"EV":copy.deepcopy(count_default_dict),"DW":copy.deepcopy(count_default_dict),"WP":copy.deepcopy(count_default_dict),"SC":copy.deepcopy(count_default_dict),\
                      "IH":copy.deepcopy(count_default_dict),"PM":copy.deepcopy(count_default_dict)}
        connection_dicts = {"EX":copy.deepcopy(count_default_dict),"EV":copy.deepcopy(count_default_dict),"DW":copy.deepcopy(count_default_dict),"WP":copy.deepcopy(count_default_dict),"SC":copy.deepcopy(count_default_dict),\
                            "IH":copy.deepcopy(count_default_dict),"PM":copy.deepcopy(count_default_dict)}



    network = read_network(network)
    sub_graphs = (network.subgraph(c) for c in nx.connected_components(network))

    sub_graph_individuals = []

    ind_dict = copy.deepcopy(count_default_dict)
    grp_dict = copy.deepcopy(count_default_dict)
    tot_dict = copy.deepcopy(count_default_dict)

    readme += "\nSubcomponents\nsize\tind\tgrp\ttot"
    
    largest_group = [0,0]
    remove_nodes = []
    for i,sg in enumerate(sub_graphs):
        total = sg.number_of_nodes()
        ind_nodes = [x for x,y in sg.nodes(data=True) if y['bipartite']==1]
        grp_nodes = [x for x,y in sg.nodes(data=True) if y['bipartite']==0]
        inds = len(ind_nodes)
        grps = len(grp_nodes)
        sub_graph_individuals.append(inds)
        if inds > largest_group[0]:
            largest_group = [inds,grps]
        ind_dict = bin_obj(ind_dict,inds)
        grp_dict = bin_obj(grp_dict,grps)
        tot_dict = bin_obj(tot_dict,total)
        if inds <= ind_threshold:
            remove_nodes += ind_nodes
            remove_nodes += grp_nodes
    for a in count_keys:
        readme += "\n" + str(a) + "\t" + str(ind_dict[a]) + "\t" + str(grp_dict[a]) + "\t" + str(tot_dict[a])

    readme += "\nLargest component of the network has " + str(largest_group[0]) + " individuals and " + str(largest_group[1]) + " groups"
    if len(remove_nodes) > 0:
        # Removing disconnected subcomponents of fewer than ind_threshold
        for a in remove_nodes:
            network.remove_node(a)

    readme += "\nSpecific Connected Component Metrics Based on Individual Count:" + "\nMean\t" + str(round((float(sum(sub_graph_individuals))/len(sub_graph_individuals)),1)) + "\tLQ\t" + str(int(np.quantile(sub_graph_individuals, .25))) + "\tMedian\t" + str(int(np.quantile(sub_graph_individuals, .5))) + "\tUQ\t" + str(int(np.quantile(sub_graph_individuals, .75)))

    n_nodes = [x for x,y in network.nodes(data=True)]
    shuffle(n_nodes)
    age_thresh_count = 0

    for n in n_nodes:
        node_counts[n[:2]] += 1
        count = len(list(network[n]))
        size_dicts[n[:2]] = bin_obj(size_dicts[n[:2]],count)
        node_size_metrics[n[:2]].append(count)
        if network.nodes[n]["bipartite"] == 1:
            connection_counts = copy.deepcopy(tmp_counts)
            count_connect_metrics_1 = copy.deepcopy(tmp_counts)
            count_connect_metrics_2 = {}    
            grp_node_types = []
            grps = list(network[n])
            ind_age = network.nodes[n]["age"]
            age_counter = copy.deepcopy(default_age_contact_counter)
            for b in grps:
                connection_counts[b[:2]] += len(list(network[b])) - 1 
                count_connect_metrics_1[b[:2]] += len(list(network[b])) - 1
                if b[:2] not in count_connect_metrics_2:
                    grp_node_types.append(b[:2])
                    count_connect_metrics_2[b[:2]] = 0
                count_connect_metrics_2[b[:2]] += len(list(network[b])) - 1
                if age_contact_matrices and age_thresh_count < age_contact_limit:
                    grps_2 = list(network[b])
                    age_counter[b[:2]][ind_age] -= 1
                    age_counter["TOT"][ind_age] -= 1
                    for c in grps_2:
                        age_counter["TOT"][network.nodes[c]["age"]] += 1
                        age_counter[b[:2]][network.nodes[c]["age"]] += 1
            if age_contact_matrices and age_thresh_count < age_contact_limit:
                age_thresh_count += 1
                for b in age_contact_keys:
                    for c in range(4):
                        group_age_contact_matrix[b][ind_age][c].append(age_counter[b][c])
            for b in node_types:
                connection_dicts[b] = bin_obj(connection_dicts[b],connection_counts[b])
                node_connect_metrics_1[b].append(count_connect_metrics_1[b])
                if b in grp_node_types:
                    node_connect_metrics_2[b].append(count_connect_metrics_2[b])
    
    readme += "\n\n Contact Metrics Through Nodes Even if No Link To Node Type\n"
    #mean
    for a in node_types:
        readme += "\t" + a
    readme += "\nMean\t"
    for a in node_types:
        if len(node_connect_metrics_1[a]) > 0:
            readme += str(int(float(sum(node_connect_metrics_1[a]))/len(node_connect_metrics_1[a]))) + "\t"
        else:
            readme += "N/A\t"
    #lq
    readme += "\nLQ\t"
    for a in node_types:
        if len(node_connect_metrics_1[a]) > 0:
            readme += str(int(np.quantile(node_connect_metrics_1[a], .25))) + "\t"
        else:
            readme += "N/A\t"
    #median
    readme += "\nMedian\t"
    for a in node_types:
        if len(node_connect_metrics_1[a]) > 0:
            readme += str(int(np.quantile(node_connect_metrics_1[a], .5))) + "\t"
        else:
            readme += "N/A\t"
    #uq
    readme += "\nUQ\t"
    for a in node_types:
        if len(node_connect_metrics_1[a]) > 0:
            readme += str(int(np.quantile(node_connect_metrics_1[a], .75))) + "\t"
        else:
            readme += "N/A\t"

    readme += ("\n\nContact Metrics Through Nodes Only if Link To Node Type")
    #mean
    readme += "\nMean\t"
    for a in node_types:
        if len(node_connect_metrics_2[a]) > 0:
            readme += str(int(float(sum(node_connect_metrics_2[a]))/len(node_connect_metrics_2[a]))) + "\t"
        else:
            readme += "N/A\t"
    #lq
    readme += "\nLQ\t"
    for a in node_types:
        if len(node_connect_metrics_2[a]) > 0:
            readme += str(int(np.quantile(node_connect_metrics_2[a], .25))) + "\t"
        else:
            readme += "N/A\t"
    #median
    readme += "\nMedian\t"
    for a in node_types:
        if len(node_connect_metrics_2[a]) > 0:
            readme += str(int(np.quantile(node_connect_metrics_2[a], .5))) + "\t"
        else:
            readme += "N/A\t"
    #uq
    readme += "\nUQ\t"
    for a in node_types:
        if len(node_connect_metrics_2[a]) > 0:
            readme += str(int(np.quantile(node_connect_metrics_2[a], .75))) + "\t"
        else:
            readme += "N/A\t"

    if age_contact_matrices:
        age_labels = ["0-14","15-29","30-59","60+"]
        readme += "\n\nAge Contact Matrices"
        for a in age_contact_keys:
            readme += "\n" + a
            readme += "\nMEAN"
            readme += "\t0-14\t15-29\t30-59\t60+\n"
            for b in range(4):
                print_str = age_labels[b]
                for c in range(4):
                    if len(group_age_contact_matrix[a][b][c]) == 0:
                        print_str += "\tN/A"
                    else:
                        print_str += "\t" + str(round(sum(group_age_contact_matrix[a][b][c])/float(len(group_age_contact_matrix[a][b][c])),1))
                readme += print_str + "\n"
            readme += "\nLQ"
            readme += "\t0-14\t15-29\t30-59\t60+\n"
            for b in range(4):
                print_str = age_labels[b]
                for c in range(4):
                    if len(group_age_contact_matrix[a][b][c]) == 0:
                        print_str += "\tN/A"
                    else:
                        print_str += "\t" + str(int(np.quantile(group_age_contact_matrix[a][b][c], .25)))
                readme += print_str + "\n"
            readme += "\nMedian"
            readme += "\t0-14\t15-29\t30-59\t60+\n"
            for b in range(4):
                print_str = age_labels[b]
                for c in range(4):
                    if len(group_age_contact_matrix[a][b][c]) == 0:
                        print_str += "\tN/A"
                    else:
                        print_str += "\t" + str(int(np.quantile(group_age_contact_matrix[a][b][c], .5)))
                readme += print_str + "\n"
            readme += "\nUQ"
            readme += "\t0-14\t15-29\t30-59\t60+\n"
            for b in range(4):
                print_str = age_labels[b]
                for c in range(4):
                    if len(group_age_contact_matrix[a][b][c]) == 0:
                        print_str += "\tN/A"
                    else:
                        print_str += "\t" + str(int(np.quantile(group_age_contact_matrix[a][b][c], .75)))
                readme += print_str + "\n"
    wf = open(G.replace("gpickle","_network_analytics.txt"),'w')
    wf.write(readme)
    wf.close()