from .proc_functions import count_groups, convert_labels, extract_analytics, read_yaml, read_network, write_community_edgelists , write_sa2_to_sa2_connection_matrix


def main(config_yaml):
    config = read_yaml(config_yaml)
    network = config['network']

    if config['count_groups']:
        count_groups(network)
    
    if config['relabel_nodes']:
        convert_labels(network, config['label1'], config['label2'])
    
    if config['write_sa2_to_sa2_connection_matrix']:
        write_sa2_to_sa2_connection_matrix(network, config['write_sa2_to_sa2_layer_list'])
    
    if config['write_community_edgelists']:
        write_community_edgelists(network)

    if config['extract_analytics']:
        extract_analytics(network, config['age_contact_matrices'], config['age_contact_limit'],config['ind_threshold'])