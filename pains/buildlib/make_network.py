import yaml
import time
import datetime

from . import close_contact_main, community_main, dw_main, merge_main, nx_main, sc_main, special_main, wp_main
from .community_functions import generate_new_nodelist
from .universal_functions import make_directory, check_path

def main(in_yaml):
    """ Main run script for the buildlib, generates all network files

    Args:
      in_yaml (str): Path to the input configuration yaml
    """
    with open(in_yaml) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    # THE FOLLOWING SEGMENTS OF CODE CHECK THAT THERE IS NO ATTEMPT TO GENERATE A NEW LAYER AND MERGE IN AN EXISTING LAYER
    if data['dw_parameters']['generate'] and data['merge_parameters']['read_dw'] and data['merge_parameters']['generate']:
        print("Unable to both CREATE new DWELLING LAYER and MERGE in existing DWELLING LAYER - QUITTING")
        quit()
    if data['wp_parameters']['generate'] and data['merge_parameters']['read_wp'] and data['merge_parameters']['generate']:
        print("Unable to both CREATE new WORKPLACE LAYER and MERGE in existing WORKPLACE LAYER - QUITTING")
        quit()
    if data['sc_parameters']['generate'] and data['merge_parameters']['read_sc'] and data['merge_parameters']['generate']:
        print("Unable to both CREATE new SCHOOL LAYER and MERGE in existing SCHOOL LAYER - QUITTING")
        quit()
    if data['community_parameters']['ex_parameters']['generate'] and data['merge_parameters']['read_ex'] and data['merge_parameters']['generate']:
        print("Unable to both CREATE new EX LAYER and MERGE in existing EX LAYER - QUITTING")
        quit()
    if data['community_parameters']['ev_parameters']['generate'] and data['merge_parameters']['read_ev'] and data['merge_parameters']['generate']:
        print("Unable to both CREATE new EV LAYER and MERGE in existing EV LAYER - QUITTING")
        quit()
    if data['nx_parameters']['generate'] and data['nx_parameters']['read_network'] and (data['dw_parameters']['generate'] or data['wp_parameters']['generate'] or data['sc_parameters']['generate'] or data['community_parameters']['ex_parameters']['generate'] or data['community_parameters']['ev_parameters']['generate'] or data['merge_parameters']['generate']):
        print("Unable to both read in network file and generate a new layer or network - QUITTING")
        quit()
    # LOAD IN LABELS YAML
    with open(data["label_yaml"]) as f:
        labels = yaml.load(f, Loader=yaml.FullLoader)
    data['output_directory'] = check_path(data['output_directory'])
    # MAKE OUTPUT DIRECTORY IF IT DOES NOT EXIST
    make_directory(data['output_directory'])
    # CREATE SPECIAL SELECTION
    excluded_sa2, excluded_ta = special_main.main(
        data['special_selection'],
        data['excluded_sa2'],
        data['excluded_ta']
        )
    # GENERATE THE DWELLING LAYER
    s1 = time.time()
    if data['dw_parameters']['generate']:
        dw_main.main(
            labels,
            data["individuals_file"],
            data["output_directory"],
            data['dw_parameters']['ethnic_assortativity'],
            data['dw_parameters']['ethnic_assortativity_weight'],
            data['dw_parameters']['small_dw_file'],
            data['dw_parameters']['large_dw_file'],
            data['dw_parameters']['census_matching'],
            data['dw_parameters']['census_file'],
            data['dw_parameters']['age_restriction'],
            data['dw_parameters']['clone_threshold'],
            excluded_sa2,
            excluded_ta
            )
    time_diff = int(time.time()-s1)
    print("DW", datetime.timedelta(seconds=time_diff))
    # GENERATE THE WORK LAYER
    s1 = time.time()
    if data['wp_parameters']['generate']:
        wp_main.main(
            labels,
            data["individuals_file"],
            data["output_directory"],
            data['wp_parameters']["sector_file"],
            data['wp_parameters']["sector_suppressed_probs"],
            data['wp_parameters']["commute_file"],
            data['wp_parameters']["wp_file"],
            excluded_sa2,
            excluded_ta,
            data['wp_parameters']["maximum_trials"]
            )
    time_diff = int(time.time()-s1)
    print("WP", datetime.timedelta(seconds=time_diff))
    # GENERATE THE SCHOOL LAYER
    s1 = time.time()
    if data['sc_parameters']['generate']:
        sc_main.main(
            labels,
            data["individuals_file"],
            data["output_directory"],
            data['sc_parameters']["ece_file"],
            data['sc_parameters']["school_file"],
            data['sc_parameters']["commute_file"],
            excluded_sa2,
            excluded_ta,
            data['sc_parameters']["school_characteristics"]
            )
    time_diff = int(time.time()-s1)
    print("SC", datetime.timedelta(seconds=time_diff))
    # GENERATE COMMUNITY LAYERS
    # THIS FUNCTION READS IN THE NODELIST AND IF SPECIFIED UPDATES THE NODELIST TO INCLUDE LINKS FROM THE SC AND WP LAYERS
    s1 = time.time()
    if data['community_parameters']['ex_parameters']['generate'] or data['community_parameters']['ev_parameters']['generate']:
        individuals_node_list,initial_string = generate_new_nodelist(
            labels,
            data["individuals_file"],
            data['community_parameters']['commute_nodelist']['sa2_ta_concordance_file'],
            data['community_parameters']['commute_nodelist']['wp_commute'],
            data['community_parameters']['commute_nodelist']['read_wp'],
            data['community_parameters']['commute_nodelist']['wp_file'],
            data['community_parameters']['commute_nodelist']['sc_commute'],
            data['community_parameters']['commute_nodelist']['read_sc'],
            data['community_parameters']['commute_nodelist']['sc_file'],
            excluded_ta,
            excluded_sa2,
            data["output_directory"]
            )
    time_diff = int(time.time()-s1)
    print("COMMUNITY SETUP", datetime.timedelta(seconds=time_diff))
    # THIS FUNCTION GENERATES THE EX LAYER
    s1 = time.time()
    if data['community_parameters']['ex_parameters']['generate']:
        community_main.main(
            individuals_node_list,
            data['community_parameters']['ex_parameters']['maximum_size'],
            data['community_parameters']['ex_parameters']['minimum_size'],
            data['community_parameters']['ex_parameters']['power_law'],
            data['community_parameters']['ex_parameters']['age_assortativity'],
            data['community_parameters']['ex_parameters']['age_parameters'],
            data['community_parameters']['ex_parameters']['participation'],
            data['community_parameters']['ex_parameters']['long_accept'],
            data['community_parameters']['ex_parameters']['ethnic_assortativity']['enforce'],
            data['community_parameters']['ex_parameters']['ethnic_assortativity']['threshold'],
            data['community_parameters']['ex_parameters']['include_long_range'],
            data['community_parameters']['ex_parameters']['offset_long_range'],
            data["output_directory"],
            data['community_parameters']['ex_parameters']['trial_threshold'],
            "EX",
            initial_string
            )
    time_diff = int(time.time()-s1)
    print("EX", datetime.timedelta(seconds=time_diff))
    # THIS FUNCTION GENERATES THE EV LAYER
    s1 = time.time()
    if data['community_parameters']['ev_parameters']['generate']:
        community_main.main(
            individuals_node_list,
            data['community_parameters']['ev_parameters']['maximum_size'],
            data['community_parameters']['ev_parameters']['minimum_size'],
            data['community_parameters']['ev_parameters']['power_law'],
            data['community_parameters']['ev_parameters']['age_assortativity'],
            data['community_parameters']['ev_parameters']['age_parameters'],
            data['community_parameters']['ev_parameters']['participation'],
            data['community_parameters']['ev_parameters']['long_accept'],
            data['community_parameters']['ev_parameters']['ethnic_assortativity']['enforce'],
            data['community_parameters']['ev_parameters']['ethnic_assortativity']['threshold'],
            data['community_parameters']['ev_parameters']['include_long_range'],
            data['community_parameters']['ev_parameters']['offset_long_range'],
            data["output_directory"],
            data['community_parameters']['ev_parameters']['trial_threshold'],
            "EV",
            initial_string
            )
    time_diff = int(time.time()-s1)
    print("EV", datetime.timedelta(seconds=time_diff))
    # THIS FUNCTION MERGES THE LAYERS AND PRODUCES THE FINAL NETWORK
    s1 = time.time()
    if data['merge_parameters']['generate']:
        merge_main.main(
            labels,
            data["individuals_file"],
            data["output_directory"],
            data['merge_parameters']['layers_to_include'],
            data['merge_parameters']['read_dw'],
            data['merge_parameters']['dw_layer'],
            data['merge_parameters']['read_wp'],
            data['merge_parameters']['wp_layer'],
            data['merge_parameters']['read_sc'],
            data['merge_parameters']['sc_layer'],
            data['merge_parameters']['read_ex'],
            data['merge_parameters']['ex_layer'],
            data['merge_parameters']['read_ev'],
            data['merge_parameters']['ev_layer'],
            excluded_ta,
            excluded_sa2,
            data['merge_parameters']['custom_layers'],
            data['merge_parameters']['custom_parameters'],
            data['merge_parameters']['link_groups'],
            data['merge_parameters']['link_group_edgelists'],
            data['merge_parameters']['individual_attributes']
            )
    time_diff = int(time.time()-s1)
    print("MERGE", datetime.timedelta(seconds=time_diff))
    # THIS FUNCTION CREATES THE NETWORKX OBJECT:
    s1 = time.time()
    if data['nx_parameters']['generate']:
        nx_main.main(
            data['nx_parameters']['read_network'],
            data['nx_parameters']['network_file'],
            data["output_directory"],
            data['nx_parameters']['workplace_shutdown'],
            data['nx_parameters']['workplace_shutdown_file'],
            data['nx_parameters']['workplace_shutdown_use_workers'],
            data['nx_parameters']['workplace_shutdown_headings'],
            in_yaml
            )
    time_diff = int(time.time()-s1)
    print("NX FUNCTIONS", datetime.timedelta(seconds=time_diff))
    # THIS FUNCTION CREATES THE CLOSE-CONTACT STRUCTURE:
    s1 = time.time()
    if data['cc_parameters']['generate']:
        close_contact_main.main(
            data['nx_parameters']['read_network'],
            data['nx_parameters']['network_file'],
            data['cc_parameters'],
            data["output_directory"]
        )
    time_diff = int(time.time()-s1)
    print("CC FUNCTIONS", datetime.timedelta(seconds=time_diff))

