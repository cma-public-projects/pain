import random
import copy

from .universal_functions import gen_indices_dict, get_available_ages, get_available_ethnicities, get_commute_sa2, get_random_individual, extract_final_metrics, id_generator, make_layer_edgelist_nodelist, make_pickle, make_readme, update_dictionaries

def generate_individuals_dict(
    labels,
    individual_nodes,
    excluded_tas,
    excluded_sa2s
    ):
    """ Generates the individuals dictionary for putting workers into a workplace

    Args:
      labels (dict): A dictionary mapping the columns in the individuals nodelist csv to the resultant node attributes in the network
      individual_nodes (str): Path to the individuals nodelist csv
      excluded_tas (list): List of TAs to exclude from the network
      excluded_sa2s (list): List of SA2s to exclude from the network

    Returns:
      Dictionary of individuals SA2s to their TAs, dictionary of individuals to fill workplaces with
    """
    rf = open(individual_nodes,"r")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n",'').split(",")
    # Building indices mapping and initialise output dictionaries
    indices_dict = gen_indices_dict(index_split,labels)
    individuals_dictionary = {}
    ta_dictionary = {}
    # For every individual
    for a in range(1,len(rline)):
        line_split = rline[a].split(",")
        # If they are excluded or not a worker then skip
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in excluded_sa2s:
            continue
        # Else if they are a worker add them to the individuals dictionary and to the TA dictionary
        if line_split[indices_dict["worker"]] == "Y":
            current_dict = individuals_dictionary
            job_count = int(line_split[indices_dict["job_count"]])
            if line_split[indices_dict["TA"]] not in current_dict:
                current_dict[line_split[indices_dict["TA"]]] = {}
            if line_split[indices_dict["SA2"]] not in current_dict[line_split[indices_dict["TA"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]] = {}
            if line_split[indices_dict["sex"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]] = {}
            if line_split[indices_dict["age"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]] = {}
            if line_split[indices_dict["ethnicity"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]] = []
            current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]].extend([line_split[indices_dict["node_id"]]] * job_count)
            if line_split[indices_dict["SA2"]] not in ta_dictionary:
                ta_dictionary[line_split[indices_dict["SA2"]]] = line_split[indices_dict["TA"]]
    return ta_dictionary,individuals_dictionary

def generate_workplace_dict(
    labels,
    work_nodes,
    excluded_tas,
    excluded_sa2s
    ):
    """ Generates the workplace dictionary for putting workers into a workplace

    Args:
      labels (dict): A dictionary mapping the columns in the individuals nodelist csv to the resultant node attributes in the network
      work_nodes (str): Path to the workplace nodelist csv
      excluded_tas (list): List of TAs to exclude from the network
      excluded_sa2s (list): List of SA2s to exclude from the network

    Returns:
      Dictionary of workplaces to fill
    """
    rf = open(work_nodes,"r")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    # Building indices mapping and initialise output dictionaries
    indices_dict = gen_indices_dict(index_split,labels)
    workplace_dictionary = {}
    # For every workplace
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in excluded_sa2s:
            continue
        if line_split[indices_dict["size"]] == "0":
            continue
        # If an included workplace add to the dictionary
        workplace_dictionary[line_split[indices_dict["node_id"]]] = {
            "links":[],
            "TA":str(int(line_split[indices_dict["TA"]])),
            "SA2":line_split[indices_dict["SA2"]],
            "sector":line_split[indices_dict["sector"]],
            "size":int(line_split[indices_dict["size"]])
            }
    return workplace_dictionary

def generate_sector_dict(
    labels,
    sector_file,
    suppressed_probs,
    excluded_tas
    ):
    """ Processes the sector file and returns a dictionary which can be used to probabilistically determine with individual triple should be used for a worker
    
    Args:
      labels (dict): A dictionary mapping the columns in the individuals nodelist csv to the resultant node attributes in the network
      sector_file (str): Path to the sector file
      suppressed_probs (dict): Dictionary of sector probabilities to fill suppressed sector values with
      excluded_tas (list): List of TAs to exclude from the network

    Returns:
      Dictionary of individuals in each workplace sector
    """
    # Initialise lists and get suppressed probabilities
    sector_suppressed_options = []
    sector_suppressed_probs = []
    current_value = 0
    for item in suppressed_probs:
        current_value += suppressed_probs[item]
        sector_suppressed_options.append(item)
        sector_suppressed_probs.append(current_value)
    # Read in sector file
    rf = open(sector_file,"r")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    # Building indices mapping and initialise output dictionaries
    indices_dict = gen_indices_dict(index_split,labels)
    sector_dictionary = {}
    # For each TA, sex, ethnicity, age, sector combination
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if line_split[indices_dict["TA"]] == "NA":
            continue
        if line_split[indices_dict["TA"]] == "999":
            continue
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        # If TA is included record information on the workers
        ta = str(int(line_split[indices_dict["TA"]]))
        sex = line_split[indices_dict["sex"]]
        ethnicity = line_split[indices_dict["ethnicity"]]
        age = line_split[indices_dict["age"]]
        sector = line_split[indices_dict["sector"]]
        # If count is suppressed then impute based on suppressed probabilities dictionary
        if line_split[indices_dict["count"]] == "-999999" or line_split[indices_dict["count"]] == "999999":
            random_val = random.uniform(0,sector_suppressed_probs[-1])
            for b in range(len(sector_suppressed_probs)):
                if random_val <= sector_suppressed_probs[b]:
                    count = sector_suppressed_options[b]
                    break
        else:
            count = int(line_split[indices_dict["count"]])
        # Add to sector dictionary
        if ta not in sector_dictionary:
            sector_dictionary[ta] = {}
        if sector not in sector_dictionary[ta]:
            sector_dictionary[ta][sector] = {}
        if sex not in sector_dictionary[ta][sector]:
            sector_dictionary[ta][sector][sex] = {}
        if age not in sector_dictionary[ta][sector][sex]:
            sector_dictionary[ta][sector][sex][age] = {}
        sector_dictionary[ta][sector][sex][age][ethnicity] = count
    return sector_dictionary

def get_sector_items(
    available_ages,
    available_ethnicities,
    sector_dictionary,
    in_ta,
    in_sector,
    in_individuals_dict
    ):
    """ Returns a series of individual characteristics based off of the workplace sector

    available_ages (list): List of available ages in the required TA
    available_ethnicities (list): List of available ethnicities in the required TA
    sector_dictionary (dict): Dictionary of individuals in each sector/TA combination
    in_ta (str): The TA of interest
    in_sector (str): The sector of interest
    in_individuals_dict (dict):  Dictionary of all workers in the TA/SA2 pair of interest

    Returns:
      Triple of age, ethnicity, sex of a worker in that sector in that TA
    """
    triples_list = []
    triples_count = []
    current_value = 0
    # For sexes in the workplace TA and sector
    for sex in sector_dictionary[in_ta][in_sector]:
        # For available ages and ethnicities if correct combination available add to the list
        for age in available_ages:
            if age not in sector_dictionary[in_ta][in_sector][sex] or age not in in_individuals_dict[sex]:
                continue
            for ethnicity in available_ethnicities:
                if ethnicity not in sector_dictionary[in_ta][in_sector][sex][age] or ethnicity not in in_individuals_dict[sex][age] or len(in_individuals_dict[sex][age][ethnicity]) == 0:
                    continue
                current_value += sector_dictionary[in_ta][in_sector][sex][age][ethnicity]
                triples_count.append(current_value)
                triples_list.append((age,ethnicity,sex))
    if len(triples_count) == 0:
        return "ERROR","ERROR","ERROR"
    # Choose a random individuals' attributes based on counts
    random_value = random.randint(1,triples_count[-1])
    for a in range(len(triples_count)):
        if random_value <= triples_count[a]:
            return [triples_list[a][0]], [triples_list[a][1]], [triples_list[a][2]]

def fill_workplaces(
    final_dictionary,
    individuals_dictionary,
    commute_dictionary,
    workplace_dictionary,
    ta_dictionary,
    sector_dictionary,
    commute_index,
    sector_match,
    maximum_trials
    ):
    """ Iterates over the current workplace dictionary and fills it from the individuals list. If the workplace is filled it is moved ti the final dictionary

    Args:
      final_dictionary (dict): Final dictionary of the workplace layer
      individuals_dictionary (dict): Dictionary of workers to fill up the workplaces
      commute_dictionary (dict): commute_work_prop_SA2 as a dictionary gives proportions of worker SA2s in a workplace SA2
      workplace_dictionary (dict): Dictionary of schools to be filled
      ta_dictionary (dict): Dictionary of worker SA2 to worker TA
      sector_dictionary (dict): Count of individuals in each sector, TA, ethnicity, age
      commute_index (str): Whether to get commuting workers by TA or SA2
      sector_match (bool): Whether to match sector to TAs
      maximum_trials (int): Maximum number of attempts to find a worker for a workplace

    Returns:
      Updated dictionaries for full workplaces, unfilled workplaces, available workers and commuting SA2s
    """
    # Shuffle workplaces so they are drawn at random
    keys = list(workplace_dictionary.keys())
    random.shuffle(keys)
    exclude_sa2s = {}
    # For all workplaces to be filled
    for wp in keys:
        if workplace_dictionary[wp]["sector"] not in exclude_sa2s:
            exclude_sa2s[workplace_dictionary[wp]["sector"]] = []
        current_trials = 0
        # While workplace isn't full and maximum number of trials has not been reached
        while workplace_dictionary[wp]["size"] > 0 and current_trials < maximum_trials:
            # Get the SA2 of the worker
            commute_sa2 = get_commute_sa2(
                commute_dictionary,
                commute_index,
                workplace_dictionary[wp][commute_index],
                exclude_sa2s[workplace_dictionary[wp]["sector"]]
                )
            if commute_sa2 == "ERROR":
                break
            if commute_sa2 not in ta_dictionary:
                commute_dictionary,ta_dictionary = update_dictionaries(
                    commute_dictionary,
                    ta_dictionary,
                    commute_sa2
                    )
                continue
            # Get the TA and the ages of the worker
            commute_ta = ta_dictionary[commute_sa2]
            available_ages = get_available_ages(
                ["F","M"],
                ["60+","30-59","15-29","0-14"],
                individuals_dictionary[commute_ta][commute_sa2]
                )
            if len(available_ages) == 0:
                commute_dictionary,ta_dictionary = update_dictionaries(
                    commute_dictionary,
                    ta_dictionary,
                    commute_sa2
                    )
                continue
            # Get the possible ethnicities of the worker
            available_ethnicities = get_available_ethnicities(
                available_ages,
                individuals_dictionary[commute_ta][commute_sa2]
                )
            # If matching by sector, get available ages, ethnicities and sexes based on age, ethnicity, sector and TA
            if sector_match:
                if workplace_dictionary[wp]["sector"] not in sector_dictionary[workplace_dictionary[wp]["TA"]]:
                    break
                available_ages,available_ethnicities,available_sexes = get_sector_items(
                    available_ages,
                    available_ethnicities,
                    sector_dictionary,
                    workplace_dictionary[wp]["TA"],
                    workplace_dictionary[wp]["sector"],
                    individuals_dictionary[commute_ta][commute_sa2]
                    )
                if available_ages == "ERROR":
                    exclude_sa2s[workplace_dictionary[wp]["sector"]].append(commute_sa2)
                    continue
            # Else just use previously drawn data
            else:
                available_sexes = ["F","M"]
            # Get a random individual with a combination of the available attributes
            final_sex,final_age,final_ethnicity,final_id = get_random_individual(
                available_sexes,
                available_ages,
                available_ethnicities,
                individuals_dictionary[commute_ta][commute_sa2]
                )
            # If individual already assigned to workplace try again
            if final_id in workplace_dictionary[wp]["links"]:
                current_trials += 1
                continue
            current_trials = 0
            workplace_dictionary[wp]["links"].append(final_id)
            individuals_dictionary[commute_ta][commute_sa2][final_sex][final_age][final_ethnicity].remove(final_id)
            workplace_dictionary[wp]["size"] -= 1
        # If workplace is now full then remove it
        if workplace_dictionary[wp]["size"] == 0:
            final_dictionary[wp] = copy.deepcopy(workplace_dictionary[wp])
            del workplace_dictionary[wp]
            del final_dictionary[wp]["size"]
    return final_dictionary, workplace_dictionary, individuals_dictionary, commute_dictionary

def get_metrics(readme,final_dictionary,individuals_dictionary,workplace_dictionary,step):
    """ Gives information regarding the success of each step for the readme

    Args:
      readme (str): Readme for the workplace layer
      final_dictionary (dict): Dictionary of filled workplaces
      individuals_dictionary (dict): Dictionary of unassigned workers
      workplace_dictionary (dict): Dictionary of unfilled workplaces
      step (int): Step number 

    Returns:
      Updated readme
    """
    # Initialise out string and count variables
    out_str = "STEP " + str(step) + "\nFILLED WORKPLACES:\t" + str(len(final_dictionary)) + "\nUNFILLED WORKPLACES:\t" + str(len(workplace_dictionary))
    workers_assigned = 0
    workers_needed = 0
    workers_remaining = 0
    # For filled workplaces count
    for a in final_dictionary:
        workers_assigned +=  len(final_dictionary[a]["links"])
    # For unfilled workplaces count
    for a in workplace_dictionary:
        workers_assigned += len(workplace_dictionary[a]["links"])
        workers_needed += workplace_dictionary[a]["size"]
    # Add to out string
    out_str += "\nROLES ASSIGNED:\t" + str(workers_assigned) + "\nROLES UNFILLED:\t" + str(workers_needed)
    # For unassigned individuals count
    for ta in individuals_dictionary:
        for sa2 in individuals_dictionary[ta]:
            for sex in individuals_dictionary[ta][sa2]:
                for age in individuals_dictionary[ta][sa2][sex]:
                    for ethnicity in individuals_dictionary[ta][sa2][sex][age]:
                        workers_remaining += len(individuals_dictionary[ta][sa2][sex][age][ethnicity])
    # Add to readme and return
    out_str += "\nROLES AVAILABLE:\t" + str(workers_remaining) + "\n\n"
    readme += out_str
    return readme

def make_final_files(
    output_path,
    final_dictionary,
    workplace_dictionary,
    readme
    ):
    """ Converts the final dictionary into a form to be merged in and saves it to files

    Args:
      output_path (str): Path to the output directory
      final_dictionary (dict): Dictionary containing information for the workplace layer
      workplace_dictionary (dict):
      readme (str): Readme for the workplace layer
    """
    if output_path[-1] != "/":
        output_path += "/"
    resultant_dict = {
        "nodes":{},
        "readme":readme
        }
    for item in workplace_dictionary:
        del workplace_dictionary[item]["size"]
        if len(workplace_dictionary[item]["links"]) == 0:
            readme += "WORKPLACE " + item + " IN TA " + workplace_dictionary[item]["TA"] + " HAS NO WORKERS - REMOVING\n"
        else:
            final_dictionary[item] = copy.deepcopy(workplace_dictionary[item])
    readme += "\n"
    for item in final_dictionary:
        resultant_dict["nodes"][item] = copy.deepcopy(final_dictionary[item])
    id_str = id_generator()
    readme = "WORK LAYER:\nUNIQUE PREFIX:\t" + id_str + "\n\n" + readme
    resultant_dict["readme"] = readme
    readme = extract_final_metrics(resultant_dict,readme)
    make_pickle(resultant_dict,output_path,id_str,"wp")
    make_readme(readme,output_path,id_str,"wp")
    make_layer_edgelist_nodelist(resultant_dict,output_path,id_str,"wp")
