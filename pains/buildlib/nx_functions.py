import random

def generate_dictionaries(in_file,cols_list):
    """ Generates shutdown dictionary

    Args: 
      in_file (str): Path to workplace shutdown file
      cols_list (list): Specifies column headings for the workplace shutdown file
    
    Returns:
      Two dictionaries corresponding to the values for each sector from the file and a dictionary of empty lists to be populated for each sector
    """
    # Make list of workplace sectors and read in the shutdown file
    valid_sectors = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S"]
    rf = open(in_file,'r')
    rline = rf.readlines()
    rf.close()
    dict_1 = {}
    dict_2 = {}
    keys_val = {}
    splitter = rline[0].replace('\n','').split(',')
    # Dictionary mapping column indices to headings
    for a in range(len(splitter)):
        if splitter[a] in cols_list:
            keys_val[splitter[a]] = a
    # For all lines in the file
    for a in range(1,len(rline)):
        # Split the line and only use if sector is in desired sectors
        splitter = rline[a].replace('\n','').split(",")
        if splitter[0] not in valid_sectors:
            break
        # Initialise Sector: Empty List 
        dict_2[splitter[0]] = []
        dict_1[splitter[0]] = {"initial_value":0}
        # Convert csv to dictionary
        for b in cols_list:
            dict_1[splitter[0]][b] = float(splitter[keys_val[b]])
    return dict_1,dict_2

def generate_workplace_shutdown(network, dict_1, dict_2, cols_list, worker):
    """ Generates shutdown dictionary

    Args: 
      network (Networkx Graph):
      dict_1 (dict): Dictionary version of census file csv (sector to shutdown probabilities for each level)
      dict_2 (dict): Dictionary mapping sectors to a list of workplace node IDs that are in that sector
      cols_list (list): Specifies column headings for the workplace shutdown file
      worker (bool): Whether or not worker count is used to determine business shutdowns
    
    Returns:
      Two dictionaries corresponding to the values for each sector from the file and a dictionary of empty lists to be populated for each sector
    """
    sub_val = 1
    # Randomise input sector list so workplace shutdowns are randomised
    for sector in dict_1:
        random.shuffle(dict_2[sector])
        for item in cols_list:
           dict_1[sector][item + "ival"] = dict_1[sector]["initial_value"]
    # While there are still sectors with workplace shutdowns to allocate       
    while len(cols_list) > 0:
        # For all sectors
        for sector in dict_1:
            # If there are businesses in the sector
            if dict_1[sector]["initial_value"] != 0:
                # While there are still businesses to shutdown
                while float(dict_1[sector][cols_list[0] + "ival"])/float(dict_1[sector]["initial_value"]) > dict_1[sector][cols_list[0]] and float(dict_1[sector][cols_list[0] + "ival"]) != 0:
                    # If using worker number allocation get worker number
                    if worker:
                        sub_val = len(list(network[dict_2[sector][0]]))
                    # Close businesses and decrement businesses required to shutdown
                    for thing in cols_list:
                         network.nodes[dict_2[sector][0]][thing] = "CLOSED"
                         dict_1[sector][thing + "ival"] -= sub_val
                    del dict_2[sector][0]
        del cols_list[0]
