from .community_functions import generate_individuals_dict, make_events, make_final_files
from .universal_functions import csv_to_dict, get_git_revision_short_hash, get_git_status, get_git_remote

def main(
    individuals_node_list,
    maximum_size,
    minimum_size,
    power_law,
    age_assortativity,
    age_parameters,
    participation,
    long_accept,
    ethnic_assortativity,
    ethnic_assortativity_threshold,
    long_range,
    offset_long_range,
    output_directory,
    threshold,
    prefix,
    initial_string
    ):
    """ Calls the community functions to generate the community layer(s)

    Args:
      individuals_node_list (dict): Dictionary of individuals in the network
      maximum_size (int): Maximum size of a community event
      minimum_size (int): Minimum size of a community event
      powerlaw_value (double): Parameter for the powerlaw distribution of event sizes
      age_assortativity (bool): Whether to use current event age assortativity when filling events
      age_parameters (str): Path to the csv containing the matrix of age assortativities between age groups
      participation (dict): Age-group: Poisson distribution mean for number of events distribution
      long_accept (dict): Age-group: probability of participating in a long-range event
      ethnic_assortativity (bool): Whether to use current event ethnic assortativity when filling events
      ethnic_assortativity_threshold (double): Probability of using ethnic assortativity for a specific group
      long_range (bool): Whether to include long-range links to community events
      offset_long_range (bool): Whether to decrease an individuals' number of short-range links by their number of long-range links
      output_directory (str): Path to the output directory      
      trial_threshold (int): Number of times to try and fill a space in a community event before skipping
      prefix (str): Group prefix (either EX or EV for close or casual community events)
      initial_string (str): Initial string to turn into the readme for this layer
    """
    # Beginning readme preamble and adding input parameters
    readme = initial_string +\
        "\nGIT HASH:" + get_git_revision_short_hash() +\
        "\nGIT REMOTE:" + get_git_remote() +\
        "\nGIT STATUS:" + get_git_status() +\
        "output_directory: " + str(output_directory) + "\n\n" +\
        prefix.lower() + "_parameters\n\n" +\
        "maximum_size: " + str(maximum_size) + "\n" +\
        "minimum_size: " + str(minimum_size) + "\n" +\
        "power_law: " + str(power_law) + "\n" +\
        "age_assortativity: " + str(age_assortativity) + "\n"
    age_list = ["0-14","15-29","30-59","60+"]
    if age_assortativity:
        readme += "age_parameters:\n"
        age_parameters = csv_to_dict(age_parameters)
        for a in age_list:
            readme += "  " + a + ":\n"
            for b in age_list:
                readme += "    " + b + ": " + str(age_parameters[a][b]) + "\n"
    else:
        age_parameters = {}
        for a in age_list:
            age_parameters[a] = {}
            for b in age_list:
                age_parameters[a][b] = 0
    readme += "participation:\n"
    for a in age_list:
        readme += "  " + a + ": " + str(participation[a]) + "\n"
    readme += "long_accept:\n"
    for a in long_accept:
        readme += "  " + a + ": " + str(long_accept[a]) + "\n"
    readme += "ethnic_assortativity:\n" +\
        "  enforce: " + str(ethnic_assortativity) + "\n" +\
        "  threshold: " + str(ethnic_assortativity_threshold) + "\n"
    readme += "include_long_range: " + str(long_range) + "\n"
    readme += "offset_long_range: " + str(offset_long_range) + "\n"
    readme += "trial_threshold: " + str(threshold) + "\n\n"
    # Generate dictionary of individuals that can fill community events
    individuals_dictionary = generate_individuals_dict(
        individuals_node_list,
        participation,
        long_range,
        offset_long_range,
        long_accept
        )
    # Generate dictionary of community events and fill them
    final_dictionary = make_events(
        individuals_dictionary,
        maximum_size,
        minimum_size,
        power_law,
        age_assortativity,
        age_parameters,
        ethnic_assortativity,
        ethnic_assortativity_threshold,
        threshold,
        prefix
        )
    # Convert the final community event dictionary 
    make_final_files(
        output_directory,
        final_dictionary,
        readme,
        prefix
        )
