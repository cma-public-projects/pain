import random
import copy

from .universal_functions import extract_final_metrics, gen_indices_dict, get_available_ages, get_available_ethnicities, get_commute_sa2, get_random_individual, id_generator, make_pickle, make_readme, make_layer_edgelist_nodelist, update_dictionaries

def generate_individuals_dict(
    labels,
    individual_nodes,
    excluded_tas,
    excluded_sa2s
    ):
    """ Generates the individuals dictionary for putting students into a school

    Args: 
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individual_nodes (str): Path to the individuals nodelist csv
      excluded_tas (list): List of TAs to exclude from the network
      excluded_sa2s (list): List of SA2s to exclude from the network      
    
    Returns:
      Student SA2 to TA dictionary and individuals dictionary of students
    """
    rf = open(individual_nodes,"r")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    # Building indices mapping and initialise output dictionaries
    indices_dict = gen_indices_dict(index_split,labels)
    individuals_dictionary = {}
    ta_dictionary = {}
    # For every individual
    for a in range(1,len(rline)):
        line_split = rline[a].split(",")
        # If they are excluded or not of student age then skip
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in excluded_sa2s:
            continue
        if line_split[indices_dict["age"]] == "30-59" or line_split[indices_dict["age"]] == "60+":
            continue
        # Else if they are a student add them to the individuals dictionary and to the TA dictionary
        if line_split[indices_dict["student"]] == "Y":
            current_dict = individuals_dictionary
            if line_split[indices_dict["TA"]] not in current_dict:
                current_dict[line_split[indices_dict["TA"]]] = {}
            if line_split[indices_dict["SA2"]] not in current_dict[line_split[indices_dict["TA"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]] = {}
            if line_split[indices_dict["sex"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]] = {}
            if line_split[indices_dict["age"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]] = {}
            if line_split[indices_dict["ethnicity"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]] = []
            current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]].extend([line_split[indices_dict["node_id"]]])
            if line_split[indices_dict["SA2"]] not in ta_dictionary:
                ta_dictionary[line_split[indices_dict["SA2"]]] = line_split[indices_dict["TA"]]
    return ta_dictionary, individuals_dictionary

def generate_schools_dictionaries(
    labels,
    school_file,
    parameters,
    excluded_tas,
    excluded_sa2s,
    readme
    ):
    """ Generates the schools dictionary for putting students into a school

    Args: 
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      school_file (str): Path to the schools nodelist csv
      parameters (dict): Dictionary of parameters for schools
      excluded_tas (list): List of TAs to exclude from the network
      excluded_sa2s (list): List of SA2s to exclude from the network      
      readme (str): Readme for the schools layer

    Returns:
      School dictionaries for single age band and two age band schools, the number of schools and the readme
    """
    rf = open(school_file,"r",encoding="utf-8")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    # Building indices mapping and initialise output dictionaries
    indices_dict = gen_indices_dict(index_split,labels)
    one_age = {}
    multi_age = {}
    school_index = 1
    # For all schools in the nodelist
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if line_split[indices_dict["type"]] == "Correspondence School":
            continue
        if line_split[indices_dict["type"]] not in parameters:
            readme += "Line " + str(a) + " of school file incorrectly formatted - SKIPPING\n"
            continue
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in excluded_sa2s:
            continue
        # If an included school then find out the age range
        if parameters[line_split[indices_dict["type"]]]["under_15"] == 0 or parameters[line_split[indices_dict["type"]]]["over_14"] == 0:
            current_dictionary = one_age
        else:
            current_dictionary = multi_age
        # Get school and student information
        ta = line_split[indices_dict["TA"]]
        sa2 = line_split[indices_dict["SA2"]]
        maori = int(line_split[indices_dict["maori"]])
        pacific = int(line_split[indices_dict["pacific"]])
        other = int(line_split[indices_dict["total_students"]]) - maori - pacific
        eth_labels = [["Maori","MaoriPacific"],["Pacific","MaoriPacific"],["Other"]]
        eth_counts = [maori,pacific,other]
        if line_split[indices_dict["coed"]] == "Co-Ed":
            sexes = ["F","M"]
        elif line_split[indices_dict["coed"]] == "Boys":
            sexes = ["M"]
        elif line_split[indices_dict["coed"]] == "Girls":
            sexes = ["F"]
        age_dictionary = {}
        # Get school type
        if parameters[line_split[indices_dict["type"]]]["under_15"] != 0:
            age_dictionary["0-14"] = parameters[line_split[indices_dict["type"]]]["under_15"]
        if parameters[line_split[indices_dict["type"]]]["over_14"] != 0:
            age_dictionary["15-29"] = parameters[line_split[indices_dict["type"]]]["over_14"]
        if parameters[line_split[indices_dict["type"]]]["secondary"]:
            prefix = "SC"
        else:
            prefix = "PM"
        # Name school
        name = prefix + str(ta).zfill(2) + str(line_split[indices_dict["node_id"]])
        school_index += 1
        # Add to correct school age dictionary
        current_dictionary[name] = {
            "TA":ta,
            "SA2":sa2,
            "links":[],
            "age":copy.deepcopy(age_dictionary),
            "sex":sexes,
            "ethnicity":{
                "ethnicity_labels":eth_labels,
                "ethnicity_count":eth_counts
                }
            }
    return one_age, multi_age, school_index, readme

# This function generates the ece dictionary
def generate_ece_dictionary(
    labels,
    ece_file,
    parameters,
    excluded_tas,
    excluded_sa2s,
    school_index,
    readme
    ):
    """ Generates the ece dictionary for putting students into a school

    Args: 
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      ece_file (str): Path to the ece nodelist csv
      parameters (dict): Dictionary of parameters for schools
      excluded_tas (list): List of TAs to exclude from the network
      excluded_sa2s (list): List of SA2s to exclude from the network     
      school_index (int): Number of schools to place ece in correct location in dict 
      readme (str): Readme for the schools layer

    Returns:
      ECE dictionaries for single age band and the readme
    """
    rf = open(ece_file,"r",encoding="utf-8")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    # Building indices mapping and initialise output dictionary
    indices_dict = gen_indices_dict(index_split,labels)
    one_age = {}
    # For all eces
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if line_split[indices_dict["type"]] == "Correspondence School":
            continue
        if line_split[indices_dict["type"]] not in parameters:
            readme += "Line " + str(a) + " of ECE file incorrectly formatted - SKIPPING\n"
            continue
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in excluded_sa2s:
            continue
        # ECEs should only be for young students
        if parameters[line_split[indices_dict["type"]]]["under_15"] == 0 or parameters[line_split[indices_dict["type"]]]["over_14"] == 0:
            current_dictionary = one_age
        else:
            print("ISSUE IN ECE FILE")
            quit()
        # Get school and student information
        ta = line_split[indices_dict["TA"]]
        sa2 = line_split[indices_dict["SA2"]]
        maori = int(line_split[indices_dict["maori"]])
        pacific = int(line_split[indices_dict["pacific"]])
        other = int(line_split[indices_dict["total_students"]]) - maori - pacific
        eth_labels = [["Maori","MaoriPacific"],["Pacific","MaoriPacific"],["Other"]]
        eth_counts = [maori,pacific,other]
        if line_split[indices_dict["coed"]] == "Co-Ed":
            sexes = ["F","M"]
        elif line_split[indices_dict["coed"]] == "Boys":
            sexes = ["M"]
        elif line_split[indices_dict["coed"]] == "Girls":
            sexes = ["F"]
        age_dictionary = {}
        if parameters[line_split[indices_dict["type"]]]["under_15"] != 0:
            age_dictionary["0-14"] = parameters[line_split[indices_dict["type"]]]["under_15"]
        if parameters[line_split[indices_dict["type"]]]["over_14"] != 0:
            age_dictionary["15-29"] = parameters[line_split[indices_dict["type"]]]["over_14"]
        if parameters[line_split[indices_dict["type"]]]["secondary"]:
            prefix = "SC"
        else:
            prefix = "PM"
        # Name school
        name = prefix + str(ta).zfill(2) + str(line_split[indices_dict["node_id"]])
        school_index += 1
        # Add to output dictionary
        current_dictionary[name] = {
            "TA":ta,
            "SA2":sa2,
            "age":copy.deepcopy(age_dictionary),
            "links":[],
            "sex":sexes,
            "ethnicity":{
                "ethnicity_labels":eth_labels,
                "ethnicity_count":eth_counts
                }
            }
    return one_age, readme

def get_student_attributes(
    available_sexes,
    available_ages,
    available_ethnicities,
    individuals_dictionary,
    school_dictionary
    ):
    """ Chooses a random student from a dictionary based off of specified sexes, ages and ethnicities

    Args:
      available_sexes (list): List of available sexes that a random student could have
      available_ages (list): List of available ages that a random student could have
      available_ethnicities (list): List of available ethnicities that a random student could have
      individuals_dict (dict): Dictionary of student and their attributes to choose from
      school_dictionary (dict): Dictionary to get student counts for

    Returns:
      The chosen students' age, sex, ethnicity and ID
    """
    tuples_list = []
    probs_list = [0]
    # Append counts of each possible sex, age, ethnicity to turn into a probability
    for sex in available_sexes:
        for age in available_ages:
            if age not in individuals_dictionary[sex]:
                continue
            maori = False
            pacific = False
            other = False
            for ethnicity in available_ethnicities:
                if ethnicity not in individuals_dictionary[sex][age]:
                    continue
                if len(individuals_dictionary[sex][age][ethnicity]) == 0:
                    continue
                if ethnicity == "Maori" and school_dictionary["ethnicity"]["ethnicity_count"][0] != 0:
                    maori = True
                elif ethnicity == "Pacific" and school_dictionary["ethnicity"]["ethnicity_count"][1] != 0:
                    pacific = True
                elif ethnicity == "MaoriPacific":
                    if school_dictionary["ethnicity"]["ethnicity_count"][0] != 0:
                        maori = True
                    if school_dictionary["ethnicity"]["ethnicity_count"][1] != 0:
                        pacific = True
                elif ethnicity == "Other" and school_dictionary["ethnicity"]["ethnicity_count"][2] != 0:
                    other = True
            if maori:
                tuples_list.append(([age],school_dictionary["ethnicity"]["ethnicity_labels"][0],[sex],0))
                probs_list.append((float(school_dictionary["ethnicity"]["ethnicity_count"][0])/sum(school_dictionary["ethnicity"]["ethnicity_count"]))*school_dictionary["age"][age] + probs_list[-1])
            if pacific:
                tuples_list.append(([age],school_dictionary["ethnicity"]["ethnicity_labels"][1],[sex],1))
                probs_list.append((float(school_dictionary["ethnicity"]["ethnicity_count"][1])/sum(school_dictionary["ethnicity"]["ethnicity_count"]))*school_dictionary["age"][age] + probs_list[-1])
            if other:
                tuples_list.append(([age],school_dictionary["ethnicity"]["ethnicity_labels"][2],[sex],2))
                probs_list.append((float(school_dictionary["ethnicity"]["ethnicity_count"][2])/sum(school_dictionary["ethnicity"]["ethnicity_count"]))*school_dictionary["age"][age] + probs_list[-1])
    del probs_list[0]
    if len(tuples_list) == 0:
        return "ERROR","ERROR","ERROR","ERROR"
    # Select a student at random based on counts
    random_val = random.uniform(0,probs_list[-1])
    for a in range(len(probs_list)):
        if random_val <= probs_list[a]:
            return tuples_list[a][0],tuples_list[a][1],tuples_list[a][2],tuples_list[a][3]
                 
def fill_schools(
    final_dictionary,
    individuals_dictionary,
    commute_dictionary,
    schools_dictionary,
    ta_dictionary,
    commute_key
    ):
    """ Fills up a schools dictionary

    Args:
      final_dictionary (dict): Final dictionary of the school layer
      individuals_dictionary (dict): Dictionary of students to fill up the schools
      commute_dictionary (dict): commute_ed_prop_SA2 as a dictionary gives proportions of student SA2s in a school SA2
      schools_dictionary (dict): Dictionary of schools to be filled
      ta_dictionary (dict): Dictionary of student SA2 to student TA
      commute_key (str): SA2 or TA depending on where students should be sourced from 

    Returns:
      Updated dictionaries for full schools, unfilled schools, available students and commuting SA2s
    """
    # Randomise school order so they are filled at random
    keys = list(schools_dictionary.keys())
    random.shuffle(keys)
    # For each school
    for sc in keys:
        exclude_sa2s = []
        # While there are still roles to be filled
        while sum(schools_dictionary[sc]["ethnicity"]["ethnicity_count"]) > 0:
            # Get an SA2 a student commutes from
            commute_sa2 = get_commute_sa2(
                commute_dictionary,
                commute_key,
                schools_dictionary[sc][commute_key],
                exclude_sa2s
                )
            if commute_sa2 == "ERROR":
                break
            if commute_sa2 not in ta_dictionary:
                commute_dictionary,ta_dictionary = update_dictionaries(
                    commute_dictionary,
                    ta_dictionary,
                    commute_sa2
                    )
                continue
            # Get the TA that the student is in and their possible ages
            commute_ta = ta_dictionary[commute_sa2]
            available_ages = get_available_ages(
                schools_dictionary[sc]["sex"],
                list(schools_dictionary[sc]["age"].keys()),
                individuals_dictionary[commute_ta][commute_sa2]
                )
            if len(available_ages) == 0:
                commute_dictionary,ta_dictionary = update_dictionaries(
                    commute_dictionary,
                    ta_dictionary,
                    commute_sa2
                    )
                continue
            # Get the student's possible ethnicities
            available_ethnicities = get_available_ethnicities(
                available_ages,
                individuals_dictionary[commute_ta][commute_sa2]
                )
            # Get the student and their attributes
            available_ages,available_ethnicities,available_sexes,list_index = get_student_attributes(
                schools_dictionary[sc]["sex"],
                available_ages,
                available_ethnicities,
                individuals_dictionary[commute_ta][commute_sa2],
                schools_dictionary[sc]
                )
            if available_ages == "ERROR":
                exclude_sa2s.append(commute_sa2)
                continue
            try:
                # Get final student (basically a redundancy/error check)
                final_sex,final_age,final_ethnicity,final_id = get_random_individual(
                    available_sexes,
                    available_ages,
                    available_ethnicities,
                    individuals_dictionary[commute_ta][commute_sa2]
                    )
            except:
               print(individuals_dictionary[commute_ta][commute_sa2])
               print(available_ages)
               print(available_sexes)
               print(available_ethnicities)
               print(len(final_dictionary))
               quit()
            # Add link between student and school
            schools_dictionary[sc]["links"].append(final_id)
            individuals_dictionary[commute_ta][commute_sa2][final_sex][final_age][final_ethnicity].remove(final_id)
            schools_dictionary[sc]["ethnicity"]["ethnicity_count"][list_index] -= 1
        # If school ethnicity count is full delete it as no longer required
        if sum(schools_dictionary[sc]["ethnicity"]["ethnicity_count"]) == 0:
            final_dictionary[sc] = copy.deepcopy(schools_dictionary[sc])
            del schools_dictionary[sc]
            del final_dictionary[sc]["age"]
            del final_dictionary[sc]["sex"]
            del final_dictionary[sc]["ethnicity"]
    return final_dictionary,schools_dictionary,individuals_dictionary,commute_dictionary         

def get_students(in_dict):
    """ Gets the number of students in a school dictionary

    Args: 
      in_dict (dict): Dictionary corresponding to the students in and required by a school

    Returns:
      The number of students in a school and the number remaining
    """
    count = 0
    needed = 0
    for a in in_dict:
        count += len(in_dict[a]["links"])
        if "ethnicity" in in_dict[a]:
            needed += sum(in_dict[a]["ethnicity"]["ethnicity_count"])
    return count,needed

def update_readme(readme,final_dictionary,school_1,school_multi,ece,individuals):
    """ Updates the school readme with metrics at each Step

    Args:
      readme (str): School readme to be updated
      final_dictionary (dict): Final dictionary for the school layer
      school_1 (dict): Dictionary of schools with one age bracket
      school_2 (dict): Dictionary of schools with two age brackets
      ece (dict): Dictionary of early childhood education centres
      individuals (dict): Dictionary of students
    """
    readme += "SCHOOLS FILLED:\t" + str(len(final_dictionary)) + "\n"
    final_count, final_needed = get_students(final_dictionary)
    readme += "STUDENTS ASSIGNED TO FULL SCHOOLS:\t" + str(final_count) + "\n"

    readme += "UNFILLED SCHOOLS WITH ONE AGE BRACKET:\t" + str(len(school_1)) + "\n"
    a1_count,a1_needed = get_students(school_1)
    readme += "STUDENTS IN UNFILLED SCHOOLS WITH ONE AGE BRACKET:\t" + str(a1_count) + "\n"
    readme += "STUDENTS NEEDED FOR UNFILLED SCHOOLS WITH ONE AGE BRACKET:\t" + str(a1_needed) + "\n"

    readme += "UNFILLED SCHOOLS WITH TWO AGE BRACKETS:\t" + str(len(school_multi)) + "\n"
    a2_count,a2_needed = get_students(school_multi)
    readme += "STUDENTS IN UNFILLED SCHOOLS WITH TWO AGE BRACKETS:\t" + str(a2_count) + "\n"
    readme += "STUDENTS NEEDED FOR UNFILLED SCHOOLS WITH TWO AGE BRACKETS:\t" + str(a2_needed) + "\n"

    readme += "UNFILLED ECE CENTRES:\t" + str(len(ece)) + "\n"
    a2_count,a2_needed = get_students(ece)
    readme += "STUDENTS IN UNFILLED ECE CENTRES:\t" + str(a2_count) + "\n"
    readme += "STUDENTS NEEDED FOR UNFILLED ECE CENTRES:\t" + str(a2_needed) + "\n"

    count = 0
    for ta in individuals:
        for sa2 in individuals[ta]:
            for sex in individuals[ta][sa2]:
                for age in individuals[ta][sa2][sex]:
                    for eth in individuals[ta][sa2][sex][age]:
                        count += len(individuals[ta][sa2][sex][age][eth])
    readme += "REMAINING STUDENTS AVAILABLE:\t" + str(count) + "\n\n"
    return readme

def make_final_files(
    output_path,
    final_dictionary,
    ece,
    schools_1,
    schools_2,
    readme
    ):
    """ Converts the final dictionary into a form to be merged in and saves it to files

    Args:
      output_path (str): Path to the output directory
      final_dictionary (dict): Dictionary containing information for the school layer
      ece (dict): Dictionary of early childhood education centres
      schools_1 (dict): Dictionary of schools with one age bracket
      schools_2 (dict): Dictionary of schools with two age brackets
      readme (str): Readme for the school layer
    """
    if output_path[-1] != "/":
        output_path += "/"
    resultant_dict = {
        "nodes":{},
        "readme":readme
        }
    # Copy items to the final_dictionary from ece dictionary
    for item in ece:
        del ece[item]["age"]
        del ece[item]["sex"]
        del ece[item]["ethnicity"]
        final_dictionary[item] = copy.deepcopy(ece[item])
    # Copy items to the final_dictionary from the schools_1 dictionary
    for item in schools_1:
        del schools_1[item]["age"]
        del schools_1[item]["sex"]
        del schools_1[item]["ethnicity"]
        final_dictionary[item] = copy.deepcopy(schools_1[item])
    # Copy items to the final_dictionary from the schools_2 dictionary        
    for item in schools_2:
        del schools_2[item]["age"]
        del schools_2[item]["sex"]
        del schools_2[item]["ethnicity"]
        final_dictionary[item] = copy.deepcopy(schools_2[item])
    # Copy items to the final (final) dictionary to tidy-up structure
    for item in final_dictionary:
        resultant_dict["nodes"][item] = copy.deepcopy(final_dictionary[item])
    id_str = id_generator()
    readme = "SCHOOL LAYER:\nUNIQUE PREFIX:\t" + id_str + "\n\n" + readme
    resultant_dict["readme"] = readme
    readme = extract_final_metrics(resultant_dict,readme)
    make_pickle(resultant_dict,output_path,id_str,"sc")
    make_readme(readme,output_path,id_str,"sc")
    make_layer_edgelist_nodelist(resultant_dict,output_path,id_str,"sc")

