import math
import random
import networkx as nx
import csv
from datetime import date

casual_to_close_subgroup_map = {
    'DW': 'DG',
    'WE': 'WG',
    'WP': 'WG',
    'SC': 'SG',
    'PM': 'PG',
}

casual_to_close_relabel_map = {
    'DW': 'DZ',
    'WE': 'WZ',
    'WP': 'WZ',
    'SC': 'SZ',
    'PM': 'PZ',
}

def apply_close_contact(G, group, config):
    """ Constructs the close contact structure for a given group

    Args:
      G (Networkx Graph): Graph to 'close-contactify'
      group (str): Group to add close contacts too
      config (dict): Config dictionary from the input YAML to generate_pain.py

    Notes: (Naively) Assumes that construction for the group hasn't already occurred
    """
    # Get group type and check whether to add close contacts
    grp_type = group[:2].upper()
    if grp_type not in config:
        return

    # Get neighbour attributes
    neighbours = list(G[group])
    group_size = len(neighbours)

    params = config[grp_type]
    if params['threshold'] < group_size:
        # grp is larger than threshold
        ## partitioning step
        random.shuffle(neighbours)
        # counters for subgroup number and individual number respectively
        ngrp, i = 0, 0
        # construct new subgroups for correct number of individuals
        while i < group_size:
            sz_proposed = random.lognormvariate(math.log(params['mean']), params['sigma'])
            # int(n) + 1 is a ceil function
            sz = min(group_size, math.ceil(sz_proposed))
            new_grp_name = f"{casual_to_close_subgroup_map[grp_type]}{group[2:]}_{ngrp}"
            # construct new subgroup and add edges
            G.add_node(new_grp_name, **G.nodes[group])
            G.add_edges_from((indv, new_grp_name) for indv in neighbours[i:i+sz])
            ngrp += 1
            i += sz
        # overcover step
        n_overcovers = int(params['overcover'] * group_size / params['mean'])
        for n in range(n_overcovers):
            sz_proposed = random.lognormvariate(math.log(params['mean']), params['sigma'])
            # int(n) + 1 is a ceil function
            sz = min(group_size, math.ceil(sz_proposed))
            new_grp_name = f"{casual_to_close_subgroup_map[grp_type]}{group[2:]}_{ngrp+n}"
            indv_nodes = random.sample(neighbours, sz)
            # construct new subgroup and add edges
            G.add_node(new_grp_name, **G.nodes[group])
            G.add_edges_from((indv, new_grp_name) for indv in indv_nodes)
    else:
        # relabel group if below threshold
        new_grp_name = f"{casual_to_close_relabel_map[grp_type]}{group[2:]}_ALL"
        nx.relabel_nodes(G, {group: new_grp_name}, copy=False)

def close_contactify(G, config, readme):
    """ Adds close contact structure for a given network

    Args:
      G (Networkx Graph): Graph to 'close-contactify'
      config (dict): Config dictionary from the input YAML to generate_pain.py
      readme (str): Readme string to append information to
    
    Returns: 
      The constructed readme containing the information of the close-contact process
    """
    # Get list of group nodes
    grp_nodes_list = []
    for node, info in G.nodes(data=True):
        if info['bipartite'] == 0:
            grp_nodes_list.append(node)
    # Add close contact structure to each group node
    for node in grp_nodes_list:
        apply_close_contact(G, node, config)
    # Update readme
    readme += 'CLOSE CONTACT STRUCTURE MADE ON NETWORK '  + '\n'
    readme += 'DATE: ' + str(date.today()) + '\n'
    return readme

def save_nodelist_edgelist_csv(G, p, out_dir, config):
    """ Saves the nodelist and edgelist for non-community layers including the close-contact structures

    Args:
      G (Networkx Graph): Graph to 'close-contactify'
      p (str): Output path of graph
      out_dir (str): Path of output directory
      config (dict): Config dictionary from the input YAML to generate_pain.py
    """
    # Get list of individual and group nodes
    individual_nodes = [x for x,y in G.nodes(data=True) if y['bipartite'] == 1]
    group_nodes = [x for x,y in G.nodes(data=True) if y['bipartite'] == 0]
    group_nodes.sort()
    # Get output names
    network_name = str(p.name)
    if network_name.split('.')[1] == 'full':
        network_ID = network_name.split('.')[0] + '_full'
    else:
        network_ID = network_name.split('.')[0]

    #********************************* Individuals nodelist for entire network ***************************************************
    indiv_data = []
    for i in individual_nodes:
        data = {}
        data['node'] = i
        for key, value in G.nodes[i].items():
            data[key]=value
        indiv_data.append(data)

    # Define output paths
    if out_dir[-1]=='/':
        indiv_nodelist_csv = out_dir + network_ID + '_indiv_nodelist.csv'
    else:
        indiv_nodelist_csv = out_dir + '/' +  network_ID + '_indiv_nodelist.csv'

    with open(indiv_nodelist_csv, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=['node','bipartite','age','TA','SA2','sex','ethnicity','vaccine_dates'])
        writer.writeheader()
        for data in indiv_data:
            writer.writerow(data)
        
    # *********************************** GROUP NODELIST(S) AND EDGELIST(S) *******************************************************************

    # The nodes need both the key and the dictionary key-value pairs contained in G.nodes[key]. Thus make these datasets. 
    if not(config['save_full_lists']):
        group_data = []
        for g in group_nodes:
            data = {}
            data['GROUP_ID'] = g
            for key, value in G.nodes[g].items():
                data[key]=value
            group_data.append(data)

        # Define output paths.
        if out_dir[-1]=='/':
            group_nodelist_csv = out_dir +  network_ID + '_group_nodelist.csv'
            edge_list_csv = out_dir + network_ID + '_edgelist.csv'
        else:
            group_nodelist_csv = out_dir + '/' +  network_ID + '_group_nodelist.csv'
            edge_list_csv = out_dir + '/' + network_ID  + '_edgelist.csv'

        # Write .csv files
        with open(group_nodelist_csv, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['GROUP_ID','TA','SA2','bipartite'], extrasaction='ignore')
            writer.writeheader()
            for data in group_data:
                writer.writerow(data)
            
        fh = open(edge_list_csv, "wb")
        nx.write_edgelist(G, fh, delimiter=',', data=False)
        
    else:
        group_nodes_sp = []
        group_nodes_d = []
        group_nodes_w = []
        for g in group_nodes:
            if g[0] not in ['S','P','D','W']:
                continue
            data = {}
            data['GROUP_ID'] = g
            for key, value in G.nodes[g].items():
                data[key]=value
            if g[0]=='D':
                group_nodes_d.append(data)
            elif g[0]=='W':
                group_nodes_w.append(data)
            elif g[0]=='S' or g[0]=='P':
                group_nodes_sp.append(data)
        
        # Define output paths.
        if out_dir[-1]=='/'=='/':
            group_nodelist_csv_sc = out_dir +  network_ID + '_group_nodelist_SP.csv'
            edge_list_csv_sc = out_dir +  network_ID +  '_edgelist_SP.csv'
            group_nodelist_csv_wp = out_dir +  network_ID + '_group_nodelist_W.csv'
            edge_list_csv_wp = out_dir +  network_ID +  '_edgelist_W.csv'
            group_nodelist_csv_dw = out_dir +  network_ID + '_group_nodelist_D.csv'
            edge_list_csv_dw = out_dir +  network_ID +  '_edgelist_D.csv'
        else:
            group_nodelist_csv_sc = out_dir + '/' + network_ID + '_group_nodelist_SP.csv'
            edge_list_csv_sc = out_dir + '/' + network_ID +  '_edgelist_SP.csv'
            group_nodelist_csv_wp = out_dir + '/' + network_ID + '_group_nodelist_W.csv'
            edge_list_csv_wp = out_dir + '/' + network_ID +  '_edgelist_W.csv'
            group_nodelist_csv_dw = out_dir + '/' + network_ID + '_group_nodelist_D.csv'
            edge_list_csv_dw = out_dir + '/' + network_ID +  '_edgelist_D.csv'

        # Write nodelist csvs
        with open(group_nodelist_csv_sc, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['GROUP_ID','TA','SA2','bipartite'], extrasaction='ignore')
            writer.writeheader()
            for data in group_nodes_sp:
                writer.writerow(data)

        with open(group_nodelist_csv_wp, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['GROUP_ID','TA','SA2','bipartite'], extrasaction='ignore')
            writer.writeheader()
            for data in group_nodes_w:
                writer.writerow(data)
        
        with open(group_nodelist_csv_dw, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['GROUP_ID','TA','SA2','bipartite'], extrasaction='ignore')
            writer.writeheader()
            for data in group_nodes_d:
                writer.writerow(data)

        edges_sp = []
        edges_d = []
        edges_w = []
        for edges in list(G.edges(data=False)):
            indiv = edges[0]
            group = edges[1]
            if group[0]=='S' or group[0]=='P':
                edges_sp.append({'INDIVIDUAL': indiv,'GROUP NODE': group})
            elif group[0]=='D':
                edges_d.append({'INDIVIDUAL': indiv,'GROUP NODE': group})
            elif group[0]=='W':
                edges_w.append({'INDIVIDUAL': indiv,'GROUP NODE': group})

        # Write edgelist csvs
        with open(edge_list_csv_sc, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['INDIVIDUAL','GROUP NODE'], extrasaction='ignore')
            writer.writeheader()
            for data in edges_sp:
                writer.writerow(data)

        with open(edge_list_csv_dw, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['INDIVIDUAL','GROUP NODE'], extrasaction='ignore')
            writer.writeheader()
            for data in edges_d:
                writer.writerow(data)

        with open(edge_list_csv_wp, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['INDIVIDUAL','GROUP NODE'], extrasaction='ignore')
            writer.writeheader()
            for data in edges_w:
                writer.writerow(data)