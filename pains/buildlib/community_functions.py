# This function generates the individuals nodelist for the community layer updating it to include work and school sa2s
import random
import pickle
import powerlaw
from copy import deepcopy
import numpy as np

from .universal_functions import extract_final_metrics, get_available_ages, get_available_ethnicities, \
     get_git_revision_short_hash, gen_indices_dict, get_random_individual, id_generator, make_pickle, make_readme

def generate_new_nodelist(
    labels, 
    individuals_file, 
    sa2_ta_concordance_file, 
    wp_commute, 
    read_wp, 
    wp_file, 
    sc_commute, 
    read_sc, 
    sc_file, 
    exclude_tas, 
    exclude_sa2s, 
    outpath):
    """ Converts the input nodelists and uses the built school and dwelling layers into a community nodelist

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individuals_file (str): Path to individuals nodelist csv
      sa2_ta_concordance_file (str): Path to SA2 to TA concordance csv
      wp_commute (bool): Whether to use the workplace layer to get short-range links to community SA2s
      read_wp (bool): Whether to read a network_wp.pkl or use current_wp.pkl
      wp_file (str): Path to network_wp.pkl if desired
      sc_commute (bool): Whether to use the school layer to get short-range links to community SA2s
      read_sc (bool): Whether to read a network_sc.pkl or use current_sc.pkl
      sc_file (str): Path to network_sc.pkl if desired
      exclude_tas (list): List of TAs to exclude
      exclude_sa2s (list): List of SA2s to exclude

    Returns: 
      The individuals nodelist dictionary with community event information and the start of the readme string for the community layer
    """
    concordance_dict = {
        "TA":{},
        "SA2":{}
        }
    initial_string = "COMMUNITY YAML AND INPUT\n" +\
        "\nGIT HASH:" + get_git_revision_short_hash() +\
        "\nGENERIC\n\n" +\
        "individuals_file: " + str(individuals_file) + "\n" + \
        "excluded_sa2s: " + str(exclude_sa2s) + "\n" +\
        "excluded_tas: " + str(exclude_tas) + "\n" +\
        "sa2_ta_concordance_file: " + str(sa2_ta_concordance_file) + "\n" +\
        "read_wp: " + str(read_wp) + "\n" +\
        "read_sc: " + str(read_sc) + "\n"
    rf = open(sa2_ta_concordance_file,"r")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    indices_dict = gen_indices_dict(index_split,labels["concordance_labels"])
    # Make a dictionary of SA2 to TA concordance 
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if line_split[indices_dict["TA"]] not in concordance_dict["TA"]:
            concordance_dict["TA"][line_split[indices_dict["TA"]]] = []
        concordance_dict["TA"][line_split[indices_dict["TA"]]].append(line_split[indices_dict["SA2"]])
        concordance_dict["SA2"][line_split[indices_dict["SA2"]]] = line_split[indices_dict["TA"]]
    individuals_node_list = {}
    rf = open(individuals_file,"r")
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    indices_dict = gen_indices_dict(index_split,labels["ih_labels"])
    # Run through every individual and exclude them if they are in an excluded TA or SA2
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if int(line_split[indices_dict["TA"]]) in exclude_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in exclude_sa2s:
            continue
        individuals_node_list[line_split[indices_dict["node_id"]]] = {
            "TA":line_split[indices_dict["TA"]],
            "SA2":line_split[indices_dict["SA2"]],
            "sex":line_split[indices_dict["sex"]],
            "ethnicity":line_split[indices_dict["ethnicity"]],
            "age":line_split[indices_dict["age"]],
            "long":[],
            "short":[]
            }
        # Add a randomly chosen SA2 for each long-range TA link for each individual to the nodelist dictionary
        if line_split[indices_dict["community_long_links"]] != "NA" and line_split[indices_dict["community_long_links"]] != "":
            sec_split = line_split[indices_dict["community_long_links"]].split(";")
            for ta in sec_split:
                if int(ta) in exclude_tas:
                    continue
                individuals_node_list[line_split[indices_dict["node_id"]]]["long"].append((ta,random.choice(concordance_dict["TA"][ta])))
    # Extract information from work layer
    if wp_commute:
        if read_wp:
            rf_wp = wp_file
        else:
            rf_wp = outpath + "current_wp.pkl"
        rf = open(rf_wp,"rb")
        rf_dict = pickle.load(rf)
        rf.close()
        # Add an individuals work SA2 to possible short-range links list
        for node in rf_dict["nodes"]:
            for link in rf_dict["nodes"][node]["links"]:
                individuals_node_list[link]["short"].append((rf_dict["nodes"][node]["TA"],rf_dict["nodes"][node]["SA2"]))
    # Extract information from school layer
    if sc_commute:
        if read_sc:
            rf_wp = sc_file
        else:
            rf_wp = outpath + "current_sc.pkl"
        rf = open(rf_wp,"rb")
        rf_dict = pickle.load(rf)
        rf.close()
        # Add an individuals school SA2 to possible short-range links list
        for node in rf_dict["nodes"]:
            for link in rf_dict["nodes"][node]["links"]:
                individuals_node_list[link]["short"].append((rf_dict["nodes"][node]["TA"],rf_dict["nodes"][node]["SA2"]))
    return individuals_node_list,initial_string

def generate_individuals_dict(individuals_node_list, participation, long_range, offset_long_range, long_accept):
    """ Converts the node list into a dictionary to be depopulated as events are filled

    Args:
      individuals_node_list (dict): Individuals nodelist dictionary
      participation (dict): Age-group: Poisson distribution mean for number of events distribution
      long_range (bool): Whether to include long-range links to community events
      offset_long_range (bool): Whether to decrease an individuals' number of short-range links by their number of long-range links
      long_accept (dict): Age-group: probability of participating in a long-range event

    Returns: 
      Dictionary of individuals to add to community events in each SA2

    Notes: 
      If an individual has multiple community events in an SA2, they are added multiple times to the list for that SA2
    """
    # Run through every individual in the nodelist
    individuals_dictionary = {}
    for individual in individuals_node_list:
        age = individuals_node_list[individual]["age"]
        sex = individuals_node_list[individual]["sex"]
        ethnicity = individuals_node_list[individual]["ethnicity"]
        # Get the number of events the individual participates in (age-group determined)
        ind_participation = np.random.poisson(participation[age] - 1) + 1
        # Add long-range links if required
        if long_range:
            # For each long-range link, probability of including is given by long_accept (age-group determined)
            for long_tuple in individuals_node_list[individual]["long"]:
                if random.uniform(0,1) > long_accept[age]:
                    continue
                ta = long_tuple[0]
                sa2 = long_tuple[1]
                # Add individual to dictionary of individuals to include in community events for that specific SA2
                if ta not in individuals_dictionary:
                    individuals_dictionary[ta] = {}
                if sa2 not in individuals_dictionary[ta]:
                    individuals_dictionary[ta][sa2] = {}
                if sex not in individuals_dictionary[ta][sa2]:
                    individuals_dictionary[ta][sa2][sex] = {}
                if age not in individuals_dictionary[ta][sa2][sex]:
                    individuals_dictionary[ta][sa2][sex][age] = {}
                if ethnicity not in individuals_dictionary[ta][sa2][sex][age]:
                    individuals_dictionary[ta][sa2][sex][age][ethnicity] = []
                individuals_dictionary[ta][sa2][sex][age][ethnicity].append(individual)
                # offset the number of short-range links by the number of long-range links if desired
                if offset_long_range:
                    ind_participation -= 1
        # For each (remaining) number of events add a short-range link
        while ind_participation > 0:
            # 50% chance to use an individuals workplace/school SA2 or just use their dwelling SA2
            if len(individuals_node_list[individual]["short"]) == 0 or random.uniform(0,1) < 0.5:
                ta = individuals_node_list[individual]["TA"]
                sa2 = individuals_node_list[individual]["SA2"]
            else:
                # Choose either the workplace or school SA2
                short_tuple = random.choice(individuals_node_list[individual]["short"])
                ta = short_tuple[0]
                sa2 = short_tuple[1]
            # Add individual to dictionary of individuals to include in community events for that specific SA2
            if ta not in individuals_dictionary:
                individuals_dictionary[ta] = {}
            if sa2 not in individuals_dictionary[ta]:
                individuals_dictionary[ta][sa2] = {}
            if sex not in individuals_dictionary[ta][sa2]:
                individuals_dictionary[ta][sa2][sex] = {}
            if age not in individuals_dictionary[ta][sa2][sex]:
                individuals_dictionary[ta][sa2][sex][age] = {}
            if ethnicity not in individuals_dictionary[ta][sa2][sex][age]:
                individuals_dictionary[ta][sa2][sex][age][ethnicity] = []
            individuals_dictionary[ta][sa2][sex][age][ethnicity].append(individual)
            ind_participation -= 1
    return individuals_dictionary


def generate_assortative_value(in_dictionary, available_items):
    """ Selects an age if age assortativity is switched on or ethnicity if ethnicity assortativity is switched on

    Args:
      in_dictionary (dict): Dictionary of community events (TA, Sa2, links and the ages and ethnicities of participants)
      available_items (list): List of the available ages or available ethnicities of individuals left to fill events with
    
    Returns: 
      age or ethnicity list
    """
    current_value = 0
    in_item_list = []
    probs_list = []
    # Get probability of each item
    for item in in_dictionary:
        if item not in available_items:
            continue
        current_value += in_dictionary[item]
        probs_list.append(current_value)
        in_item_list.append(item)
    if len(probs_list) == 0:
        return "Error"
    # Choose an item using assortativity
    random_value = random.uniform(0,probs_list[-1])
    for a in range(len(probs_list)):
        if random_value <= probs_list[a]:
            return [in_item_list[a]]
    

def make_events(
    individuals_dictionary,
    maximum_size,
    minimum_size,
    powerlaw_value,
    age_assortativity,
    age_parameters,
    ethnic_assortativity,
    ethnic_assortativity_threshold,
    trial_threshold,
    prefix
    ):
    """ Creates and fills the community events for the community layer

    Args:
      individuals_dictionary (dict): Dictionary of individuals to add to community events
      maximum_size (int): Maximum size of a community event
      minimum_size (int): Minimum size of a community event
      powerlaw_value (double): Parameter for the powerlaw distribution of event sizes
      age_assortativity (bool): Whether to use current event age assortativity when filling events
      age_parameters (matrix): Matrix of age assortativities between age groups
      ethnic_assortativity (bool): Whether to use current ethnic assortativity when filling events
      ethnic_assortativity_threshold (double): Probability of using ethnic assortativity for a specific group
      trial_threshold (int): Number of times to try and fill a space in a community event before skipping
      prefix (str): Group prefix (either EX or EV for close or casual community events)

    Returns: 
      The final dictionary of community events
    """
    # initialising dictionaries and counters
    final_dictionary = {}
    node_index = 1
    ind_count = 0
    size_dict = {
        0:0,
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
        7:0,
        8:0,
        9:0,
        10:0,
        "<100":0
        }
    # For every SA2 in every TA
    for ta in individuals_dictionary:
        for sa2 in individuals_dictionary[ta]:
            # Get available ages of individuals that can fill events
            available_ages = get_available_ages(
                ["F","M"],
                ["0-14","15-29","30-59","60+"],
                individuals_dictionary[ta][sa2]
                )
            # While there are still available individuals
            while len(available_ages) > 0:
                ev_size = 0
                # Get the event size and details and populate dictionary
                while ev_size < minimum_size or ev_size > maximum_size:
                    ev_size = int(list(powerlaw.Power_Law(xmin=minimum_size,xmax=maximum_size,parameters=[powerlaw_value],discrete=True).generate_random(int(1)))[0])
                name = prefix + ta.zfill(2) + str(node_index).zfill(8)
                node_index += 1
                final_dictionary[name] = {
                    "TA":ta,
                    "SA2":sa2,
                    "links":[],
                    "age_dict":{
                        "0-14":0,
                        "15-29":0,
                        "30-59":0,
                        "60+":0
                        },
                    "ethnicity_dict":{
                        "Maori":0,
                        "Pacific":0,
                        "Other":0,
                        "MaoriPacific":0
                        }
                    }
                # While the event is unfilled, look for individuals to fill it (possibly using assortativity)
                while ev_size > 0:
                    if len(available_ages) == 0:
                        break
                    if age_assortativity and len(final_dictionary[name]["links"]) > 0:
                        age_list = generate_assortative_value(
                            final_dictionary[name]["age_dict"],
                            available_ages
                            )
                    else:
                        age_list = deepcopy(available_ages)
                    available_ethnicities = get_available_ethnicities(
                        age_list,
                        individuals_dictionary[ta][sa2]
                        )
                    if ethnic_assortativity and len(final_dictionary[name]["links"]) > 0 and random.uniform(0,1) <= ethnic_assortativity_threshold:
                        ethnicity_list = generate_assortative_value(
                            final_dictionary[name]["ethnicity_dict"],
                            available_ethnicities
                            )
                    else:
                        ethnicity_list = deepcopy(available_ethnicities)
                    if ethnicity_list == "Error":
                        ethnicity_list = deepcopy(available_ethnicities)
                    trials = 0
                    # Try filling until trial threshold is reached
                    while trials < trial_threshold:
                        final_sex,final_age,final_ethnicity,final_id = get_random_individual(
                            ["F","M"],
                            age_list,
                            ethnicity_list,
                            individuals_dictionary[ta][sa2]
                            )
                        if final_id in final_dictionary[name]["links"]:
                            trials += 1
                            continue
                        final_dictionary[name]["links"].append(final_id)
                        for age in final_dictionary[name]["age_dict"]:
                            try:
                                final_dictionary[name]["age_dict"][age] += age_parameters[final_age][age]
                            except:
                                print(name)
                                print(final_dictionary[name])
                                print(final_age)
                                print(age_parameters)
                                print(age_parameters[final_age])
                                quit()
                        final_dictionary[name]["ethnicity_dict"][final_ethnicity] += 1
                        individuals_dictionary[ta][sa2][final_sex][final_age][final_ethnicity].remove(final_id)
                        ev_size -= 1
                        break
                    available_ages = get_available_ages(
                        ["F","M"],
                        available_ages,
                        individuals_dictionary[ta][sa2]
                        )
                    if trials == trial_threshold or ev_size == 0:
                        break
    return final_dictionary

def make_final_files(output_path, final_dictionary, readme, prefix):
    """ Converts the final dictionary into a form to be merged in and saves it to files

    Args: 
      output_path (str): Path to output directory
      final_dictionary (dict): Dictionary of created community events
      readme (str): String to turn into readme for the community layer
      prefix (str): Group prefix (either EX or EV for close or casual community events)
    """
    if output_path[-1] != "/":
        output_path += "/"
    resultant_dict = {
        "nodes":{},
        "readme":readme
        }
    # Copy nested dictionary structure to new node dictionary, update readme and save layer
    for item in final_dictionary:
        resultant_dict["nodes"][item] = deepcopy(final_dictionary[item])
    id_str = id_generator()
    readme = "COMMUNITY " + prefix + " LAYER:\nUNIQUE PREFIX:\t" + id_str + "\n\n" + readme
    resultant_dict["readme"] = readme
    readme = extract_final_metrics(resultant_dict,readme)
    make_pickle(resultant_dict,output_path,id_str,prefix.lower())
    make_readme(readme,output_path,id_str,prefix.lower())
                        




    
