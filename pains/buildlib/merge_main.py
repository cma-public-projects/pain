from .merge_functions import create_custom_layer, link_group_nodes, make_final_files, read_in_individuals, read_in_layer

def main(
    labels,
    individuals_file,
    output_directory,
    layers_to_include,
    read_dw,
    dw_layer,
    read_wp,
    wp_layer,
    read_sc,
    sc_layer,
    read_ex,
    ex_layer,
    read_ev,
    ev_layer,
    exclude_ta,
    exclude_sa2,
    custom_layers,
    custom_parameters,
    link_groups,
    link_group_edgelists,
    individual_attributes
    ):
    """ Calls the merge functions to merge the layers together into a final network dictionary

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individuals_file (str): Path to the individuals nodelist csv
      output_directory (str): Path to the output directory     
      layers_to_include (list): a list of the layers to merge together 
      read_dw (bool): Whether to read in an existing dwelling layer or use current_dw.pkl
      dw_layer (str): Path to the existing dwelling layer if desired
      read_wp (bool): Whether to read in an existing workplace layer or use current_wp.pkl
      wp_layer (str): Path to the existing workplace layer if desired
      read_sc (bool): Whether to read in an existing school layer or use current_sc.pkl
      sc_layer (str): Path to the existing school layer if desired
      read_ex (bool): Whether to read in an existing ex layer or use current_ex.pkl
      ex_layer (str): Path to the existing ex layer if desired
      read_ev (bool): Whether to read in an existing ev layer or use current_ev.pkl
      ev_layer (str): Path to the existing ev layer if desired
      exclude_ta (list): List of SA2s to exclude from the network
      exclude_sa (list): List of TAs to exclude from the network
      custom_layers (bool): Whether to merge in custom layers 
      custom_parameters (dict): Nested dictionary, outer dictionary containing layers to add, inner dictionaries have node/edge lists for the layer
      link_groups (bool): Whether to link group nodes with edges
      link_group_edgelists (str): Path to a csv containing edgelists between group nodes
      individual_attributes (dict): Dictionary of mappings of individual attributes to include in the merge individuals layer/nodelist
    """
    # Setting up new readme
    final_dictionary = {
        "readme":"",
        "individual nodes":{},
        "group nodes":{}
        }
    # Setting temp current files
    if output_directory[-1] != "/":
        output_directory += "/"
    dw_file = output_directory + "current_dw.pkl"
    sc_file = output_directory + "current_sc.pkl"
    wp_file = output_directory + "current_wp.pkl"
    ex_file = output_directory + "current_ex.pkl"
    ev_file = output_directory + "current_ev.pkl"
    # If other layers required
    if read_dw:
       dw_file = dw_layer
    if read_sc:
       sc_file = sc_layer
    if read_wp:
       wp_file = wp_layer
    if read_ex:
       ex_file = ex_layer
    if read_ev:
       ev_file = ev_layer


    #empty list to put the layers to include into
    layers = []
    for layer_code in layers_to_include:
        if layer_code == "DW":
            layers.append(dw_file)
        elif layer_code == "WP":
            layers.append(wp_file)
        elif layer_code == "SC":
            layers.append(sc_file)
        elif layer_code == "EX":
            layers.append(ex_file)
        elif layer_code == "EV":
            layers.append(ev_file)

    # print(layers)


    # layers = [dw_file,sc_file,wp_file,ex_file,ev_file]
    # layers = [dw_file] ## hardcoding for JASSS article - needs to be automated and specified in the yaml

    # Read in layers
    for layer in layers:
        final_dictionary = read_in_layer(
            layer,
            final_dictionary
            )
    # If custom layers to be merged then read in and merge
    if custom_layers:
        readme_str = "CUSTOM LAYER PARAMETERS:\n\n"
        for item in custom_parameters:
            if 'nodelist' in custom_parameters[item]:
                readme_str += "NODELIST:\t" + custom_parameters[item]['nodelist'] + "\nEDGELIST:\t" + custom_parameters[item]['edgelist'] + "\n\n"
            else:
                readme_str += "EDGELIST:\t" + custom_parameters[item]['edgelist'] + "\n\n"
            final_dictionary = create_custom_layer(
                labels['custom_layers'],
                final_dictionary,
                custom_parameters[item]
                )
    # If edges between group nodes are desired
    if link_groups:
        for edgelist in link_group_edgelists:
            final_dictionary = link_group_nodes(
                labels["link_group_nodes"],
                final_dictionary,
                edgelist,
                )
    # Add individuals dictionary to network and make final network files
    final_dictionary = read_in_individuals(
        individuals_file,
        exclude_ta,
        exclude_sa2,
        final_dictionary,
        individual_attributes
        )
    make_final_files(
       final_dictionary,
       output_directory
       )

