def main(
    special_selection,
    excluded_sa2,
    excluded_ta
    ):
    """ Adds/removes TAs and SA2s from excluded list as required

    Args:
      special_selection (dict): Dictionary outlining whether network is restricted to certain areas/TAs/SA2s/etc.
      excluded_sa2 (list): Initial list of excluded SA2s
      excluded_ta (list): Initial list of excluded TAs
    """
    true_count = 0
    north_island = [1,2,3,11,12,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,76]
    for item in special_selection:
        if item == "custom_variables":
            continue
        if special_selection[item]:
            true_count += 1
        if true_count > 1:
            print("More than one option specified as True in SPECIAL SELECTION\nQUITTING")
            quit()
    if true_count == 0:
        return excluded_sa2,excluded_ta
    for a in ([i for i in range(77)] + [99,999]):
        if a not in excluded_ta:
            excluded_ta.append(a)
    if special_selection["auckland"]:
        excluded_ta.remove(76)
    if special_selection["wellington"]:
        excluded_ta.remove(47)
    if special_selection["christchurch"]:
        excluded_ta.remove(60)
    if special_selection["north_island"]:
        excluded_ta = [x for x in excluded_ta if x not in north_island]
    if special_selection["south_island"]:
        excluded_ta = excluded_ta + [x for x in north_island if x not in excluded_ta]
    if special_selection["custom"]:
        excluded_ta = [x for x in excluded_ta if x not in special_selection["custom_variables"]["selected_ta"]]
        if len(special_selection["custom_variables"]["selected_sa2"]) == 0:
            return excluded_sa2,excluded_ta
        else:
            print("Select SA2 not implemented\nQUITTING")
            quit()




