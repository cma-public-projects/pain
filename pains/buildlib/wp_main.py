from .universal_functions import get_git_revision_short_hash, get_git_remote, get_git_status, generate_commute_dictionary
from .wp_functions import fill_workplaces, generate_individuals_dict, generate_sector_dict, generate_workplace_dict, get_metrics, make_final_files

# This is the main executable for the generation of the workplace file
def main(
    labels,
    individuals_file,
    output_directory,
    sector_file,
    suppressed_probs,
    commute_file,
    work_nodes,
    excluded_sa2s,
    excluded_tas,
    trials
    ):
    """ Calls the dwelling functions to generate the dwelling layer

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individuals_node_list (str): Path to the individuals nodelist csv
      output_directory (str): Path to the output directory      
      sector_file (str): Path to the sector file
      suppressed_probs (dict): Dictionary of sector probabilities to fill suppressed sector values with
      commute_file (str): Path to the commute file
      work_nodes (str): Path to the workplace nodelist csv
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network
      trials (dict): Dictionary giving maximum number of trials each 'Step' should take when trying to find a worker for a workplace
    """
    # READ FILES AND CONVERT THEM INTO DICTIONARIES
    readme = "WP YAML AND INPUT\n" +\
        "\nGIT HASH:" + get_git_revision_short_hash() +\
        "\nGIT REMOTE:" + get_git_remote() +\
        "\nGIT STATUS:" + get_git_status() +\
        "\nGENERIC\n\n" +\
        "individuals_file: " + str(individuals_file) + "\n" + \
        "output_directory: " + str(output_directory) + "\n" +\
        "excluded_sa2s: " + str(excluded_sa2s) + "\n" +\
        "excluded_tas: " + str(excluded_tas) + "\n\n" +\
        "wp_parameters\n\n" +\
        "wp_file: " + str(work_nodes) + "\n" +\
        "sector_file: " + str(sector_file) + "\n" +\
        "sector_suppressed_probs:\n" +\
        "  1: " + str(suppressed_probs[1]) + "\n" +\
        "  2: " + str(suppressed_probs[2]) + "\n" +\
        "  3: " + str(suppressed_probs[3]) + "\n" +\
        "  4: " + str(suppressed_probs[4]) + "\n" +\
        "  5: " + str(suppressed_probs[5]) + "\n" +\
        "commute_file: " + str(commute_file) + "\n" +\
        "maximum_trials:\n" +\
        "  Step_1: " + str(trials["Step_1"]) + "\n" +\
        "  Step_2: " + str(trials["Step_2"]) + "\n" +\
        "  Step_3: " + str(trials["Step_3"]) + "\n" +\
        "  Step_4: " + str(trials["Step_4"]) + "\n\n"
    # Generate individuals, workplace, sector and commute dictionaries
    ta_dictionary, individuals_dictionary = generate_individuals_dict(
        labels["ih_labels"],
        individuals_file,
        excluded_tas,
        excluded_sa2s
        )
    workplace_dictionary = generate_workplace_dict(
        labels["wp_labels"],
        work_nodes,
        excluded_tas,
        excluded_sa2s
        )
    sector_dictionary = generate_sector_dict(
        labels["sector_labels"],
        sector_file,
        suppressed_probs,
        excluded_tas
        )
    commute_dictionary = generate_commute_dictionary(
        labels["commute_labels"],
        commute_file,
        excluded_sa2s,
        excluded_tas
        )
    final_dictionary = {}
    readme = get_metrics(readme,final_dictionary,individuals_dictionary,workplace_dictionary,0)
    # Step 1
    final_dictionary,workplace_dictionary,individuals_dictionary,commute_dictionary = fill_workplaces(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        workplace_dictionary,
        ta_dictionary,
        sector_dictionary,
        "SA2",
        True,
        trials["Step_1"]
        )
    readme = get_metrics(readme,final_dictionary,individuals_dictionary,workplace_dictionary,1)
    # Step 2
    final_dictionary,workplace_dictionary,individuals_dictionary,commute_dictionary = fill_workplaces(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        workplace_dictionary,
        ta_dictionary,
        sector_dictionary,
        "TA",
        True,
        trials["Step_2"]
        )
    readme = get_metrics(readme,final_dictionary,individuals_dictionary,workplace_dictionary,2)
    # Step 3 
    final_dictionary,workplace_dictionary,individuals_dictionary,commute_dictionary = fill_workplaces(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        workplace_dictionary,
        ta_dictionary,
        sector_dictionary,
        "SA2",
        False,
        trials["Step_3"]
        )
    readme = get_metrics(readme,final_dictionary,individuals_dictionary,workplace_dictionary,3)
    # Step 4
    final_dictionary,workplace_dictionary,individuals_dictionary,commute_dictionary = fill_workplaces(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        workplace_dictionary,
        ta_dictionary,
        sector_dictionary,
        "TA",
        False,
        trials["Step_4"]
        )
    readme = get_metrics(readme,final_dictionary,individuals_dictionary,workplace_dictionary,4)
    # Step 5
    make_final_files(
        output_directory,
        final_dictionary,
        workplace_dictionary,
        readme
        )
