from .dw_functions import cleanup_dictionary_for_filling, clone_dwellings, create_solo_dwellings, fill_dwellings, generate_individuals_dict, generate_large_dwellings_dict, generate_small_dwellings_dict, make_final_files, merge_individuals_dict, update_readme
from .universal_functions import get_git_revision_short_hash, get_git_remote, get_git_status

def main(
    labels,
    individual_nodes,
    output_path,
    ethnic_assortativity,
    ethnic_assortativity_weight,
    small_dwelling_nodes,
    large_dwelling_nodes,
    census_matching,
    census_file,
    restrict_age,
    clone_threshold,
    excluded_sa2s,
    excluded_tas
    ):
    """ Calls the dwelling functions to generate the dwelling layer

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individuals_node_list (str): Path to the individuals nodelist csv
      output_path (str): Path to the output directory      
      ethnic_assortativity (bool): Whether to use current dwelling ethnic assortativity when filling dwellings
      ethnic_assortativity_weight (double): Probability of using ethnic assortativity for a specific dwelling
      small_dwelling_nodes (str): Path to the small dwelling nodelist csv
      large_dwelling_nodes (str): Path to the large dwelling nodelist csv
      census_matching (bool): Whether to try match the census dwelling size distribution
      census_file (str): Path to the census dwelling size distribution file
      restrict_age (bool): Whether to restrict the first individual in a dwelling to be a non-minor
      clone_threshold (int): Number of times to attempt to find a suitable dwelling to clone for un-housed individuals to populate
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network
    """
    # READ FILES AND CONVERT THEM INTO DICTIONARIES
    readme = "DW YAML AND INPUT\n" +\
        "\nGIT HASH:" + get_git_revision_short_hash() +\
        "\nGIT REMOTE:" + get_git_remote() +\
        "\nGIT STATUS:" + get_git_status() +\
        "\nGENERIC\n\n" +\
        "individuals_file: " + str(individual_nodes) + "\n" + \
        "output_directory: " + str(output_path) + "\n" +\
        "excluded_sa2s: " + str(excluded_sa2s) + "\n" +\
        "excluded_tas: " + str(excluded_tas) + "\n\n" +\
        "dw_parameters\n\n" +\
        "ethnic_assortativity: " + str(ethnic_assortativity) + "\n" +\
        "ethnic_assortativity_weight: " + str(ethnic_assortativity_weight) + "\n" +\
        "small_dw_file: " + str(small_dwelling_nodes) + "\n" +\
        "large_dw_file: " + str(large_dwelling_nodes) + "\n" +\
        "census_matching: " + str(census_matching) + "\n" +\
        "census_file: " + str(census_file) + "\n" +\
        "age_restriction: " + str(restrict_age) + "\n\nINITIALISATION\n"
    # Generate individuals dictionary, small dwellings dictionary and large dwellings dictionary
    individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling = generate_individuals_dict(
        labels["ih_labels"],
        individual_nodes,
        excluded_sa2s,
        excluded_tas
        )
    small_dwellings_dict = generate_small_dwellings_dict(
        labels["dw_labels"]["small_dw"],
        small_dwelling_nodes,
        excluded_sa2s,
        excluded_tas
        )
    large_dwellings_dict = generate_large_dwellings_dict(
        labels["dw_labels"]["large_dw"],
        large_dwelling_nodes,
        excluded_sa2s,
        excluded_tas
        )
    final_dictionary = {}
    # STEP 1
    small_error_dict = {} 
    small_dwellings_dict,small_error_dict = cleanup_dictionary_for_filling(
        individuals_dict_small_dwelling,
        small_dwellings_dict,
        small_error_dict
        )
    # STEP 2
    large_error_dict = {}
    large_dwellings_dict,large_error_dict = cleanup_dictionary_for_filling(
        individuals_dict_large_dwelling,
        large_dwellings_dict,
        large_error_dict
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 3\n"
    # STEP 3
    final_dictionary,small_dwellings_dict,individuals_dict_small_dwelling = fill_dwellings(
        final_dictionary,
        small_dwellings_dict,
        individuals_dict_small_dwelling,
        ethnic_assortativity,
        ethnic_assortativity_weight,
        restrict_age,
        census_matching,
        census_file,
        ["Baseline","Step1","Step2","Step3","Step4"]
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 4\n"
    # STEP 4
    final_dictionary,large_dwellings_dict,individuals_dict_large_dwelling = fill_dwellings(
        final_dictionary,
        large_dwellings_dict,
        individuals_dict_large_dwelling,
        False,
        ethnic_assortativity_weight,
        restrict_age,
        False,
        census_file,
        ["Large"]
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 5.1\n"
    # STEP 5
    final_dictionary,small_error_dict,individuals_dict_small_dwelling = fill_dwellings(
        final_dictionary,
        small_error_dict,
        individuals_dict_small_dwelling,
        ethnic_assortativity,
        ethnic_assortativity_weight,
        restrict_age,
        census_matching,
        census_file,
        ["Baseline","Step1","Step2","Step3","Step4"]
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 5.2\n"
    individuals_dict_no_dwelling = merge_individuals_dict(
        individuals_dict_small_dwelling,
        individuals_dict_no_dwelling
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 5.3\n"
    final_dictionary,small_error_dict,individuals_dict_no_dwelling = fill_dwellings(
        final_dictionary,
        small_error_dict,
        individuals_dict_no_dwelling,
        ethnic_assortativity,
        ethnic_assortativity_weight,
        restrict_age,
        census_matching,
        census_file,
        ["Baseline","Step1","Step2","Step3","Step4"]
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 6.1\n"
    # STEP 6
    final_dictionary,large_error_dict,individuals_dict_large_dwelling = fill_dwellings(
        final_dictionary,
        large_error_dict,
        individuals_dict_large_dwelling,
        False,
        ethnic_assortativity_weight,
        restrict_age,
        False,
        census_file,
        ["Large"]
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 6.2\n"
    individuals_dict_no_dwelling = merge_individuals_dict(
        individuals_dict_large_dwelling,
        individuals_dict_no_dwelling
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 6.3\n"
    final_dictionary,large_error_dict,individuals_dict_no_dwelling = fill_dwellings(
        final_dictionary,
        large_error_dict,
        individuals_dict_no_dwelling,
        False,
        ethnic_assortativity_weight,
        restrict_age,
        False,
        census_file,
        ["Large"]
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 7\n"
    # STEP 7
    final_dictionary,individuals_dict_no_dwelling = clone_dwellings(
        final_dictionary,
        individuals_dict_no_dwelling,
        ethnic_assortativity,
        ethnic_assortativity_weight,
        restrict_age,
        census_matching,
        census_file,
        clone_threshold
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    readme += "STEP 8\n"
    # STEP 8
    final_dictionary,individuals_dict_no_dwelling = create_solo_dwellings(
        final_dictionary,
        individuals_dict_no_dwelling
        )
    readme = update_readme(readme,final_dictionary,small_dwellings_dict,large_dwellings_dict,small_error_dict,large_error_dict,individuals_dict_small_dwelling, individuals_dict_large_dwelling, individuals_dict_no_dwelling)
    # STEP 9
    make_final_files(
        output_path,
        final_dictionary,
        readme
        )