import random
import pickle
import string
import copy
import networkx as nx
import numpy as np
import subprocess
import os
from datetime import date

def get_git_revision_short_hash() -> str:
    """ Returns the 7 character git hash that the network layer was generated on
    """
    return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()

def get_git_status() -> str:
    """ Returns a string (with multiple \n) stating the git status (including the branch and any working changes) that the network layer was generated on
    """
    return subprocess.check_output(['git', 'status']).decode('ascii')

def get_git_remote() -> str:
    """ Returns the url for the git remote that the network layer was generated on
    """
    return subprocess.check_output(['git','remote','get-url', subprocess.check_output(['git','remote','show']).decode('ascii').strip()]).decode('ascii').strip()


def make_directory(path):
    """ Checks if a directory exists and creates one

    Args:
      path (str): Path to directory to check
    """
    if not(os.path.exists(path)):
        os.makedirs(path)

def check_path(path):
    """ Checks whether a path string ends in a '/' and updates

    Args:
      path (str): Path to directory

    Returns:
      Updated path if required
    """
    if path[-1] != "/":
        path += "/"
    return path

def id_generator(size=5, chars=string.ascii_uppercase + string.digits):
        """ Creates the prefix for a given layer

        Args: 
          size (int): Number of characters in the layer prefix
          chars (string): String of possible characters to use in the prefix

        Returns: 
          A string of random characters from chars of length size to use as a prefix
        """
        return str(date.today().strftime("%Y_%m_%d")) + "_" + ''.join(random.choice(chars) for _ in range(size))

def gen_indices_dict(l_0,labels_dict):
    """ Generates an indices dictionary for each of the layer building scripts

    Args: 
      l_0 (list): List of column headings from a csv
      labels_dict (dict): Dictionary of column headings to code string labels

    Returns:
      Dictionary mapping code string labels to columns in the csv
    """
    return_dict = {}
    for item in labels_dict:
        if labels_dict[item] in l_0:
            return_dict[item] = l_0.index(labels_dict[item])
    return return_dict

def generate_commute_dictionary(
    labels,
    commute_file,
    excluded_sa2s,
    excluded_tas
    ):
    """ Reads a commute file and converts it into a dictionary

    Args: 
      labels (dict): A dictionary mapping the columns in the commute csv to the resultant node attributes in the network
      commute_file (str): Path to the commute file of interest
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network
    
    Returns:
      The commute dictionary formed from the file
    """
    # Read in commute file
    rf = open(commute_file,'r')
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace('\n','').split(',')
    indices_dict = gen_indices_dict(index_split,labels)
    commute_dictionary = {"SA2":{},"TA":{}}
    # For all origin (workplace) SA2s in the commute file
    for a in range(1,len(rline)):
        line_split = rline[a].split(',')
        if line_split[indices_dict["origin"]] == "NA":
            continue
        if int(line_split[indices_dict["destination"]]) in excluded_sa2s:
            continue
        # if origin id is a TA and is in excluded_TAs or id =99
        if line_split[indices_dict["origin"]][:2] == "TA" and (int(line_split[indices_dict["origin"]].replace("TA",'')) in excluded_tas or int(line_split[indices_dict["origin"]].replace("TA",'')) == 99):
            continue
        # if origin id is an sa2 and is in excluded_sa2s
        if line_split[indices_dict["origin"]][:2] != "TA" and int(line_split[indices_dict["origin"]]) in excluded_sa2s:
            continue
        if line_split[indices_dict["origin"]][:2] == "TA":
            index_1 = "TA"
            index_2 = str(int(line_split[indices_dict["origin"]].replace("TA",'')))
        else:
            index_1 = "SA2"
            index_2 = line_split[indices_dict["origin"]]
        if index_2 not in commute_dictionary[index_1]:
            commute_dictionary[index_1][index_2] = {}
        # Get SA2 to SA2 dictionary value of commuting proportion
        commute_dictionary[index_1][index_2][line_split[indices_dict["destination"]]] = float(line_split[indices_dict["proportion"]])
    return commute_dictionary

def get_commute_sa2(
    commute_dictionary,
    commute_index,
    in_sa2,
    exclude_sa2s
    ):
    """ Iterates over the commute dictionary and returns an SA2 if there are people in it
    
    Args:
      commute_dictionary (dict): Commute dictionary to search in
      commute_index (str): Statistical level to get commute data for (i.e. TA or SA2)
      in_sa2 (str): SA2 that the workplace to fill is in
      exclude_sa2s (list): List of SA2s to exclude from the network

    Returns:
      SA2 if there are commuting individuals in it
    """
    if in_sa2 not in commute_dictionary[commute_index]:
        return "ERROR"
    sa2_list = []
    sa2_prob = []
    current_value = 0
    # Run through SA2s that commute to the workplace
    for sa2 in commute_dictionary[commute_index][in_sa2]:
        if sa2 in exclude_sa2s:
            continue
        # Update list and probabilities of the commuting individual per SA2
        current_value += commute_dictionary[commute_index][in_sa2][sa2]
        sa2_list.append(sa2)
        sa2_prob.append(current_value)
    if len(sa2_prob) == 0:
        return "ERROR"
    # Choose the commute SA2
    random_value = random.uniform(0,sa2_prob[-1])
    for a in range(len(sa2_prob)):
        if random_value <= sa2_prob[a]:
            return sa2_list[a]

def update_dictionaries(
    commute_dictionary,
    ta_dictionary,
    commute_sa2
    ):
    """ Updates a commute dictionary, removing the dictionary record associated with the specified destination id (`commute_sa2`)

    Args:
      commute_dictionary (dict): Commute dictionary to update
      ta_dictionary (dict): Dictionary of individuals SA2s to their commute TAs
      commute_sa2 (str): SA2 individual is commuting from

    Returns:
      The updated commute and TA dictionaries with the SA2s removed

    Notes:
      This function is called when the commute_sa2 input is found to no longer be a valid 'destination' to consider 
      when selecting individuals from 'destinations' to connect to 'origin' group nodes
    """
    commute_keys = commute_dictionary.keys()
    # For all geographic levels of commute_dictionary (ie. TA or SA2)
    for commute_index in commute_keys:
        # Get the IDs of the TAs/SA2's of all the origins (workplaces/schools) in this network
        origin_keys = commute_dictionary[commute_index].keys()
        # For each TA/SA2 that has an origin (workplace/school) in the network, 
        # if the commute_sa2 is listed as a possible destination (employee residence/student residence) for this origin TA/SA2, 
        # remove it from the list, so that it can't again be selected as an SA2 to select a worker/student from.
        for origin in origin_keys:
            if commute_sa2 in commute_dictionary[commute_index][origin]:
                del commute_dictionary[commute_index][origin][commute_sa2]
        if len(commute_dictionary[commute_index][origin]) == 0:
            del commute_dictionary[commute_index][origin]
    if commute_sa2 in ta_dictionary:
        del ta_dictionary[commute_sa2]
    return commute_dictionary, ta_dictionary

def get_random_individual(
    available_sexes,
    available_ages,
    available_ethnicities,
    individuals_dict
    ):
    """ Chooses a random individual from a dictionary based off of specified sexes, ages and ethnicities

    Args:
      available_sexes (list): List of available sexes that a random individual could have
      available_ages (list): List of available ages that a random individual could have
      available_ethnicities (list): List of available ethnicities that a random individual could have
      individuals_dict (dict): Dictionary of individuals and their attributes to choose from

    Returns:
      The chosen individuals' age, sex, ethnicity and ID
    """
    current_val = 0
    tuples = []
    counts = []
    # Append counts of each possible sex, age, ethnicity to turn into a probability
    for sex in available_sexes:
        if sex not in individuals_dict:
            continue
        for age in available_ages:
            if age not in individuals_dict[sex]:
                continue
            for ethnicity in available_ethnicities:
                if ethnicity not in individuals_dict[sex][age]:
                    continue
                current_val += len(individuals_dict[sex][age][ethnicity])
                tuples.append((sex,age,ethnicity))
                counts.append(current_val)
    # Select an individual at random based on counts
    random_val = random.randint(1,counts[-1])
    for a in range(len(counts)):
        if random_val <= counts[a]:
            return tuples[a][0],tuples[a][1],tuples[a][2],random.choice(individuals_dict[tuples[a][0]][tuples[a][1]][tuples[a][2]])

def get_available_ages(
    sex_list,
    age_list,
    in_individuals_dict
    ):
    """ Checks which ages from an array are available in the individuals dictionary

    Args:
      sex_list (list): List of sexes to assess age availability for
      age_list (list): List of ages to assess age availability for
      in_individuals (dict): Dictionary of individuals and their attributes

    Returns:
      List of available ages
    """
    # Initialise return list
    return_ages = []
    # Run through all ages
    for age in age_list:
        # Run through sexes
        for sex in sex_list:
            if sex not in in_individuals_dict:
                continue
            # Append age if it is available for a sex in the sex_list
            if age in in_individuals_dict[sex] and age not in return_ages:
                for ethnicity in in_individuals_dict[sex][age]:
                    if len(in_individuals_dict[sex][age][ethnicity]) > 0:
                        return_ages.append(age)
                        break
    return return_ages

def get_available_ethnicities(
    age_list,
    in_individuals_dict
    ):
    """ Checks which ethnicities are available in the individuals dictionary from an array of ages

    Args:
      age_list (list): List of ages 
      in_individuals_dict (dict): Dictionary of individuals and their attributes

    Returns:
      List of ethnicities available for the desired list of ages
    """
    # Initialise return list
    return_ethnicities = []
    # Run through all ages
    for age in age_list:
        if len(return_ethnicities) == 4:
            return return_ethnicities
        # Run through all individuals and append ethnicity if available
        for sex in in_individuals_dict:
            if age in in_individuals_dict[sex]:
                for ethnicity in in_individuals_dict[sex][age]:
                    if ethnicity not in return_ethnicities:
                        if len(in_individuals_dict[sex][age][ethnicity]) > 0:
                            return_ethnicities.append(ethnicity)
    return return_ethnicities

def extract_final_metrics(final_dictionary, readme):
    """ Extracts metrics for the final network

    Args:
      final_dictionary (dict): Dictionary containing information about the final network
      readme (str): The readme string for the network

    Returns:
      String containing the metrics
    """
    # Initialise outputs and readme
    type_dict = {}
    out_str = "FINAL DICTIONARY METRICS:\n\n"
    # For all nodes in the dictionary
    for node in final_dictionary["nodes"]:
        # Initialise type dictionary for node types
        if node[:2] not in type_dict:
            type_dict[node[:2]] = {
                "sizes":[],
                "total":0,
                "count":0,
                "s1":0
                }
        # Add information about node types (size, links, number)
        type_dict[node[:2]]["count"] += 1
        size = len(final_dictionary["nodes"][node]["links"])
        type_dict[node[:2]]["sizes"].append(size)
        type_dict[node[:2]]["total"] += size
        if size == 1:
            type_dict[node[:2]]["s1"] += 1
    # Add information to readme string
    for nt in type_dict:
        out_str += nt + " NODES\n\n"
        out_str += "TOTAL " + nt + " NODES:\t" + str(type_dict[nt]["count"]) + "\n"
        out_str += "TOTAL CONNECTIONS TO " + nt + " NODES:\t" + str(type_dict[nt]["total"]) + "\n\n"
        out_str += "MINIMUM " + nt + " SIZE:\t" + str(min(type_dict[nt]["sizes"])) + "\n"
        out_str += "LQ " + nt + " SIZE:\t" + str(np.percentile(type_dict[nt]["sizes"],25)) + "\n"
        out_str += "MEDIAN " + nt + " SIZE:\t" + str(np.percentile(type_dict[nt]["sizes"],50)) + "\n" 
        out_str += "UQ " + nt + " SIZE:\t" + str(np.percentile(type_dict[nt]["sizes"],75)) + "\n"
        out_str += "MAXUMUM " + nt + " SIZE:\t" + str(max(type_dict[nt]["sizes"])) + "\n\n"
        out_str += "MEAN " + nt + " SIZE:\t" + str(round(sum(type_dict[nt]["sizes"])/len(type_dict[nt]["sizes"]),2)) + "\n"
        out_str += nt + " NODES OF SIZE 1:\t" + str(type_dict[nt]["s1"]) + "\n\n"
    readme += out_str
    return readme


def make_layer_edgelist_nodelist(
    final_dictionary,
    out_path,
    id_str,
    prefix
    ):
    """ Makes a nodelist and edgelist at time of generation

    Args:
      final_dictionary (dict): Dictionary containing information about the layer
      out_path (str): Path to output directory
      id_str (str): ID of network
      prefix (str): Layer string
    """
    # Opening nodelist and edgelist files and writing in headings
    wf1 = open(out_path + id_str + "_" + prefix + "_NODELIST.csv",'w')
    wf2 = open(out_path + id_str + "_" + prefix + "_EDGELIST.csv",'w')
    wf1_headings = []
    wf2.write("INDIVIDUAL_ID,GROUP_ID\n")
    # For nodes in the layer grab the node and edges and write to csvs
    for node in final_dictionary["nodes"]:
        if len(wf1_headings) == 0:
            for item in final_dictionary["nodes"][node]:
                if item != "links":
                    wf1_headings.append(item)
            wf1.write("GROUP_ID")
            for item in wf1_headings:
                wf1.write("," + item)
            wf1.write(",count\n")
        wf1.write(node)
        for item in wf1_headings:
            wf1.write("," + str(final_dictionary["nodes"][node][item]))
        wf1.write("," + str(len(final_dictionary["nodes"][node]["links"])) + "\n")
        for individual in final_dictionary["nodes"][node]["links"]:
            wf2.write(individual + "," + node + "\n")
    wf1.close()
    wf2.close()
               
def get_mean(in_list):
    """ Returns the mean of the in_list as a string
    """
    if len(in_list) == 0:
        return "NA"
    else:
        return str(round((sum(in_list))/float(len(in_list)),2))

def get_min(in_list):
    """ Returns the minimum of the in_list as a string
    """
    return str(min(in_list))

def get_max(in_list):
    """ Returns the maximum of the in_list as a string
    """    
    return str(max(in_list))

def get_lq(in_list):
    """ Returns the mean of the in_list as a string
    """
    return str(np.quantile(in_list,0.25))

def get_med(in_list):
    """ Returns the lower-quartile of the in_list as a string
    """
    return str(np.quantile(in_list,0.5))

def get_uq(in_list):
    """ Returns the upper-quartile of the in_list as a string
    """
    return str(np.quantile(in_list,0.75))

def get_connections(in_list):
    """ Returns the sum of the in_list as a string
    """
    return str(sum(in_list))
 
def get_count(in_list):
    """ Returns the length of the in_list as a string
    """
    return str(len(in_list))

def extract_network_metrics(network):
    """ Extracts metrics from the final constructed network

    Args:
      network (Networkx Graph): Network to extract metrics for

    Returns:
      String of desired metrics
    """
    # Initialise output metrics
    node_types = []
    node_type_dict = {}
    out_str = ""
    ih_dict_1 = {"total":[]} 
    ih_dict_2 = {}
    temp_dict_ih_1 = {"total":0}
    # Getting number of each node type
    for node in network:
        if node[:2] not in node_types:
            node_types.append(node[:2])
            node_type_dict[node[:2]] = []
        node_type_dict[node[:2]].append(len(list(network[node])))
    # Initialising individuals metrics dictionary
    for nt in node_types:
        if nt != "IH":
            ih_dict_1[nt] = []
            ih_dict_2[nt] = []
            temp_dict_ih_1[nt] = 0
    # Run through all individuals and get attributes
    for node in network:
        if node[:2] != "IH":
            continue
        a_nodes = list(network[node])
        current_dict_ih_2 = {}
        current_dict_ih_1 = copy.deepcopy(temp_dict_ih_1)
        for a_node in a_nodes:
            if a_node[:2] not in current_dict_ih_2:
                current_dict_ih_2[a_node[:2]] = 0
            p_count = len(list(network[a_node]))
            current_dict_ih_2[a_node[:2]] += p_count - 1
            current_dict_ih_1[a_node[:2]] += p_count - 1
            current_dict_ih_1["total"] += p_count - 1
        for item in current_dict_ih_1:
            ih_dict_1[item].append(current_dict_ih_1[item])
        for item in current_dict_ih_2:
            ih_dict_2[item].append(current_dict_ih_2[item])
    # Get number of contacts and connections for groups and individuals
    out_str += "NODE CONNECTIONS\n"
    out_str += "NODE TYPE,COUNT,CONNECTIONS,MEAN,MINIMUM,LOWER QUARTILE,MEDIAN,UPPER QUARTILE,MAXIMUM\n"
    for node_type in node_types:
        out_str += node_type + "," + get_count(node_type_dict[node_type]) + "," + get_connections(node_type_dict[node_type]) + "," + get_mean(node_type_dict[node_type]) + "," + get_min(node_type_dict[node_type]) + "," + get_lq(node_type_dict[node_type]) + "," + get_med(node_type_dict[node_type]) + "," + get_uq(node_type_dict[node_type]) + "," + get_uq(node_type_dict[node_type]) + "," + get_max(node_type_dict[node_type]) + "\n"
    out_str += "INDIVIDUAL CONTACTS THROUGH NODES\n"
    out_str += "NODE TYPE,COUNT,CONNECTIONS,MEAN,MINIMUM,LOWER QUARTILE,MEDIAN,UPPER QUARTILE,MAXIMUM\n"
    for node_type in ih_dict_1:
        out_str += node_type + "," + get_count(ih_dict_1[node_type]) + "," + get_connections(ih_dict_1[node_type]) + "," + get_mean(ih_dict_1[node_type]) + "," + get_min(ih_dict_1[node_type]) + "," + get_lq(ih_dict_1[node_type]) + "," + get_med(ih_dict_1[node_type]) + "," + get_uq(ih_dict_1[node_type]) + "," + get_uq(ih_dict_1[node_type]) + "," + get_max(ih_dict_1[node_type]) + "\n"
    out_str += "INDIVIDUAL CONTACTS THROUGH NODES ONLY IF CONNECTION TO THAT NODE TYPE\n"
    out_str += "NODE TYPE,COUNT,CONNECTIONS,MEAN,MINIMUM,LOWER QUARTILE,MEDIAN,UPPER QUARTILE,MAXIMUM\n"
    for node_type in ih_dict_2:
        out_str += node_type + "," + get_count(ih_dict_2[node_type]) + "," + get_connections(ih_dict_2[node_type]) + "," + get_mean(ih_dict_2[node_type]) + "," + get_min(ih_dict_2[node_type]) + "," + get_lq(ih_dict_2[node_type]) + "," + get_med(ih_dict_2[node_type]) + "," + get_uq(ih_dict_2[node_type]) + "," + get_uq(ih_dict_2[node_type]) + "," + get_max(ih_dict_2[node_type]) + "\n"
    return out_str

def csv_to_dict(in_csv):
    """ Turns an input csv into a usable dictionary

    Args:
      in_csv (str): Path to input csv

    Returns:
      Dictionary of the input csv
    """
    # Read in csv
    rf = open(in_csv,'r')
    rline = rf.readlines()
    rf.close
    # Get column headings
    keys = rline[0].replace('\n','').split(',')
    out_dict = {}
    # For each line, get information and add to the dictionary
    for a in range(1,len(rline)):
         splitter = rline[a].replace('\n','').split(',')
         out_dict[splitter[0]] = {}
         for b in range(1,len(splitter)):
             if splitter[b] == "True":
                 out_dict[splitter[0]][keys[b]] = True
             elif splitter[b] == "False":
                 out_dict[splitter[0]][keys[b]] = False
             else:
                 out_dict[splitter[0]][keys[b]] = float(splitter[b])
    return out_dict

def make_edge_list(network, network_name):
    """ Generates an edge_list of individuals to node_types

    Args: 
      network (Networkx Graph): The network object to generate the edgelist of
      network_name (str): Path/String to the final network file
    """
    wf_dw = open(network_name.replace('.gpickle','_EDGELIST_DW.csv'),'w')
    wf_wp = open(network_name.replace('.gpickle','_EDGELIST_WP.csv'),'w')
    wf_sc = open(network_name.replace('.gpickle','_EDGELIST_SC.csv'),'w')
    wf_dw.write("INDIVIDUAL,GROUP NODE\n")
    wf_wp.write("INDIVIDUAL,GROUP NODE\n")
    wf_sc.write("INDIVIDUAL,GROUP NODE\n")
    for node in network:
        if node[:2] != "IH":
            continue
        linked_nodes = list(network[node])
        for l_node in linked_nodes:
            if l_node[0] == "E":
                continue
            elif l_node[:2] == "DW":                    
                wf_dw.write(node + "," + l_node + "\n")
            elif l_node[:2] == "WP":
                wf_wp.write(node + "," + l_node + "\n")    
            elif l_node[:2] == "PM" or l_node[:2] == "SC":
                wf_sc.write(node + "," + l_node + "\n")
    wf_dw.close()
    wf_wp.close()
    wf_sc.close()

def make_nodelist_and_dictionary(network, network_name):
    """ Generates a nodelist for each node type and a pkl of the individuals' attributes

    Args: 
      network (Networkx Graph): The network object to generate the nodelists of
      network_name (str): Path/String to the final network file
    """
    # Open output csvs and initialise output metrics
    wf_dw = open(network_name.replace('.gpickle','_NODELIST_DW.csv'),'w')
    wf_wp = open(network_name.replace('.gpickle','_NODELIST_WP.csv'),'w')
    wf_sc = open(network_name.replace('.gpickle','_NODELIST_SCPM.csv'),'w')
    wf_ih = open(network_name.replace('.gpickle','_NODELIST_IH.csv'),'w')
    ih_att = []
    wp_att = []
    sc_att = []
    dw_att = []
    ih_dict = {}
    # Run through all nodes
    for node in network:
        # If an individual grab their attributes and connections
        if node[:2] == "IH":
            if len(ih_att) == 0:
                wf_ih.write("node")
                for item in network.nodes[node]:
                    ih_att.append(item)
                    wf_ih.write("," + item)
                wf_ih.write(",size\n")
            wf_ih.write(node)
            for item in ih_att:
                if item == 'vaccine_dates':
                   wf_ih.write("," + ';'.join(map(str, network.nodes[node][item])))
                else:
                   wf_ih.write("," + str(network.nodes[node][item]))
            size = str(len(list(network[node])))
            wf_ih.write("," + size + "\n")
            ih_dict[node] = copy.deepcopy(network.nodes[node])
            ih_dict[node]["size"] = ih_dict
        # If a dwelling grab its attributes and size
        elif node[:2] == "DW":
            if len(dw_att) == 0:
                wf_dw.write("node")
                for item in network.nodes[node]:
                    dw_att.append(item)
                    wf_dw.write("," + item)
                wf_dw.write(",size\n")
            wf_dw.write(node)
            for item in dw_att:
                wf_dw.write("," + str(network.nodes[node][item]))
            size = str(len(list(network[node])))
            wf_dw.write("," + size + "\n")
        # If a workplace grab its attributes and size
        elif node[:2] == "WP":
            if len(wp_att) == 0:
                wf_wp.write("node")
                for item in network.nodes[node]:
                    wp_att.append(item)
                    wf_wp.write("," + item)
                wf_wp.write(",size\n")
            wf_wp.write(node)
            for item in wp_att:
                wf_wp.write("," + str(network.nodes[node][item]))
            size = str(len(list(network[node])))
            wf_wp.write("," + size + "\n")
        # If an education centre grab its attributes and size
        elif node[:2] == "SC" or node[:2] == "PM":
            if len(sc_att) == 0:
                wf_sc.write("node")
                for item in network.nodes[node]:
                    sc_att.append(item)
                    wf_sc.write("," + item)
                wf_sc.write(",size\n")
            wf_sc.write(node)
            for item in sc_att:
                wf_sc.write("," + str(network.nodes[node][item]))
            size = str(len(list(network[node])))
            wf_sc.write("," + size + "\n")
    wf_sc.close()
    wf_ih.close()
    wf_wp.close()
    wf_dw.close()
    wf = open(network_name.replace('.gpickle','_INDIVIDUAL_ATTRIBUTES.pkl'),'wb')
    pickle.dump(ih_dict,wf)
    wf.close()

def make_pickle(out_dict, out_path, id_str, prefix):
    """ Saves the dictionaries as pickle files

    Args:
      out_dict (dict): Dictionary to save as a pickle
      out_path (str): Path to output directory
      id_str (str): ID string of the network
      prefix (str): Layer prefix
    """
    wf = open(out_path + id_str + "_" + prefix + ".pkl",'wb')
    pickle.dump(out_dict,wf)
    wf.close()
    wf = open(out_path + "current" + "_" + prefix + ".pkl",'wb')
    pickle.dump(out_dict,wf)
    wf.close()

def make_readme(readme, out_path, id_str, prefix):
    """ Writes the readme into human readable files

    Args:
      readme (str): The readme to format
      out_path (str): Path to output directory
      id_str (str): ID string of the network
      prefix (str): Layer prefix
    """
    wf = open(out_path + id_str + "_" + prefix + "_readme.txt",'w')
    wf.write(readme)
    wf.close()
    wf = open(out_path + "current" + "_" + prefix + "_readme.txt",'w')
    wf.write(readme)
    wf.close()
 
