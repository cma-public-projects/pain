# script for taking a network and splitting its large groups into smaller close contact and casual contact subgroups. 
# the first argument is the path to the network. 
# the second argument is the path to the .yaml (construct_close_contacts.yaml) which contains parameters that determine the size and number of the constructed groups. 

import networkx as nx
import csv
import pickle
import pathlib

from .close_contact_functions import close_contactify, save_nodelist_edgelist_csv
from . import universal_functions

def main(read_network, network_file, config, outpath):
    """ Calls the close_contact_functions functions to add close-contact structure to a network

    Args:
      read_network (bool): Whether to read in a network.pkl or use the current_network.pkl
      network_file (str): Path to network.pkl if desired
      config (dict): Config dictionary from the input YAML to generate_pain.py
      outpath (str): Path to output directory
    """
    # Grabbing network information
    if read_network:
        open_file = network_file
    else:
        open_file = outpath + "current_network.pkl"
    in_file = open(open_file,'rb')
    in_dict = pickle.load(in_file)
    in_file.close()
    # Reading in network
    orig_gpickle = in_dict["network_name"].replace('.pkl','.gpickle')
    in_file = open(orig_gpickle,'rb')
    network = pickle.load(in_file)
    in_file.close()
    # Writing readme preamble
    readme = 'CLOSE CONTACTS BEING MADE WITH THE FOLLOWING SETTINGS: \n'
    git_string = "\nGITHASH:" + universal_functions.get_git_revision_short_hash() + "\n"
    readme += git_string
    for key, value in config.items():
        if ((str(key) == "generate") or (str(key)=="output_path") or (str(key)=="save_full_lists")):
            continue
        readme += str(key) + ': \n'
        for subkey, subvalue in value.items():
            readme += '\t' + str(subkey) + ' = ' + str(subvalue) + '\n'
    readme += 'FROM INPUT FILE: ' + str(orig_gpickle) + '\n'
    # Adding close contact structure to network
    readme = close_contactify(network, config, readme)

    # Outputting new 'full' network and readme
    p = pathlib.Path(orig_gpickle).with_suffix('.full.gpickle')
    new_graph_file = str(p)
    with open(new_graph_file, 'wb') as fp:
        pickle.dump(network, fp)
    readme += 'NEW NETWORK WITH CLOSE COVER: ' + new_graph_file + '\n'
    if config['output_path']:
        outpath = config['output_path']
    save_nodelist_edgelist_csv(network, p, outpath, config)
    wf = open(new_graph_file.split('.full.gpickle')[0] + "_full_README.txt",'w')
    wf.write(readme)
    wf.close()

