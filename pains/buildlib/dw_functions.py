import copy
import random
import yaml

from .universal_functions import extract_final_metrics, gen_indices_dict, get_available_ages, get_available_ethnicities, get_random_individual, id_generator, make_layer_edgelist_nodelist, make_pickle, make_readme

def generate_individuals_dict(
    labels,
    individual_nodes,
    excluded_sa2s,
    excluded_tas
    ):
    """ Generates the individuals dictionary for housing individuals

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individual_nodes (str): Path to the individuals nodelist csv
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network

    Returns: 
      Three dictionaries corresponding to the individuals in small, large and no dwellings
    """
    rf = open(individual_nodes,'r')
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n",'').split(',')
    indices_dict = gen_indices_dict(index_split,labels)
    # Generate three dictionaries required
    small_dwelling = {}
    large_dwelling = {}
    no_dwelling = {}
    # For all individuals in the nodelist csv get their attributes and assign them to the correct dwelling size dictionary
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n",'').split(',')
        if int(line_split[indices_dict["TA"]]) in excluded_tas:
            continue
        if int(line_split[indices_dict["SA2"]]) in excluded_sa2s:
            continue
        if line_split[indices_dict["assigned_small_dwelling"]] == "Y" and line_split[indices_dict["assigned_dwelling"]] == "Y":
            current_dict = small_dwelling
            if line_split[indices_dict["TA"]] not in current_dict:
                current_dict[line_split[indices_dict["TA"]]] = {}
            if line_split[indices_dict["SA2"]] not in current_dict[line_split[indices_dict["TA"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]] = {}
            if line_split[indices_dict["sex"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]] = {}
            if line_split[indices_dict["age"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]] = {}
            if line_split[indices_dict["ethnicity"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]] = []
            current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]].append(line_split[indices_dict["node_id"]])
        elif line_split[indices_dict["assigned_small_dwelling"]] == "N" and line_split[indices_dict["assigned_dwelling"]] == "Y":
            current_dict = large_dwelling
            if line_split[indices_dict["TA"]] not in current_dict:
                current_dict[line_split[indices_dict["TA"]]] = {}
            if line_split[indices_dict["SA2"]] not in current_dict[line_split[indices_dict["TA"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]] = {}
            if line_split[indices_dict["sex"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]] = {}
            if line_split[indices_dict["age"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]] = {}
            if line_split[indices_dict["ethnicity"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]] = []
            current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]].append(line_split[indices_dict["node_id"]])
        elif line_split[indices_dict["assigned_small_dwelling"]] == "N" and line_split[indices_dict["assigned_dwelling"]] == "N":
            current_dict = no_dwelling
            if line_split[indices_dict["TA"]] not in current_dict:
                current_dict[line_split[indices_dict["TA"]]] = {}
            if line_split[indices_dict["SA2"]] not in current_dict[line_split[indices_dict["TA"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]] = {}
            if line_split[indices_dict["sex"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]] = {}
            if line_split[indices_dict["age"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]] = {}
            if line_split[indices_dict["ethnicity"]] not in current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]]:
                current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]] = []
            current_dict[line_split[indices_dict["TA"]]][line_split[indices_dict["SA2"]]][line_split[indices_dict["sex"]]][line_split[indices_dict["age"]]][line_split[indices_dict["ethnicity"]]].append(line_split[indices_dict["node_id"]])
        else:
            print("INVALID DWELLING COMBINATIONS\n" + rline[0] + rline[a] + "QUITTING")
            quit()
    return small_dwelling,large_dwelling,no_dwelling

def generate_small_dwellings_dict(
    labels,
    small_dwelling_nodes,
    excluded_sa2s,
    excluded_tas
    ):
    """ Converts the small dwelling node list into a different formatted dictionary for use in the generation of the dwelling layer
    
    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      small_dwelling_nodes (str): Path to the small dwelling nodelist csv
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network  

    Returns: 
      Dictionary of ages required to fill dwellings in each SA2
    """
    # Read in the small dwelling nodelist csv
    rf = open(small_dwelling_nodes,'r')
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace('\n','').split(',')
    indices_dict = gen_indices_dict(index_split,labels)
    small_dwellings_dict = {}
    # For all small dwellings in the nodelist csv
    for a in range(1,len(rline)):
        line_split = rline[a].replace('\n','').split(',')
        ta = str(int(line_split[indices_dict["TA"]]))
        sa2 = line_split[indices_dict["SA2"]]
        if int(ta) in excluded_tas:
            continue
        if int(sa2) in excluded_sa2s:
            continue
        # Pull out dwelling information
        dw_id = line_split[indices_dict["node_id"]].replace('S','') + "_S"
        age_1 = line_split[indices_dict["count_0_14"]]
        age_2 = line_split[indices_dict["count_15_29"]]
        age_3 = line_split[indices_dict["count_30_59"]]
        age_4 = line_split[indices_dict["count_60"]]
        dw_size = line_split[indices_dict["size"]]
        adults = line_split[indices_dict["count_adults"]]
        workers = line_split[indices_dict["count_workers"]]
        step = line_split[indices_dict["step"]]
        # Pull out age information for each step in the filling process
        if step == "Baseline":
            age_bands = [["60+"],["30-59"],["15-29"],["0-14"]]
            age_counts = [int(age_4),int(age_3),int(age_2),int(age_1)]
        if step == "Step1":
            age_bands = [["60+"],["30-59","15-29"],["0-14"]]
            age_counts = [int(age_4),int(workers),int(age_1)]
        if step == "Step2":
            age_bands = [["60+","30-59","15-29"],["0-14"]]
            age_counts = [int(adults),int(age_1)]
        if step == "Step3":
            age_bands = [["60+","30-59","15-29","0-14"]]
            age_counts = [int(dw_size)]
        if step == "Step4":
            age_bands = [["60+","30-59","15-29","0-14"]]
            age_counts = [int(dw_size)]
        if ta not in small_dwellings_dict:
            small_dwellings_dict[ta] = {}
        if sa2 not in small_dwellings_dict[ta]:
            small_dwellings_dict[ta][sa2] = {}
        if step not in small_dwellings_dict[ta][sa2]:
            small_dwellings_dict[ta][sa2][step] = {}
        # Build return dictionary of required ages to fill for each SA2
        small_dwellings_dict[ta][sa2][step][dw_id] = {"age_bands":age_bands,"age_counts":age_counts}
    return small_dwellings_dict

def sum_dictionary(in_dictionary, step):
    """ This function sums over the inputted dictionary in different ways depending on the 'Step' of the dwelling build process

    Args:
      in_dictionary (dict): Dictionary of individuals to sum over
      step (int): Current step of the dwelling build process
    
    Returns: 
      The count desired as a string
    """
    count = 0
    # Sum over all SA2s in all TAs either dwellings, individuals, unfilled dwellings, etc.
    for ta in in_dictionary:
        for sa2 in in_dictionary[ta]:
           if step == 1:
               for a in in_dictionary[ta][sa2]:
                   for b in in_dictionary[ta][sa2][a]:
                       for c in in_dictionary[ta][sa2][a][b]:
                           count += len(in_dictionary[ta][sa2][a][b][c])
           elif step == 2:
               for a in in_dictionary[ta][sa2]:
                   count += len(in_dictionary[ta][sa2][a])
           elif step == 3:
               count += len(in_dictionary[ta][sa2])
           elif step == 4:
               for a in in_dictionary[ta][sa2]:
                   count += len(in_dictionary[ta][sa2][a]["links"])
    return str(count)

def update_readme(
    readme,
    final_dictionary,
    small_dwellings_dict,
    large_dwellings_dict,
    small_error_dict,
    large_error_dict,
    individuals_dict_small_dwelling,
    individuals_dict_large_dwelling,
    individuals_dict_no_dwelling
    ):
    """ Updates the readme with metrics from all of the Dictionaries

    Args:
      readme (str): Unfinished readme for the dwelling layer
      final_dictionary (dict): Final dwelling nodelist dictionary at end of build process
      small_dwellings_dict (dict): Small dwelling vacancies dictionary
      large_dwellings_dict (dict): Large dwelling vacancies dictionary
      individuals_dict_small_dwelling (dict): Dictionary of individuals assigned to small dwellings
      individuals_dict_large_dwelling (dict): Dictionary of individuals assigned to large dwellings
      individuals_dict_no_dwelling (dict): Dictionary of individuals assigned to no dwellings

    Returns:
      The updated readme string
    """
    # Update readme with each count of interest
    readme += "DWELLINGS FILLED:\t" + sum_dictionary(final_dictionary,3) + "\n"
    readme += "PEOPLE IN DWELLINGS:\t" + sum_dictionary(final_dictionary,4) + "\n"
    readme += "UNFILLED SMALL DWELLINGS:\t" + sum_dictionary(small_dwellings_dict,2) + "\n"
    readme += "UNFILLED REMOVED SMALL DWELLINGS:\t" + sum_dictionary(small_error_dict,2) + "\n"
    readme += "UNFILLED LARGE DWELLINGS:\t" + sum_dictionary(large_dwellings_dict,2) + "\n"
    readme += "UNFILLED REMOVED LARGE DWELLINGS:\t" + sum_dictionary(large_error_dict,2) + "\n"
    readme += "INDIVIDUALS ASSIGNED SMALL DWELLING:\t" + sum_dictionary(individuals_dict_small_dwelling,1) + "\n"
    readme += "INDIVIDUALS ASSIGNED LARGE DWELLING:\t" + sum_dictionary(individuals_dict_large_dwelling,1) + "\n"
    readme += "INDIVIDUALS ASSIGNED NO DWELLING:\t" + sum_dictionary(individuals_dict_no_dwelling,1) + "\n\n"
    return readme

# This function 
def generate_large_dwellings_dict(
    labels,
    large_dwelling_nodes,
    excluded_sa2s,
    excluded_tas
    ):
    """ Converts the large dwelling node list into a dictionary for use in the generation of the dwelling layer

    Args: 
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      large_dwelling_nodes (str): Path to the large dwelling nodelist csv
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network

    Returns:
      Dictionary of the large dwellings and their associated information
    """
    # Read in large dwelling nodelist
    rf = open(large_dwelling_nodes,'r')
    rline = rf.readlines()
    rf.close()
    # Get index of column headings needed for this calc
    index_split = rline[0].replace('\n','').split(',')
    indices_dict = gen_indices_dict(index_split,labels)
    large_dwellings_dict = {}
    # For each large dwelling read in its attributes and add to dictionary
    for a in range(1,len(rline)):
        line_split = rline[a].replace('\n','').split(',')
        ta = str(int(line_split[indices_dict["TA"]]))
        sa2 = line_split[indices_dict["SA2"]]
        if int(ta) in excluded_tas:
            continue
        if int(sa2) in excluded_sa2s:
            continue
        dw_size = line_split[indices_dict["size"]]
        dw_id = line_split[indices_dict["node_id"]].replace('L','') + "_L"
        if ta not in large_dwellings_dict:
            large_dwellings_dict[ta] = {}
        if sa2 not in large_dwellings_dict[ta]:
            large_dwellings_dict[ta][sa2] = {"Large":{}}
        large_dwellings_dict[ta][sa2]["Large"][dw_id] = {"age_bands":[["60+","30-59","15-29","0-14"]],"age_counts":[int(dw_size)]}
    return large_dwellings_dict

def get_people_needed(dwelling_dict):
    """ Checks the number of people needed to fill all dwellings in the SA2

    Args:
      dwelling_dict (dict): Dictionary of dwellings within the SA2

    Returns:
      List of of counts for individuals in each age band (and aggregate bands) required to fill the dwellings
    """
    output_list = [0,0,0,0,0,0,0]
    # Run through all dwelling types
    for dw_type in dwelling_dict:
        # Run through all dwellings of that type
        for dw_id in dwelling_dict[dw_type]:
            # Run through each age band and data to total counts
            for a in range(len(dwelling_dict[dw_type][dw_id]["age_bands"])):
                if dwelling_dict[dw_type][dw_id]["age_bands"][a] == ['0-14']:
                    output_list[0] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                elif dwelling_dict[dw_type][dw_id]["age_bands"][a] == ['15-29']:
                    output_list[1] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[4] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[5] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                elif dwelling_dict[dw_type][dw_id]["age_bands"][a] == ['30-59']:
                    output_list[2] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[4] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[5] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                elif dwelling_dict[dw_type][dw_id]["age_bands"][a] == ['60+']:
                    output_list[3] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[5] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                elif dwelling_dict[dw_type][dw_id]["age_bands"][a] == ['30-59','15-29']:
                    output_list[4] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[5] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                elif dwelling_dict[dw_type][dw_id]["age_bands"][a] == ['60+','30-59','15-29']:
                    output_list[5] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                elif dwelling_dict[dw_type][dw_id]["age_bands"][a] == ["60+","30-59","15-29","0-14"]:
                    output_list[6] += dwelling_dict[dw_type][dw_id]["age_counts"][a]
                else:
                    print("INVALID AGE CONFIGURATION",dwelling_dict[dw_type][dw_id]["age_bands"][a]) #Tidy up
                    quit()
    return output_list

def update_people_needed(output_list, dwelling_dict):
    """ Updates the people needed list when a dwelling is moved to an error dict
    Args:
      output_list (list): Output list from get_people_needed or get_people_available type functions
      dwelling_dict (dict): Dictionary of dwellings within the SA2
    
    Returns:
      The updated people needed list
    """
    # For all age bands in the dwelling update counts of total required people
    for a in range(len(dwelling_dict["age_bands"])):
        if dwelling_dict["age_bands"][a] == ['0-14']:
            output_list[0] -= dwelling_dict["age_counts"][a]
            output_list[6] -= dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['15-29']:
            output_list[1] -= dwelling_dict["age_counts"][a]
            output_list[4] -= dwelling_dict["age_counts"][a]
            output_list[5] -= dwelling_dict["age_counts"][a]
            output_list[6] -= dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['30-59']:
            output_list[2] -= dwelling_dict["age_counts"][a]
            output_list[4] -= dwelling_dict["age_counts"][a]
            output_list[5] -= dwelling_dict["age_counts"][a]
            output_list[6] -= dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['60+']:
            output_list[3] -= dwelling_dict["age_counts"][a]
            output_list[5] -= dwelling_dict["age_counts"][a]
            output_list[6] -= dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['30-59','15-29']:
            output_list[4] -= dwelling_dict["age_counts"][a]
            output_list[5] -= dwelling_dict["age_counts"][a]
            output_list[6] -= dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['60+','30-59','15-29']:
            output_list[5] -= dwelling_dict["age_counts"][a]
            output_list[6] -= dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ["60+","30-59","15-29","0-14"]:
            output_list[6] -= dwelling_dict["age_counts"][a]
        else:
            print("INVALID AGE CONFIGURATION",dwelling_dict["age_bands"][a]) #Tidy up
            quit()
    return output_list

def get_people_needed_single_dwelling(dwelling_dict):
    """ Get counts of people needed for a single dwelling

    Args:
      dwelling_dict (dict): Dictionary of individuals needed for a single dwelling

    Returns:
      List of of counts for individuals in each age band (and aggregate bands) required to fill the dwelling
    """
    output_list = [0,0,0,0,0,0,0]
    # Run through each age band and update number needed based on the dwelling dict of the dwelling
    for a in range(len(dwelling_dict["age_bands"])):
        if dwelling_dict["age_bands"][a] == ['0-14']:
            output_list[0] += dwelling_dict["age_counts"][a]
            output_list[6] += dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['15-29']:
            output_list[1] += dwelling_dict["age_counts"][a]
            output_list[4] += dwelling_dict["age_counts"][a]
            output_list[5] += dwelling_dict["age_counts"][a]
            output_list[6] += dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['30-59']:
            output_list[2] += dwelling_dict["age_counts"][a]
            output_list[4] += dwelling_dict["age_counts"][a]
            output_list[5] += dwelling_dict["age_counts"][a]
            output_list[6] += dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['60+']:
            output_list[3] += dwelling_dict["age_counts"][a]
            output_list[5] += dwelling_dict["age_counts"][a]
            output_list[6] += dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['30-59','15-29']:
            output_list[4] += dwelling_dict["age_counts"][a]
            output_list[5] += dwelling_dict["age_counts"][a]
            output_list[6] += dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ['60+','30-59','15-29']:
            output_list[5] += dwelling_dict["age_counts"][a]
            output_list[6] += dwelling_dict["age_counts"][a]
        elif dwelling_dict["age_bands"][a] == ["60+","30-59","15-29","0-14"]:
            output_list[6] += dwelling_dict["age_counts"][a]
        else:
            print("INVALID AGE CONFIGURATION",dwelling_dict["age_bands"][a]) #Tidy up
            quit()
    return output_list

def get_people_available(individuals_dict):
    """ Gets the people available for populating dwellings

    Args:
      individuals_dict (dict): Dictionary of individuals to populate dwellings with

    Returns:
      List of of counts for individuals in each age band (and aggregate bands) available to fill dwellings
    """
    output_list = [0,0,0,0,0,0,0]
    for sex in individuals_dict:
        for age in individuals_dict[sex]:
            if age == '0-14':
                for ethnicity in individuals_dict[sex][age]:
                    count = len(individuals_dict[sex][age][ethnicity])
                    output_list[0] += count
                    output_list[6] += count
            elif age == '15-29':
                for ethnicity in individuals_dict[sex][age]:
                    count = len(individuals_dict[sex][age][ethnicity])
                    output_list[1] += count
                    output_list[4] += count
                    output_list[5] += count
                    output_list[6] += count
            elif age == '30-59':
                for ethnicity in individuals_dict[sex][age]:
                    count = len(individuals_dict[sex][age][ethnicity])
                    output_list[2] += count
                    output_list[4] += count
                    output_list[5] += count
                    output_list[6] += count
            elif age == '60+':
                for ethnicity in individuals_dict[sex][age]:
                    count = len(individuals_dict[sex][age][ethnicity])
                    output_list[3] += count
                    output_list[5] += count
                    output_list[6] += count
            else:
                print("INVALID AGE",age) # Tidy up
                quit()
    return output_list

def check_availability(people_needed, people_available):
    """ Checks to see if the people needed is greater than the people available

    Args:
      people_needed (list): List of individuals required for each age
      people_available (list): List of people available for each age
    
    Returns:
      Boolean for whether or not there are more people needed than available
    """
    for a in range(len(people_needed)):
        if people_needed[a] > people_available[a]:
            return True
    return False

def get_random_dwelling(dwelling_dict, people_needed):
    """ Returns a random dwelling step and dwelling id

    Args:
      dwelling_dict (dict): Dictionary of dwellings to fill
      people_needed (list): List of individuals required for each age

    Returns:
      Random 'Step', random dwelling ID to fill, number of people needed in the dwelling
    """
    current_value = 0
    unweighted_probs = []
    dict_keys = []
    for a in dwelling_dict:
        current_value += len(dwelling_dict[a])
        dict_keys.append(a)
        unweighted_probs.append(current_value)
    random_value = random.randint(1, unweighted_probs[-1])
    for a in range(len(unweighted_probs)):
        if random_value <= unweighted_probs[a]:
            step = dict_keys[a]
            dw_id = random.choice(list(dwelling_dict[step].keys()))
            people_needed = update_people_needed(people_needed, dwelling_dict[step][dw_id])
            return step, dw_id, people_needed

def cleanup_dictionary_for_filling(
    in_individuals_dict,
    dwelling_dict,
    dwelling_error_dict
    ):
    """ Ensures that the dwellings dictionary requires fewer individuals than there are available in the individuals dict

    Args:
      in_individuals_dict (dict): Dictionary of individuals to fill dwellings with
      dwelling_dict (dict): Dictionary of dwellings to fill
      dwelling_error_dict (dict): Empty dictionary to be filled with dwellings to remove

    Returns:
      Updated dictionary of dwellings and dictionary of dwellings that were removed
    """
    del_list_ta = []
    for ta in dwelling_dict:
        del_list_sa2 = []
        for sa2 in dwelling_dict[ta]:
            people_needed = get_people_needed(dwelling_dict[ta][sa2])
            if ta in in_individuals_dict and sa2 in in_individuals_dict[ta]:
                people_available = get_people_available(in_individuals_dict[ta][sa2])
            else:
                people_available = [0,0,0,0,0,0,0]
            while check_availability(people_needed,people_available):
                if ta not in dwelling_error_dict:
                    dwelling_error_dict[ta] = {}
                if sa2 not in dwelling_error_dict[ta]:
                    dwelling_error_dict[ta][sa2] = {}
                dw_step, dw_id, people_needed = get_random_dwelling(dwelling_dict[ta][sa2],people_needed)
                if dw_step not in dwelling_error_dict[ta][sa2]:
                    dwelling_error_dict[ta][sa2][dw_step] = {}
                dwelling_error_dict[ta][sa2][dw_step][dw_id] = dwelling_dict[ta][sa2][dw_step].pop(dw_id)
                if len(dwelling_dict[ta][sa2][dw_step]) == 0:
                    del dwelling_dict[ta][sa2][dw_step]
            if len(dwelling_dict[ta][sa2]) == 0:
                del_list_sa2.append(sa2)
        for sa2 in del_list_sa2:
            del dwelling_dict[ta][sa2]
        if len(dwelling_dict[ta]) == 0:
            del_list_ta.append(ta)
    for ta in del_list_ta:
        del dwelling_dict[ta]
    return dwelling_dict, dwelling_error_dict

def get_census_dictionaries(census_file):
    """ Reads the census file and imports the dictionaries

    Args:
      census_file (str): Path to the census file yaml

    Returns:
      Dictionaries containing the dwelling size distribution by age, and by age and ethnicity
    """
    with open(census_file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data["size_age_counts"],data["size_age_ethnicity_counts"]

def get_census_age(
    available_ages,
    census_age_dict,
    dwelling_size
    ):
    """ Uses dwelling size to determine the age of an individual based off of dwelling size based off of census data

    Args:
      available_ages (list): List of available ages for a dwelling
      census_age_dict (dict): Dictionary  of dwelling size distribution by age
      dwelling_size (int): Size of the dwelling to fill
    
    Returns:
      Age of an individual to add to the dwelling
    """
    key = dwelling_size
    if dwelling_size >= 12:
        key = "12+"
    current_val = 0
    ages_list = []
    ages_vals = []
    for age in available_ages:
        current_val += census_age_dict[key][age]
        ages_list.append(age)
        ages_vals.append(current_val)
    try:
        random_val = random.randint(1,ages_vals[-1])
    except:
        print(key,ages_list,ages_vals)
        quit()
    for a in range(len(ages_vals)):
        if random_val <= ages_vals[a]:
            return ages_list[a]

def get_census_ethnicity(
    census_age,
    census_eth_dict,
    available_ethnicities,
    dwelling_size
    ):
    """ Uses dwelling size and the individuals age to determine an individuals ethnicity based off of census data

    Args:
      census_age (str): Individual's age
      census_eth_dict (dict): Dictionary of dwelling size distribution by ethnicity and age
      available_ethnicities (list): List of available ethnicities to use
      dwelling_size (int): Size of the dwelling to fill

    Returns:
      Ethnicity of an individual to add to the dwelling
    """
    key = dwelling_size
    if dwelling_size >= 12:
        key = "12+"
    current_val = 0
    ethnicities_list = []
    ethnicities_val = []
    for ethnicity in available_ethnicities:
        current_val += census_eth_dict[census_age][key][ethnicity]
        ethnicities_list.append(ethnicity)
        ethnicities_val.append(current_val)
    random_val = random.randint(1,ethnicities_val[-1])
    for a in range(len(ethnicities_val)):
        if random_val <= ethnicities_val[a]:
            return ethnicities_list[a]

def choose_ethnicity(
    available_ethnicities,
    current_ethnicities
    ):
    """ Chooses an ethnicity based off of the current composition of a dwelling

    Args:
      available_ethnicities (list): List of available ethnicities to fill dwelling with
      current_ethnicities (list): List of current ethnicities in the dwelling 
    """
    # Get counts of current numbers of each ethnicity in the dwelling
    counts_dict = {}
    for ethnicity in current_ethnicities:
        if ethnicity in available_ethnicities:
            if ethnicity not in counts_dict:
                counts_dict[ethnicity] = 0
            counts_dict[ethnicity] += 1
    # If no-one is in the dwelling just return the available ethnicities
    if len(counts_dict) == 0:
        return available_ethnicities
    ethnicity_list = []
    ethnicity_counts = []
    current_val = 0
    # Convert from dict to list
    for ethnicity in counts_dict:
        current_val += counts_dict[ethnicity]
        ethnicity_list.append(ethnicity)
        ethnicity_counts.append(current_val)
    # Choose using assortativity of current dwelling ethnicity counts
    random_eth_value = random.randint(1,ethnicity_counts[-1])
    for a in range(len(ethnicity_counts)):
        if random_eth_value <= ethnicity_counts[a]:
            return [ethnicity_list[a]]

def get_individual(
    available_ages,
    current_ethnicities,
    ethnic_assortativity,
    ethnic_assortativity_weight,
    census_matching,
    dwelling_size,
    census_age_dict,
    census_eth_dict,
    in_individuals_dict
    ):
    """ Gets an individual for a dwelling from a specified age and ethnicity list

    Args:
      available_ages (list): List of available ages to select from
      current_ethnicities (list): List of current ethnicities of the dwelling      
      ethnic_assortativity (bool): Whether or not to fill a dwelling using its current ethnic assortativity
      ethnic_assortativity_weight (double): Probability of using ethnic assortativity for any specific dwelling
      census_matching (bool): Whether or not to use the census yaml to probabilistically fill dwellings
      dwelling_size (int): Final size of the dwelling to be filled
      census_age_dict (dict): Age dwelling size data from the census file yaml
      census_eth_dict (dict): Ethnicity dwelling size data from the census file yaml
      in_individuals_dict (dict): Dictionary of individuals to fill-up the dwelling
    
    Returns:
      The sex, age, ethnicity and ID of chosen individual
    """
    random_eth_val = random.uniform(0,1)
    # If using census matching and not assortativity --> get individual
    if census_matching and (len(current_ethnicities) == 0 or (ethnic_assortativity and random_eth_val > ethnic_assortativity_weight) or (not ethnic_assortativity)):
        census_age = get_census_age(
            available_ages,
            census_age_dict,
            dwelling_size,
            )
        available_ethnicities = get_available_ethnicities(
            [census_age],
            in_individuals_dict
            )
        census_ethnicity = get_census_ethnicity(
            census_age,
            census_eth_dict,
            available_ethnicities,
            dwelling_size,
            )
        final_sex,final_age,final_ethnicity,final_id = get_random_individual(
            ['M','F'],
            [census_age],
            [census_ethnicity],
            in_individuals_dict
            )  
        return final_sex,final_age,final_ethnicity,final_id
    # If not using census matching and not assortativity/assortativity check fails --> get individual
    elif not census_matching and ((len(current_ethnicities) == 0 ) or (not ethnic_assortativity) or (ethnic_assortativity and random_eth_val > ethnic_assortativity_weight)):
        available_ethnicities = get_available_ethnicities(
            available_ages,
            in_individuals_dict
            )
        final_sex,final_age,final_ethnicity,final_id = get_random_individual(
            ['M','F'],
            available_ages,
            available_ethnicities,
            in_individuals_dict)
    # If ethnic assortativity check passes --> get individual
    elif (census_matching and ethnic_assortativity and random_eth_val <= ethnic_assortativity_weight and len(current_ethnicities) != 0)  or (not census_matching and ethnic_assortativity and random_eth_val <= ethnic_assortativity_weight and len(current_ethnicities) != 0):
        available_ethnicities = get_available_ethnicities(
            available_ages,
            in_individuals_dict
            )
        chosen_ethnicity = choose_ethnicity(
            available_ethnicities,
            current_ethnicities
            )
        final_sex,final_age,final_ethnicity,final_id = get_random_individual(
            ['M','F'],
            available_ages,
            chosen_ethnicity,
            in_individuals_dict
            )
    else:
        print("INVALID CONFIGURATION FOR SELECTING INDIVIDUALS - QUITTING")
        print(census_matching,ethnic_assortativity,len(current_ethnicities))
        quit()
    return final_sex,final_age,final_ethnicity,final_id

def fill_dwellings(
    final_dictionary,
    dwelling_dict,
    in_individuals_dict,
    ethnic_assortativity,
    ethnic_assortativity_weight,
    age_restriction,
    census_matching,
    census_file,
    steps
    ):
    """ Fills the dwellings in steps 3,4,5,6

    Args:
      final_dictionary (dict): Dictionary for the dwelling layer
      dwelling_dict (dict): Dictionary of dwellings to be filled
      in_individuals_dict (dict): Dictionary of individuals to fill-up the dwellings
      ethnic_assortativity (bool): Whether or not to fill a dwelling using its current ethnic assortativity
      ethnic_assortativity_weight (double): Probability of using ethnic assortativity for any specific dwelling
      age_restriction (bool): Whether or not to be able to use a 0-14 year old as the first individual in the dwelling
      census_matching (bool): Whether or not to use the census yaml to probabilistically fill dwellings
      census_file (str): Path to the census file yaml
      steps (list): Which steps have been completed/need to be completed by the fill_dwellings code (e.g. ['Baseline','Step1',..])
    
    Returns:
      The updated dwelling layer dictionary, unfilled dwelling dictionary and unassigned individuals dictionary
    """
    # Get census data
    census_age_dict,census_eth_dict = get_census_dictionaries(census_file)
    # For all dwellings in this SA2, TA
    for ta in dwelling_dict:
        if ta not in final_dictionary:
            final_dictionary[ta] = {}
        for sa2 in dwelling_dict[ta]:
            if sa2 not in final_dictionary[ta]:
                 final_dictionary[ta][sa2] = {}
            if sa2 not in in_individuals_dict[ta]:
                continue
            # Get people available to assign to dwellings in this SA2
            people_available = get_people_available(in_individuals_dict[ta][sa2])
            # For algorithmic steps
            for step in steps:
                if step in dwelling_dict[ta][sa2]:
                    keys = list(dwelling_dict[ta][sa2][step].keys())
                    # For dwellings to be filled in current step(s)
                    for key in keys:
                        # Check if can be filled and update people needed once dwelling has been filled
                        if check_availability(
                            get_people_needed_single_dwelling(dwelling_dict[ta][sa2][step][key]),
                            people_available
                            ):
                            continue
                        people_available = update_people_needed(
                            people_available,
                            dwelling_dict[ta][sa2][step][key]
                            )
                        final_dictionary[ta][sa2][key] = {
                            "links":[],
                            "ages":[],
                            "ethnicities":[],
                            "origin":{
                                "step":step,
                                "age_bands":copy.deepcopy(dwelling_dict[ta][sa2][step][key]["age_bands"]),
                                "age_counts":copy.deepcopy(dwelling_dict[ta][sa2][step][key]["age_counts"])
                                }
                            }
                        # Find suitable individuals and assign them to the dwelling
                        for a in range(len(dwelling_dict[ta][sa2][step][key]["age_bands"])):
                            for b in range(dwelling_dict[ta][sa2][step][key]["age_counts"][a]):
                                available_ages = get_available_ages(['F','M'],dwelling_dict[ta][sa2][step][key]["age_bands"][a],in_individuals_dict[ta][sa2])
                                if age_restriction and len(available_ages) > 1 and len(final_dictionary[ta][sa2][key]["links"]) == 0 and '0-14' in available_ages:
                                    available_ages.remove('0-14')
                                final_sex,final_age,final_ethnicity,final_id = get_individual(
                                    available_ages,
                                    final_dictionary[ta][sa2][key]["ethnicities"],
                                    ethnic_assortativity,
                                    ethnic_assortativity_weight,
                                    census_matching,
                                    sum(final_dictionary[ta][sa2][key]["origin"]["age_counts"]),
                                    census_age_dict,
                                    census_eth_dict,
                                    in_individuals_dict[ta][sa2]
                                    )
                                final_dictionary[ta][sa2][key]["links"].append(final_id)
                                final_dictionary[ta][sa2][key]["ethnicities"].append(final_ethnicity)
                                final_dictionary[ta][sa2][key]["ages"].append(final_age)
                                in_individuals_dict[ta][sa2][final_sex][final_age][final_ethnicity].remove(final_id)
                        del dwelling_dict[ta][sa2][step][key]
    return final_dictionary, dwelling_dict, in_individuals_dict

def clone_dwellings(
    final_dictionary,
    in_individuals_dict,
    ethnic_assortativity,
    ethnic_assortativity_weight,
    age_restriction,
    census_matching,
    census_file,
    clone_threshold
    ):
    """ Clones dwellings from the final dictionary and populates them from the remaining people

    Args:
      final_dictionary (dict): Dictionary for the dwelling layer
      in_individuals_dict (dict): Dictionary of individuals to fill-up the cloned dwellings
      ethnic_assortativity (bool): Whether or not to fill a dwelling using its current ethnic assortativity
      ethnic_assortativity_weight (double): Probability of using ethnic assortativity for any specific dwelling
      age_restriction (bool): Whether or not to be able to use a 0-14 year old as the first individual in the dwelling
      census_matching (bool): Whether or not to use the census yaml to probabilistically fill dwellings
      census_file (str): Path to the census file yaml
      clone_threshold (int): Maximum number of times to attempt to find a suitable dwelling to duplicate
    
    Returns:
      The updated dwelling layer dictionary and dictionary of individuals that still need to be assigned to a dwelling
    """
    # Get census values and init counter
    census_age_dict,census_eth_dict = get_census_dictionaries(census_file)
    clone_count = 1
    # For TAs and SA2s with individuals to assign to dwellings
    for ta in in_individuals_dict:
        if ta not in final_dictionary:
            continue
        for sa2 in in_individuals_dict[ta]:
            if sa2 not in final_dictionary[ta]:
                continue
            # Get list of individuals
            keys = list(final_dictionary[ta][sa2].keys())
            current_attempts = 0
            # Get attributes of individuals
            people_available = get_people_available(in_individuals_dict[ta][sa2])
            # While below threshold and people still to be assigned
            while current_attempts < clone_threshold and len(keys) > 0 and sum(people_available) > 1:
                current_dwelling_id = random.choice(keys)
                people_available = get_people_available(in_individuals_dict[ta][sa2])
                # Check if dwelling available to be filled
                if check_availability(get_people_needed_single_dwelling(final_dictionary[ta][sa2][current_dwelling_id]["origin"]),
                    people_available
                    ):
                    current_attempts += 1
                    keys.remove(current_dwelling_id)
                    continue
                # Clone dwelling
                new_name = current_dwelling_id + "C" + str(clone_count).zfill(4)
                clone_count += 1
                current_attempts = 0
                final_dictionary[ta][sa2][new_name] = {
                    "links":[],
                    "ages":[],
                    "ethnicities":[],
                    "origin":copy.deepcopy(final_dictionary[ta][sa2][current_dwelling_id]["origin"])
                   }
                # Get individuals that can fit within the demographics of the dwelling and fill the cloned dwelling
                for a in range(len(final_dictionary[ta][sa2][current_dwelling_id]["origin"]["age_bands"])):
                    for b in range(final_dictionary[ta][sa2][current_dwelling_id]["origin"]["age_counts"][a]):
                        available_ages = get_available_ages(['F','M'],final_dictionary[ta][sa2][current_dwelling_id]["origin"]["age_bands"][a],in_individuals_dict[ta][sa2])
                        if age_restriction and len(available_ages) > 1 and len(final_dictionary[ta][sa2][new_name]["links"]) == 0 and '0-14' in available_ages:
                            available_ages.remove('0-14')
                        final_sex,final_age,final_ethnicity,final_id = get_individual(
                            available_ages,
                            final_dictionary[ta][sa2][new_name]["ethnicities"],
                            ethnic_assortativity,
                            ethnic_assortativity_weight,
                            census_matching,
                            sum(final_dictionary[ta][sa2][new_name]["origin"]["age_counts"]),
                            census_age_dict,
                            census_eth_dict,
                            in_individuals_dict[ta][sa2]
                            )
                        final_dictionary[ta][sa2][new_name]["links"].append(final_id)
                        final_dictionary[ta][sa2][new_name]["ethnicities"].append(final_ethnicity)
                        final_dictionary[ta][sa2][new_name]["ages"].append(final_age)
                        in_individuals_dict[ta][sa2][final_sex][final_age][final_ethnicity].remove(final_id)
    return final_dictionary, in_individuals_dict

def create_solo_dwellings(
    final_dictionary,
    in_individuals_dict
    ):
    """ Puts all remaining individuals in the network into a dwelling of size 1
    
    Args:
      final_dictionary (dict): Dictionary of the dwelling layer
      in_individuals_dict (dict): Dictionary of individuals to assign to a dwelling of size 1
    Returns:
      Final dwelling dictionary with added solo dwellings, dictionary of people still with no dwelling assigned (should be empty)

    """
    for ta in in_individuals_dict:
        solo_index = 1
        if ta not in final_dictionary:
            final_dictionary[ta] = {}
        for sa2 in in_individuals_dict[ta]:
            if sa2 not in final_dictionary[ta]:
                final_dictionary[ta][sa2] = {}
            people_available = get_people_available(in_individuals_dict[ta][sa2])
            while sum(people_available) > 0:
                new_name = "DW" + str(ta).zfill(2) + str(solo_index).zfill(8) + "_U"
                final_dictionary[ta][sa2][new_name] = {
                    "links":[],
                    }
                solo_index += 1
                available_ages = get_available_ages(['F','M'],["60+","30-59","15-29","0-14"],in_individuals_dict[ta][sa2])
                final_sex,final_age,final_ethnicity,final_id = get_individual(
                    available_ages,
                    [],
                    False,
                    0,
                    False,
                    1,
                    {},
                    {},
                    in_individuals_dict[ta][sa2]
                    )
                final_dictionary[ta][sa2][new_name]["links"].append(final_id)
                in_individuals_dict[ta][sa2][final_sex][final_age][final_ethnicity].remove(final_id)
                people_available = get_people_available(in_individuals_dict[ta][sa2])
    return final_dictionary, in_individuals_dict
 
def merge_individuals_dict(
    ind_dict_1,
    ind_dict_2
    ):
    """ Merges the individuals dictionaries after step 4 in the build dwelling layer process

    Args:
      ind_dict_1 (dict): Dictionary of individuals 
      ind_dict_2 (dict): Dictionary of other individuals

    Returns:
      Dictionary combining both of the individuals dictionaries
    """
    # For all individuals in the first dict
    for ta in ind_dict_1:
        if ta not in ind_dict_2:
            ind_dict_2[ta] = {}
        for sa2 in ind_dict_1[ta]:
            if sa2 not in ind_dict_2[ta]:
                ind_dict_2[ta][sa2] = {}
            sex_list = []
            for sex in ind_dict_1[ta][sa2]:
                if sex not in ind_dict_2[ta][sa2]:
                    ind_dict_2[ta][sa2][sex] = {}
                age_list = []
                for age in ind_dict_1[ta][sa2][sex]:
                    age_list.append(age)
                    if age not in ind_dict_2[ta][sa2][sex]:
                        ind_dict_2[ta][sa2][sex][age] = {}
                    eth_list = []
                    for ethnicity in ind_dict_1[ta][sa2][sex][age]:
                        if ethnicity not in ind_dict_2[ta][sa2][sex][age]:
                            ind_dict_2[ta][sa2][sex][age][ethnicity] = []
                        # Add in the individual to the other dictionar
                        ind_dict_2[ta][sa2][sex][age][ethnicity].extend(copy.deepcopy(ind_dict_1[ta][sa2][sex][age][ethnicity]))
                        eth_list.append(ethnicity)
        # Delete the individual from the first dictionary
                    for ethnicity in eth_list:
                        del ind_dict_1[ta][sa2][sex][age][ethnicity]
                for age in age_list:
                    del ind_dict_1[ta][sa2][sex][age]
            for sex in sex_list:
                del ind_dict_1[ta][sa2][sex]
    return ind_dict_2

def make_final_files(
    output_path,
    final_dictionary,
    readme
    ):
    """ Converts the final dictionary into a form to be merged in and saves it to files

    Args:
      output_path (str): Path to the output directory
      final_dictionary (dict): Dictionary containing information for the dwelling layer
      readme (str): Readme for the dwelling layer
    """
    if output_path[-1] != "/":
        output_path += "/"
    resultant_dict = {
        "nodes":{},
        "readme":readme
        }
    # Update counts and information for the dwelling layer
    for ta in final_dictionary:
        for sa2 in final_dictionary[ta]:
            for item in final_dictionary[ta][sa2]:
                resultant_dict["nodes"][item] = {
                    "links":copy.deepcopy(final_dictionary[ta][sa2][item]["links"]),
                    "TA":ta,
                    "SA2":sa2
                    }
    # Output files with random layer ID
    id_str = id_generator()
    readme = "DWELLING LAYER:\nUNIQUE PREFIX:\t" + id_str + "\n\n" + readme
    resultant_dict["readme"] = readme
    readme = extract_final_metrics(resultant_dict,readme)
    make_pickle(resultant_dict,output_path,id_str,"dw")
    make_readme(readme,output_path,id_str,"dw")
    make_layer_edgelist_nodelist(resultant_dict,output_path,id_str,"dw")
