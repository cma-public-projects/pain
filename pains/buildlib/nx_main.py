import networkx as nx
import pickle

from .universal_functions import extract_network_metrics, make_edge_list, make_nodelist_and_dictionary, get_git_revision_short_hash, get_git_remote, get_git_status
from .nx_functions import generate_dictionaries, generate_workplace_shutdown

def main(
    read_network,
    network_file,
    outpath,
    workplace_shutdown,
    workplace_shutdown_file,
    workplace_shutdown_use_workers,
    workplace_shutdown_headings,
    input_yaml_path
    ):
    """ This calls the nx functions to build the actual network object and adds any NPI shutdown attributes

    Args:
      read_network (bool): Whether or not to read in a specific network or current_network.pkl
      network_file (st): Path to specific network if desired
      outpath (str): Path to output directory
      workplace_shutdown (bool): Whether or not to add workplace shutdown levels to the network
      workplace_shutdown_file (str): Path to file containing workplace shutdown information
      workplace_shutdown_use_workers (bool): Whether or not worker count is used to determine business shutdowns
      workplace_shutdown_headings (list): Specifies column headings for the workplace shutdown file
      input_yaml_path (str): path to network construction yaml
    """
    # Read in network or use current pickle
    if read_network:
        open_file = network_file
    else:
        open_file = outpath + "current_network.pkl"
    in_file = open(open_file,'rb')
    in_dict = pickle.load(in_file)
    in_file.close()
    # Read in network object
    network = nx.Graph(readme=in_dict["readme"])
    # Generate shutdown attributes
    if workplace_shutdown:
        shutdown_dict_1,shutdown_dict_2 = generate_dictionaries(workplace_shutdown_file,workplace_shutdown_headings)
    age_dict = {
        '0-14':0,
        '15-29':1,
        '30-59':2,
        '60+':3
        }
    # For each individual add to network
    for individual in in_dict["individual nodes"]:
        network.add_node(
            individual,
            bipartite= 1,
            )
        for att in in_dict["individual nodes"][individual]:
            network.nodes[individual][att] = in_dict["individual nodes"][individual][att]
    # For each group node add to network
    for group_node in in_dict["group nodes"]:
        network.add_node(
            group_node,
            bipartite=0,
            SA2 = int(in_dict["group nodes"][group_node]["SA2"]),
            TA = int(in_dict["group nodes"][group_node]["TA"])
            )
        if group_node[:2] == "WP":
            network.nodes[group_node]["sector"] = in_dict["group nodes"][group_node]["sector"]
        # Add links between individuals and group node
        for individual in in_dict["group nodes"][group_node]["links"]:
            if individual not in in_dict["individual nodes"]:
                print("INDIVIDUAL %s IN GROUP NODE %s IS NOT PRESENT IN THE INDIVIDUALS LAYER.\nQUITTING" % (individual,group_node))
                quit()
            network.add_edge(individual,group_node)
        if group_node[:2] == "WP":
            if workplace_shutdown:
                shutdown_dict_2[network.nodes[group_node]["sector"]].append(group_node)
                for item in workplace_shutdown_headings:
                    network.nodes[group_node][item] = "OPEN"
                if workplace_shutdown_use_workers:
                    shutdown_dict_1[network.nodes[group_node]["sector"]]["initial_value"] += len(list(network[group_node]))
                else:
                    shutdown_dict_1[network.nodes[group_node]["sector"]]["initial_value"] += 1
    # Generate shutdown information, extract general network metrics, made final nodelists and edgelists and network gpickle
    if workplace_shutdown:
        generate_workplace_shutdown(network,shutdown_dict_1,shutdown_dict_2,workplace_shutdown_headings,workplace_shutdown_use_workers)
    extracted_metrics = extract_network_metrics(network)
    make_edge_list(network,in_dict["network_name"].replace('.pkl','.gpickle'))
    make_nodelist_and_dictionary(network,in_dict["network_name"].replace('.pkl','.gpickle'))

    wf = open(in_dict["network_name"].replace('.pkl','_README.txt'),'w')
    
    readme_preamble = "Network construction yaml path:" + input_yaml_path + "\n" +\
        "\nGIT HASH:" + get_git_revision_short_hash() +\
        "\nGIT REMOTE:" + get_git_remote() +\
        "\nGIT STATUS:" + get_git_status() + "\n"

    wf.write(readme_preamble + extracted_metrics + "\n" + in_dict["readme"])
    wf.close()
    with open(in_dict["network_name"].replace('.pkl','.gpickle'),'wb') as fp:
        pickle.dump(network, fp, protocol=4)