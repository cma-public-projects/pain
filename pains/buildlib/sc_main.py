import time

from .sc_functions import fill_schools, generate_individuals_dict, generate_ece_dictionary, generate_schools_dictionaries, make_final_files, update_readme
from .universal_functions import csv_to_dict, generate_commute_dictionary, get_git_revision_short_hash, get_git_status,get_git_remote

def main(
    labels,
    individuals_file,
    output_directory,
    ece_file,
    school_file,
    commute_file,
    excluded_sa2s,
    excluded_tas,
    sc_parameters
    ):
    """ Calls the schools functions to generate the school layer

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      individuals_node_list (str): Path to the individuals nodelist csv
      output_path (str): Path to the output directory      
      ethnic_assortativity (bool): Whether to use current dwelling ethnic assortativity when filling dwellings
      ethnic_assortativity_weight (double): Probability of using ethnic assortativity for a specific dwelling
      small_dwelling_nodes (str): Path to the small dwelling nodelist csv
      large_dwelling_nodes (str): Path to the large dwelling nodelist csv
      census_matching (bool): Whether to try match the census dwelling size distribution
      census_file (str): Path to the census dwelling size distribution file
      restrict_age (bool): Whether to restrict the first individual in a dwelling to be a non-minor
      clone_threshold (int): Number of times to attempt to find a suitable dwelling to clone for un-housed individuals to populate
      excluded_sa2s (list): List of SA2s to exclude from the network
      excluded_tas (list): List of TAs to exclude from the network
    """
    # READ FILES AND CONVERT THEM INTO DICTIONARIES
    readme = "SC AND PM YAML AND INPUT\n" +\
        "\nGIT HASH:" + get_git_revision_short_hash() +\
        "\nGIT REMOTE:" + get_git_remote() +\
        "\nGIT STATUS:" + get_git_status() +\
        "\nGENERIC\n\n" +\
        "individuals_file: " + str(individuals_file) + "\n" + \
        "output_directory: " + str(output_directory) + "\n" +\
        "excluded_sa2s: " + str(excluded_sa2s) + "\n" +\
        "excluded_tas: " + str(excluded_tas) + "\n\n" +\
        "sc_parameters\n\n" +\
        "ece_file: " + str(ece_file) + "\n" +\
        "school_file: " + str(school_file) + "\n" +\
        "commute_file: " + str(commute_file) + "\n" +\
        "school_characteristics:\n"

    sc_parameters = csv_to_dict(sc_parameters)

    for a in sc_parameters:
        readme += "  " + a + ":\n"
        readme += "  under_15: " + str(sc_parameters[a]["under_15"]) + "\n"
        readme += "  over_14: " + str(sc_parameters[a]["over_14"]) + "\n"
        readme += "  secondary: " + str(sc_parameters[a]["secondary"]) + "\n\n"

    readme += "SCHOOLS DICT ERRORS\n"

    ta_dictionary, individuals_dictionary = generate_individuals_dict(
        labels["ih_labels"],
        individuals_file,
        excluded_tas,
        excluded_sa2s
        )

    one_age_group_school_dictionary, multi_age_group_school_dictionary,school_index, readme =  generate_schools_dictionaries(
        labels["sc_labels"]["sc_nodes"],
        school_file,
        sc_parameters,
        excluded_tas,
        excluded_sa2s,
        readme
        )

    ece_dictionary,readme = generate_ece_dictionary(
        labels["sc_labels"]["ece_nodes"],
        ece_file,
        sc_parameters,
        excluded_tas,
        excluded_sa2s,
        school_index,
        readme
        )

    commute_dictionary = generate_commute_dictionary(
        labels["commute_labels"],
        commute_file,
        excluded_sa2s,
        excluded_tas
        )


    final_dictionary = {}

    readme += "\nINITIALISATION\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    # Step 1
    final_dictionary,one_age_group_school_dictionary,individuals_dictionary,commute_dictionary = fill_schools(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        one_age_group_school_dictionary,
        ta_dictionary,
        "SA2"
        )
    readme += "STEP 1\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    # Step 2
    final_dictionary,multi_age_group_school_dictionary,individuals_dictionary,commute_dictionary = fill_schools(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        multi_age_group_school_dictionary,
        ta_dictionary,
        "SA2"
        )
    readme += "STEP 2\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    # Step 3
    final_dictionary,one_age_group_school_dictionary,individuals_dictionary,commute_dictionary = fill_schools(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        one_age_group_school_dictionary,
        ta_dictionary,
        "TA"
        )
    readme += "STEP 3\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    # Step 4
    final_dictionary,multi_age_group_school_dictionary,individuals_dictionary,commute_dictionary = fill_schools(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        multi_age_group_school_dictionary,
        ta_dictionary,
        "TA"
        )
    readme += "STEP 4\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    # Step 5
    final_dictionary,ece_dictionary,individuals_dictionary,commute_dictionary = fill_schools(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        ece_dictionary,
        ta_dictionary,
        "SA2"
        )
    readme += "STEP 5\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    # Step 6
    final_dictionary,ece_dictionary,individuals_dictionary,commute_dictionary = fill_schools(
        final_dictionary,
        individuals_dictionary,
        commute_dictionary,
        ece_dictionary,
        ta_dictionary,
        "TA"
        )
    readme += "STEP 6\n"
    readme = update_readme(readme,final_dictionary,one_age_group_school_dictionary,multi_age_group_school_dictionary,ece_dictionary,individuals_dictionary)

    readme += "UNASSIGNED STUDENTS ARE 0-14 YEAR OLDS WHO ARE NOT ENROLLED (BUT COULD BE) AND 15-30 YEAR OLDS IN TERTIARY EDUCATION WHO ARE NOT ASSIGNED IN OUR SCHOOL LAYER.\n"
    # Step 7
    make_final_files(
        output_directory,
        final_dictionary,
        ece_dictionary,
        one_age_group_school_dictionary,
        multi_age_group_school_dictionary,
        readme
        )
