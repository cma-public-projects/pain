import pickle
import copy

from .universal_functions import gen_indices_dict, id_generator, make_pickle

def read_in_layer(layer, final_dictionary):
    """ Reads in a pickle file and adds it to the current network
    
    Args: 
      layer (str): Path to the layer pickle file to read in
      final_dictionary (dict): Network dictionary to add layer to

    Returns:
      The updated final_dictionary
    """
    # Read in layer
    rf = open(layer,"rb")
    rf_dict = pickle.load(rf)
    rf.close()
    # Add to dictionary and record in readme
    final_dictionary["group nodes"].update(rf_dict["nodes"])
    final_dictionary["readme"] += "\n" + rf_dict["readme"]
    return final_dictionary

def ind_var(item, desired_type):
    """ Creates a dictionary item of a given type

    Args:
      item (any): Item to cast to a specific type
      desired_type (str): Resultant form of the item to cast to

    Returns:
      A string, integer, float, list of strings, list of integers, or a list of floats
    """
    if desired_type == "str":
        return str(item)
    elif desired_type == "int":
        return int(item)
    elif desired_type == "float":
        return float(item)
    elif desired_type == "list-str":
        out_list = []
        splitter = item.split(";")
        for a in splitter:
            out_list.append(str(a))
        return out_list
    elif desired_type == "list-int":
        out_list = []
        if item == "":
            return out_list
        splitter = item.split(";")
        for a in splitter:
            out_list.append(int(a))
        return out_list
    elif desired_type == "list-float":
        out_list = []
        splitter = item.split(";")
        for a in splitter:
            out_list.append(float(a))
        return out_list

def create_custom_layer(
    labels,
    final_dictionary,
    parameters
    ):
    """ Reads in a custom edgelist and nodelist and creates a new nodes in the network
    
    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      final_dictionary (dict): Network dictionary to add a custom layer to
      parameters (dict): Dictionary of parameters for the custom layer (nodelist/edgelist paths)

    Returns:
      The final_dictionary with the custom layer added in
    """
    # If using a custom nodelist add nodes to network
    if "nodelist" in parameters:
        rf = open(parameters["nodelist"],"r")
        rline = rf.readlines()
        rf.close()
        l_0 = rline[0].replace("\n","").split(",")
        indices_dict = gen_indices_dict(l_0,labels["node_list"])
        for a in range(1,len(rline)):
            line_split = rline[a].replace("\n","").split(",")
            if line_split[indices_dict["node_id"]] in final_dictionary["group nodes"]:
                print("NODE %s ALREADY IN NETWORK\nQUITTING" % line_split[indices_dict["group_node_id"]])
                quit()
            final_dictionary["group nodes"][line_split[indices_dict["node_id"]]] = {"TA":line_split[indices_dict["TA"]],"SA2":line_split[indices_dict["SA2"]],"links":[]}
    # Add custom edgelist to network
    rf = open(parameters["edgelist"],"r")
    rline = rf.readlines()
    rf.close()
    l_0 = rline[0].replace("\n","").split(",")
    indices_dict = gen_indices_dict(l_0,labels["edge_list"])
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if line_split[indices_dict["group_node_id"]] not in final_dictionary["group nodes"]:
            print("CANT PLACE INDIVIDUAL %s IN GROUP NODE %s BECAUSE %s IS NOT IN THE NETWORK" % (line_split[indices_dict["individual_node_id"]],line_split[indices_dict["group_node_id"]],line_split[indices_dict["group_node_id"]]))
            quit()
        elif line_split[indices_dict["individual_node_id"]] in final_dictionary["group nodes"][line_split[indices_dict["group_node_id"]]]["links"]:
            print("ERROR INDIVIDUAL %s CANNOT BE PLACED IN NODE %s BECAUSE THEY ARE ALREADY IN IT" % (line_split[indices_dict["individual_node_id"]],line_split[indices_dict["group_node_id"]]))
            quit()
        final_dictionary["group nodes"][line_split[indices_dict["group_node_id"]]]["links"].append(line_split[indices_dict["individual_node_id"]])
    return final_dictionary

def link_group_nodes(
    labels,
    final_dictionary,
    edgelist
    ):
    """ Reads in an edge list of group nodes to group nodes   

    Args:
      labels (dict): A dictionary mapping the columns in the nodelist csvs to the resultant node attributes in the network
      final_dictionary (dict): Network dictionary to add group node to group node edges to
      edgelist (str): Path to the edgelist csv to add in

    Returns:
      The final_dictionary with the edgelist layer added in
    """
    # Read in edgelist
    rf = open(edgelist,"r")
    rline = rf.readlines()
    rf.close()
    l_0 = rline[0].replace("\n","").split(",")
    indices_dict = gen_indices_dict(l_0,labels)
    # For every new edge in the edgelist add ito the network if able
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if "NA" in line_split:
            continue
        if line_split[indices_dict["from_node"]] not in final_dictionary["group nodes"]:
            print("UNABLE TO LINK NODE %s TO NODE %s AS SPECIFIED IN EDGELIST %s AS %s IS NOT IN THE NETWORK" % (line_split[indices_dict["from_node"]],line_split[indices_dict["to_node"]],edgelist,line_split[indices_dict["from_node"]]))
            quit()
        if line_split[indices_dict["to_node"]] not in final_dictionary["group nodes"]:
            print("UNABLE TO LINK NODE %s TO NODE %s AS SPECIFIED IN EDGELIST %s AS %s IS NOT IN THE NETWORK" % (line_split[indices_dict["from_node"]],line_split[indices_dict["to_node"]],edgelist,line_split[indices_dict["to_node"]]))
            quit()
        final_dictionary["readme"] += "LINKING NODE %s TO %s AS SPECIFIED IN EDGELIST %s" % (line_split[indices_dict["from_node"]],line_split[indices_dict["to_node"]],edgelist)
        for individual in final_dictionary["group nodes"][line_split[indices_dict["from_node"]]]["links"]:
            if individual not in final_dictionary["group nodes"][line_split[indices_dict["to_node"]]]["links"]:
                final_dictionary["group nodes"][line_split[indices_dict["to_node"]]]["links"].append(individual)
            else:
                final_dictionary["readme"] += "\tINDIVIDUAL %s IS ALREADY IN %s" % (individual,line_split[indices_dict["to_node"]])
                print("\tINDIVIDUAL %s IS ALREADY IN %s" % (individual,line_split[indices_dict["to_node"]]))
    return final_dictionary

def read_in_individuals(
    individuals_file,
    exclude_ta,
    exclude_sa2,
    final_dictionary,
    individual_attributes
    ):
    """ Reads in the individuals file and appends it to the final dictionary

    Args: 
      individuals_file (str): Path to the individuals nodelist csv
      exclude_ta (list): List of SA2s to exclude from the network
      exclude_sa (list): List of TAs to exclude from the network
      final_dictionary (dict): Network dictionary to add individuals to
    
    Returns:
      The final_dictionary with the individuals dictionary added in
    """
    rf = open(individuals_file,"r")
    rline = rf.readlines()
    rf.close()
    indices_dict = {"Individual_ID":"x"}
    for a in individual_attributes:
        if individual_attributes[a]["include"]:
            indices_dict[individual_attributes[a]["column_heading"]] = "x"
    # Get index of column headings needed for this calc
    index_split = rline[0].replace("\n","").split(",")
    age_dict = {
        "0-14":0,
        "15-29":1,
        "30-59":2,
        "60+":3
        }
    for a in range(len(index_split)):
        if index_split[a] in indices_dict:
            indices_dict[index_split[a]] = a
    # For all individuals, add their attributes to the final dictionary
    for a in range(1,len(rline)):
        line_split = rline[a].replace("\n","").split(",")
        if "TA" in individual_attributes and int(line_split[indices_dict[individual_attributes["TA"]["column_heading"]]]) in exclude_ta:
            continue
        if "SA2" in individual_attributes and int(line_split[indices_dict[individual_attributes["SA2"]["column_heading"]]]) in exclude_sa2:
            continue
        final_dictionary["individual nodes"][line_split[indices_dict["Individual_ID"]]] = {}
        if "age" in individual_attributes and individual_attributes["age"]["include"]:
            final_dictionary["individual nodes"][line_split[indices_dict["Individual_ID"]]]["age"] = ind_var(age_dict[line_split[indices_dict[individual_attributes["age"]["column_heading"]]]],individual_attributes["age"]["type"])
        for item in individual_attributes:
            if item == "age":
                continue
            if individual_attributes[item]["include"]:
                final_dictionary["individual nodes"][line_split[indices_dict["Individual_ID"]]][item] = ind_var(line_split[indices_dict[individual_attributes[item]["column_heading"]]],individual_attributes[item]["type"])
    return final_dictionary

def make_final_files(final_dictionary, output_path):
    """ Creates the final files

    Args:
      final_dictionary (dict): Dictionary of the final network object
      output_path (str): Path to output directory
    """
    id_str = id_generator()
    readme = "FINAL NETWORK:\nUNIQUE PREFIX:\t" + id_str + "\n\n" + final_dictionary["readme"]
    final_dictionary["network_name"] = output_path + id_str + "_" + "network" + ".pkl"
    make_pickle(final_dictionary,output_path,id_str,"network")
