Outputs directory containing the files created by 'PAINputs', this is mostly 'nodelists' but also some processing IDI files that are needed for PAIN.

Analysis files and summary plots etc. may also live here, but that is TBC.

This folder is ignored by git by default. You can choose to either change the code defaults to save your outputs in a different folder on your local machine 

[TBC what you need to change and where - maybe a specific text file with the path like we have for Dropbox paths in the original nodelist repos.]