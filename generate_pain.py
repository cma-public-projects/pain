#! /usr/bin/env python3
from sys import argv

from pains.buildlib import make_network

if __name__ == "__main__":
    
    make_network.main(argv[1])