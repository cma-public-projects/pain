SELECT
	count(distinct snz_uid) as triple_count
	,is_maori
	,is_pacific
	,sex
	,age_band
	,work_ind
	,education_ind
	,ur_sa2
	,ur_ta
FROM
(
SELECT
	[census_individual_2018].[snz_uid]
	,[cen_ind_sex_code] as sex -- 1=M, 2=F
	,[cen_ind_age_code] as age
	,CASE 
		when [cen_ind_age_code] >=0 and [cen_ind_age_code] <15 then '0-14'
		when [cen_ind_age_code] >=15 and [cen_ind_age_code] <30 then '15-29'
		when [cen_ind_age_code] >=30 and [cen_ind_age_code] <60 then '30-59'
		else '60+'
	END as age_band
--	,[cen_ind_five_yr_age_grp_code] as five_yr_band
	,[cen_ind_ethgr_maori_ttl_code] as is_maori --20=Y
	,[cen_ind_ethgr_pacific_ttl_code] as is_pacific --30=Y
	,[cen_ind_ur_address_sa2] as ur_sa2
	,[cen_ind_ur_address_ta] as ur_ta
--	,[cen_ind_wp_address_sa2] as workplace_sa2
--	,[cen_ind_wp_address_ta] as workplace_ta
	,CASE
		when [cen_ind_wklfs_code] = '1' then 'Y' --1 means "Full-time"
		when [cen_ind_wklfs_code] = '2' then 'Y' --2 means "Part-time"
		else 'N'
	END as work_ind
	
	,CASE
		when [cen_ind_study_prtpcn_code] = '1' then 'Y' --1 means "full-time"
		when [cen_ind_study_prtpcn_code] = '2' then 'Y' --2 means "part-time"
		else 'N'
	 END as education_ind
FROM
	[IDI_Clean_20200120].[cen_clean].[census_individual_2018]
--JOIN
--	[IDI_Clean_20200120].[data].[personal_detail]
--	ON [census_individual_2018].[snz_uid] = [personal_detail].[snz_uid]
) as sq1
--WHERE ur_sa2 > 300000 -- REMEMBER TO CHANGE THIS!!!
GROUP BY
	[is_maori]
	,[is_pacific]
	,[sex]
	,[age_band]
	,[work_ind]
	,[education_ind]
	,[ur_sa2]
	,[ur_ta]
ORDER BY
	[ur_ta]
	,[ur_sa2]
	,age_band
