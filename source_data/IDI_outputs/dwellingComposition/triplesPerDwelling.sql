SELECT
	ur_dwl_id
	,count(distinct snz_uid) as triple_count
	,is_maori
	,is_pacific
	,sex
	,age_band
	,ur_sa2
	,ur_ta
	--,visitor
FROM
(
SELECT
	[census_individual_2018].[snz_uid]
	,[ur_snz_cen_dwell_uid] as ur_dwl_id
	--,[cen_ind_visitor_ind] as visitor --0=No, 1=Yes w/ UR, 2=Yes w/o UR
	,[cen_ind_sex_code] as sex -- 1=M, 2=F
	,[cen_ind_age_code] as age
	,CASE 
		when [cen_ind_age_code] >=0 and [cen_ind_age_code] <15 then '0-14'
		when [cen_ind_age_code] >=15 and [cen_ind_age_code] <30 then '15-29'
		when [cen_ind_age_code] >=30 and [cen_ind_age_code] <60 then '30-59'
		else '60+'
	END as age_band
--	,[cen_ind_five_yr_age_grp_code] as five_yr_band
	,[cen_ind_ethgr_maori_ttl_code] as is_maori --20=Y
	,[cen_ind_ethgr_pacific_ttl_code] as is_pacific --30=Y
	,[cen_ind_ur_address_sa2] as ur_sa2
	,[cen_ind_ur_address_ta] as ur_ta
--	,[cen_ind_wp_address_sa2] as workplace_sa2
--	,[cen_ind_wp_address_ta] as workplace_ta
FROM
	[IDI_Clean_20200120].[cen_clean].[census_individual_2018]
--JOIN
--	[IDI_Clean_20200120].[data].[personal_detail]
--	ON [census_individual_2018].[snz_uid] = [personal_detail].[snz_uid]
) as sq1
--WHERE ur_sa2 > 300000 -- REMEMBER TO CHANGE THIS!!!
--WHERE visitor = 2 --no visitors w/o a UR
GROUP BY
	[sq1].[ur_dwl_id]
	,[is_maori]
	,[is_pacific]
	,[sex]
	,[age_band]
	,[ur_sa2]
	,[ur_ta]
	--,visitor
ORDER BY
	[ur_ta]
	,[ur_sa2]
	,[ur_dwl_id] DESC
	,age_band
