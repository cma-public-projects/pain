---
title: "DwellingDegree_Triple"
author: "Steven Turnbull"
date: "1/10/2020"
output: html_document
---

```{R - Libraries, message = FALSE}
#Import necessary libraries
library(RODBC)
library(dplyr)
library(tidyr)
library(reshape2)
library(stringr)
library(magrittr)
library(readr)
library(ggplot2)
library(forcats)
library(here)  # needs installing locally
```

```{R Random Rounding Function}
#Random rounding
rrn <- function(x, n=3, seed)
{
  if (!missing(seed)) set.seed(seed)
  
  rr <- function(x, n){
    if (is.na(x)) return(0)
    if ((x%%n)==0) return(x)
    res <- abs(x)
    lo <- (res%/%n) * n
    if ((runif(1) * n) <= res%%n) res <- lo + n
    else res <- lo
    return(ifelse(x<0, (-1)*res, res))
  }
  
  isint <- function(x){
    x <- x[!is.na(x)]
    sum(as.integer(x)==x)==length(x)
  }
  
  if (class(x) %in% c("numeric", "integer")){
    if(isint(x)) return(sapply(x, rr, n))
    else return(x)
  }
  
  for (i in 1:ncol(x))
  {
    if (class(x[,i]) %in% c("numeric", "integer") &
        isint(x[,i])) x[,i] <- sapply(x[,i], rr, n)
  }
  x
}

```


```{R SQL Import}
sql_query <- read_file("triplesPerDwelling.sql")
  
  #open connection to sql
con=odbcDriverConnect("Driver=ODBC Driver 17 for SQL Server; Trusted_Connection=YES; Server=PRTPRDSQL36.stats.govt.nz,1433;Database=IDI_Clean_20200120")

  
#Run query and save as df
df <- sqlQuery(con, sql_query) 
  #close connection
odbcCloseAll()

#population count test
print(paste0("Population from sql: ", sum(df$triple_count)))
```

```{R Clean df}
#calculate dwelling size 
df_clean <- df %>% 
  group_by(ur_dwl_id,ur_sa2) %>%
  mutate(dwl_size = sum(triple_count)) %>%
  ungroup() 

df_clean <- df_clean %>%
  mutate(dwl_size = ifelse(is.na(ur_dwl_id),NA,dwl_size))

# head(df_clean)

# checking all 'sex' entries are 1 or 2
if(df_clean %>% filter(!(sex %in% c(1,2))) %>% nrow()!= 0){message('There are some entries with no values for sex')}

#Recode sex from 1 and 2 to M and F (checked using 2018 Census variables metadata file on IDI wiki)
df_clean <- df_clean %>% mutate(sex = recode(sex,
                                             `1` = "M",
                                             `2` = "F")
                                )


# tidying ethnicity

df_clean <- df_clean %>%
  mutate(ethnicity = case_when(
    is_maori==0 & is_pacific == 0  ~  'Other',
    is_maori==0 & is_pacific == 30  ~  'Pacific',
    is_maori==20 & is_pacific == 0  ~  'Maori',
    is_maori==20 & is_pacific == 30  ~  'MaoriPacific',
    TRUE  ~ "NA"
    )
  ) %>%
  mutate(ethnicity = factor(ethnicity,
                            levels = c(
                            'MaoriPacific',
                            'Maori',
                            'Pacific',
                            'Other'))
         ) %>%
  #recategorise top dwelling sizes so group big dwellings
  mutate(small_dwelling = ifelse(dwl_size<20, "Y", "N"))

# check
if(df_clean %>% filter(is.na(ethnicity)) %>% nrow()!= 0) {message( 'There are some entries with unrecognised entries for ethnicity')}
```

```{R}

# national counts
df_nz_counts_raw <- df_clean %>%
  group_by(ethnicity,sex,age_band,small_dwelling) %>%
  #work out number of unique dwellings for a triple
  mutate(number_unique_dwellings = n_distinct(ur_dwl_id)) %>%
  # if the number of unique dwellings is below 3, remove
  filter(number_unique_dwellings > 2) %>%
  #Begin aggregation
  summarise(triple_count_total = sum(triple_count)) %>%
  ungroup()
  
write.csv(df_nz_counts_raw,"outputs/dwellingTriples_smallDwellings_nz_raw.csv")

#suppress values less than 6 on dwelling size columns, but keep 0s. This means that instances where there are 0 cases for a triple are NOT suppressed.
df_nz_counts_safe <- df_nz_counts_raw %>%
  mutate(triple_count_total = ifelse((triple_count_total != 0 & triple_count_total < 6),  -999999, triple_count_total)) %>%
  mutate(triple_count_total = ifelse(triple_count_total == 0, -999, triple_count_total))
#random rounding
#Apply random round on counts
df_nz_counts_safe$triple_count_total <- rrn(x = df_nz_counts_safe$triple_count_total, n = 3)
write.csv(df_nz_counts_safe,"outputs/dwellingTriples_smallDwellings_nz_safe.csv")
```

```{R}
# TA counts
df_ta_counts_raw <- df_clean %>%
  group_by(ur_ta, ethnicity,sex,age_band,small_dwelling) %>%
 #work out number of unique dwellings for a triple
  mutate(number_unique_dwellings = n_distinct(ur_dwl_id)) %>%
  # if the number of unique dwellings is below 3, remove
  filter(number_unique_dwellings > 2) %>%
 
  #Begin aggregation
  summarise(triple_count_total = sum(triple_count)) %>%
  ungroup() 
  
write.csv(df_ta_counts_raw,"outputs/dwellingTriples_smallDwellings_ta_raw.csv")

#suppress values less than 6 on dwelling size columns, but keep 0s. This means that instances where there are 0 cases for a triple are NOT suppressed.
df_ta_counts_safe <- df_ta_counts_raw %>%
  mutate(triple_count_total = ifelse((triple_count_total != 0 & triple_count_total < 6),  -999999, triple_count_total)) %>%
  mutate(triple_count_total = ifelse(triple_count_total == 0, -999, triple_count_total))
#random rounding
#Apply random round on counts
df_ta_counts_safe$triple_count_total <- rrn(x = df_ta_counts_safe$triple_count_total, n = 3)

write.csv(df_ta_counts_safe,"outputs/dwellingTriples_smallDwellings_ta_safe.csv")

```

```{R}
# SA2 counts
df_sa2_counts_raw <- df_clean %>%
  group_by(ur_sa2, ethnicity,sex,age_band,small_dwelling) %>%
  #work out number of unique dwellings for a triple
  mutate(number_unique_dwellings = n_distinct(ur_dwl_id)) %>%
  ungroup() %>%
  # if the number of unique dwellings is below 3, remove
  filter(number_unique_dwellings > 2) %>%
  group_by(ur_sa2, ethnicity,sex,age_band,small_dwelling) %>%
  #Begin aggregation
  summarise(triple_count_total = sum(triple_count)) %>%
  ungroup() 
  
write.csv(df_sa2_counts_raw,"outputs/dwellingTriples_smallDwellings_sa2_raw.csv")

#suppress values less than 6 on dwelling size columns, but keep 0s. This means that instances where there are 0 cases for a triple are NOT suppressed.
df_sa2_counts_safe <- df_sa2_counts_raw %>%
  mutate(triple_count_total = ifelse((triple_count_total != 0 & triple_count_total < 6),  -999999, triple_count_total)) %>%
  mutate(triple_count_total = ifelse(triple_count_total == 0, -999, triple_count_total))
#random rounding
#Apply random round on counts
df_sa2_counts_safe$triple_count_total <- rrn(x = df_sa2_counts_safe$triple_count_total, n = 3)

write.csv(df_sa2_counts_safe,"outputs/dwellingTriples_smallDwellings_sa2_safe.csv")
```