/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [snz_uid]
	,[cen_ind_sex_code] as sex -- 1=M, 2=F
	,[cen_ind_age_code] as age
   
   ,CASE 
		when [cen_ind_age_code] >=0 and [cen_ind_age_code] <15 then '0-14'
		when [cen_ind_age_code] >=15 and [cen_ind_age_code] <30 then '15-29'
		when [cen_ind_age_code] >=30 and [cen_ind_age_code] <60 then '30-59'
		else '60+'
	END as age_band
	
    ,[cen_ind_ethgr_maori_ttl_code] as is_maori --20=Y
	,[cen_ind_ethgr_pacific_ttl_code] as is_pacific --30=Y

	,[cen_ind_ur_address_rc]
	,[cen_ind_ur_address_ta]
	,[cen_ind_ur_address_sa2]
    ,[cen_ind_emplnt_stus_code]
    ,[cen_ind_occupation_code]
    ,[cen_ind_industry_code]
    ,[cen_ind_sector_of_ownp_code]
    ,[cen_ind_wklfs_code]
    ,[cen_ind_trvl_to_work_recode]
	,[cen_ind_job_ind_code]
	,[cen_ind_wp_address_rc]
    ,[cen_ind_wp_address_ta]
	,[cen_ind_wp_address_sa2]


  FROM [IDI_Clean_20201020].[cen_clean].[census_individual_2018]