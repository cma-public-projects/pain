
SELECT [snz_uid]
      ,[snz_employer_ird_uid]
      ,[ir_ems_pbn_nbr]
      ,[ir_ems_pbn_anzsic06_code]
	  ,[sa2_code]
      ,[ta_code]
	  --,[br_pbn_dim_start_month_key]
	  --,[br_pbn_dim_end_month_key]
	 
	  --,[ir_ems_return_period_date]

  FROM [IDI_Clean_20201020].[ir_clean].[ird_ems]

  -- join PBN table to get employer details 
  LEFT JOIN (
            -- This table provides a list of PBNs with their location in the time specified.
              SELECT [br_pbn_pbn_nbr]
			  ,[sa2_code]
			  ,[ta_code]
	
			  FROM (
				    SELECT [br_pbn_pbn_nbr]
							,[sa2_code]
							,[ta_code]
							,[br_pbn_dim_end_month_key]
							,max([br_pbn_dim_end_month_key]) OVER (PARTITION BY [br_pbn_pbn_nbr])  as max_end_month
							FROM [IDI_Clean_20201020].[br_clean].[pbn] 
							
							LEFT JOIN [IDI_Metadata].[clean_read_CLASSIFICATIONS].[meshblock_current] -- this is where we get SA2 and TA info, need to double check this is the correct concordance (MB2018?)
							ON [br_pbn_geo_meshblock_code] = [IDI_Metadata].[clean_read_CLASSIFICATIONS].[meshblock_current].[meshblock_code]

							-- get all busineses in months leading up to and including census month 2018
							WHERE ([br_pbn_dim_end_month_key] < '201804') --only pick out the business info for census month and the months preceding. 
							AND 	[sa2_code] IS NOT NULL		-- only pick out rows that have location info
					 ) AS TableB
			   -- will get single row for the PBN info closest to census month
			   WHERE TableB.[br_pbn_dim_end_month_key] = TableB.max_end_month

			   ) AS TableA  
			 
  ON [IDI_Clean_20201020].[ir_clean].[ird_ems].[ir_ems_pbn_nbr] = TableA.[br_pbn_pbn_nbr]

  -- select employees who had info returned in specific time period
  WHERE  [ir_ems_return_period_date] like '2018-03-%%' --for tax records in census month 2018. 
														   

 -- only select from work and salary info.
  AND    [ir_ems_income_source_code] like 'W&S'


 -- Now group over the selected columns to remove duplicated rows.
 -- Note - duplicate snz_uid may still exist where an individual has two or more jobs
  GROUP BY [snz_uid]
      ,[snz_employer_ird_uid]
      ,[ir_ems_pbn_nbr]
      ,[ir_ems_pbn_anzsic06_code]
	  ,[sa2_code]
      ,[ta_code]


	  