This is where the raw source data for the nodelist and network construction lives. 

The files in this folder (and its subfolders) should be considered read only. They are quite large, and any edits will risk bloating the repository.