#!/bin/bash -e
#SBATCH --job-name=building_network
#SBATCH --account=uoa03023
#SBATCH --time=01:00:00
#SBATCH --mem=25GB

module purge
export PYTHONPATH="/nesi/project/uoa03023/PyPackages/lib/python3.7/site-packages:$PYTHONPATH"
module load Python/3.7.3-gimkl-2018b

python generate_network.py
