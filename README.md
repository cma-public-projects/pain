# Populated Aotearoa Interaction Network (PAIN)

*NOTE: this repository is still a Work in Progress*

`PAINputs` is a set of `R` files for building the required `pain` inputs (hence PAINputs). These inputs consist of the synthetic population nodelists (individuals, dwellings, schools, workplaces, and community 'events') which are constructed using data from the New Zealand Integrated Data Infrastructure (IDI) and other sources.

`pain` is a `Python` library for building a linked synthetic population of Aotearoa New Zealand representated as a multi-layer, bipartite interaction network. It relies heavily on input data from the New Zealand Integrated Data Infrastructure (IDI) and other sources which are produced in the `PAINputs` section of this repo.

Both the `PAINputs` data and analysis and the interaction networks produced by the `pain` library can be used for research purposes. The networks produced by the `pain` library have been designed to be used with [`cobin`](https://gitlab.com/cma-public-projects/cobin) -- the associated library for simulating contagion on a bipartite network -- and together have played a key role in the prediction and modelling of the spread of COVID-19 in New Zealand by COVID-19 Modelling Aotearoa from 2020-2023.

## Documentation
*NOTE - THIS IS OUT OF DATE* HTML documentation on how to install and run the package, as well as how the model works can be found in the directory `docs/build/html` the landing page is `index.html` [here](docs/build/html/index.html)

Details on development and building of the documentation is [here](docs/README.md)

# PAIN inputs (PAINputs)

## Root directory files (`PAINputs`)

`PAINputs` is currently just set of .Rmd notebooks for building the synthetic population nodelists (individuals, dwellings, schools, workplaces, and community 'events') using data from the New Zealand Integrated Data Infrastructure (IDI) and other sources, as well as for then building and cleaning/manipulating source data files that are needed as inputs for the `PAIN` library.

This will hopefully get operationalised into some files in the root directory in future, but we are not there yet.

### Run files (`PAINputs`)

- `PAINputs/build/construct_individuals_nodelist.Rmd` *included* : A notebook that constructs a individual nodelist based primarily on Census 2018 data from within the IDI. All individuals in NZ have attributes: ageband [0-14,15-29,30-59,60+]; sex [M,F]; ethnicity [Maori,Pacific,MaoriPacific,Other], SA2 of usual residence, TA of usual residence. Depending on selections in the Rmd, individuals may also have: dwelling info status (present in census2018 or not), dwelling size (large or small), workforce status, education status, domestic travel links, vaccine status, or vaccine dates.

- `PAINputs/build/construct_dwelling_nodelist.Rmd` *included*: A notebook that constructs a dwellings nodelist based primarily on Census 2018. All dwellings have a size, and for small dwellings (under 20 people) most will also have age structure (number of people in each of the four agebands [0-14,15-29,30-59,60+] in the dwelling). Depending on selections in the Rmd, dwellings may also have an 'aged care' flag (Y/N).

- `PAINputs/build/construct_workplace_nodelist.Rmd` *not included yet*
- `PAINputs/build/construct_school_nodelist.Rmd` *not included yet*
- `PAINputs/build/tidying_pain_config_files.Rmd` *not included yet*

# Populated Aotearoa Interaction Network

## Root directory files (`pain`)

### Run files (`pain`)
- generate_pain.py: A script to construct and output a `pain` network object. The `pain` documentation tutorial has step by step instructions for how to do this.
- process_pain.py: A script to run the `pain` postprocessing on a `pain` network instance (`.gpickle` files). The `pain` documentation tutorial has step by step instructions for how to do this.

### Dependency files
- setup.cfg
- environment.yaml

## Other files

## Disclaimer

The results in this work are not official statistics. They have been created for research purposes from the Integrated Data Infrastructure (IDI), managed by Statistics New Zealand. The opinions, findings, recommendations, and conclusions expressed in this paper are those of the author(s), not Statistics NZ. Access to the anonymised data used in this study was provided by Statistics NZ under the security and confidentiality provisions of the Statistics Act 1975. Only people authorised by the Statistics Act 1975 are allowed to see data about a particular person, household, business, or organisation, and the results in this paper have been confidentialised to protect these groups from identification and to keep their data safe. Careful consideration has been given to the privacy, security, and confidentiality issues associated with using administrative and survey data in the IDI. Further detail can be found in the Privacy impact assessment for the Integrated Data Infrastructure available from www.stats.govt.nz.